package twiscode.playpal;

/**
 * Created by Crusader on 6/22/2016.
 */
public class feedback {
    private String nama;
    private String comment;
    private String screenshot;
    public feedback(){}

    public feedback(String nama, String comment, String screenshot)
    {
        this.nama = nama;
        this.comment = comment;
        this.screenshot = screenshot;
    }
    public String getNama()
    {
        return nama;
    }

    public void setNama(String nama)
    {
        this.nama = nama;
    }

    public String getComment()
    {
        return comment;
    }

    public void setComment(String comment)
    {
        this.comment = comment;
    }
    public String getScreenshot()
    {
        return screenshot;
    }

    public void setScreenshot(String screenshot)
    {
        this.screenshot = screenshot;
    }
}
