package twiscode.playpal;

/**
 * Created by Romario on 22/06/2016.
 */

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.firebase.client.utilities.Pair;

import java.util.ArrayList;
import java.util.List;

import twiscode.playpal.Utilities.ConfigManager;

public abstract class FirebaseListAdapterFollow extends BaseAdapter {
    ConfigManager appManager;
    private Query mRef;
    private Firebase mRefUser = new Firebase(appManager.FIREBASE).child("users");
    private Class<String> mModelClass;
    private int mLayout;
    private LayoutInflater mInflater;
    private String mStatus;
    private List<Pair<String, User>> mModels;
    private List<String> mKeys;
    private ChildEventListener mListener;
    private Follow mFollow;
    Firebase mFirebaseRefCustom = new Firebase(ConfigManager.FIREBASE).child("users");


    /**
     * @param mRef        The Firebase location to watch for data changes. Can also be a slice of a location, using some
     *                    combination of <code>limit()</code>, <code>startAt()</code>, and <code>endAt()</code>,
     * @param mModelClass Firebase will marshall the data at a location into an instance of a class that you provide
     * @param mLayout     This is the mLayout used to represent a single list item. You will be responsible for populating an
     *                    instance of the corresponding view with the data from an instance of mModelClass.
     * @param activity    The activity containing the ListView
     */
    public FirebaseListAdapterFollow(Query mRef, final Class<String> mModelClass, int mLayout, Activity activity, String mStatus, Follow mFollow) {
        this.mRef = mRef;
        this.mModelClass = mModelClass;
        this.mLayout = mLayout;
        this.mStatus = mStatus;
        this.mFollow = mFollow;
        mInflater = activity.getLayoutInflater();
        mModels = new ArrayList<Pair<String, User>>();
        mKeys = new ArrayList<String>();


        // Look for all child events. We will then map them to our own internal ArrayList, which backs ListView

        /*mListener = this.mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> users = (HashMap<String, Object>) dataSnapshot.getValue();

                for (Object user : users.values()) {
                    HashMap<String, Object> userMap = (HashMap<String, Object>) user;
                    String userNumber = (String) userMap.remove("uid");
                    if (!mModels.contains(userNumber)) {
                        String name = (String) userMap.remove("nama");
                        Log.v("Data", name);
                        String username = (String) userMap.remove("username");
                        Log.v("Data", username);
                        User user1 = new User(userNumber, name, username, "", "email", "", "", "", "");
                        mModels.add(user1);

                    }
                }
                // thread executing here can get info from database and make subsequent call
                Collections.addAll(mModels);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });*/

        mListener = this.mRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, final String previousChildName) {

                if (!dataSnapshot.getValue(String.class).equals("")) {
                    final String modelString = dataSnapshot.getValue(FirebaseListAdapterFollow.this.mModelClass);
                    final String key = dataSnapshot.getKey();
                    mRefUser.child(modelString).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            User newUser = dataSnapshot.getValue(User.class);
                            Pair<String, User> pair = new Pair<String, User>(modelString, newUser);
                            if (previousChildName == null) {
                                //Log.v("Ini Null",model.toString());
                                mModels.add(0, pair);
                                //Log.v("Nyoba models nul",mModels.toString());
                                mKeys.add(0, key);
                            } else {
                                //Log.v("Nggak Null",model.toString());
                                int previousIndex = mKeys.indexOf(previousChildName);
                                int nextIndex = previousIndex + 1;
                                if (nextIndex == mModels.size()) {
                                    mModels.add(pair);
                                    //Log.v("Nyoba models 1",mModels.toString());
                                    mKeys.add(key);
                                } else {
                                    mModels.add(nextIndex, pair);
                                    //Log.v("Nyoba models 2",mModels.toString());
                                    mKeys.add(nextIndex, key);
                                }
                            }
                            //Log.v("coba mModels", mModels.get(0).getFirst().getGoingTo());
                            notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {

                        }
                    });
                    notifyDataSetChanged();
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                //One of the mModels changed. Replace it in our list and name mapping
                /*
                final String newModel = dataSnapshot.getValue(String.class);
                mRefUser.orderByValue().equalTo(newModel).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String key = dataSnapshot.getKey();
                        User newUser = dataSnapshot.getValue(User.class);
                        Pair<String, User> newPair = new Pair<String, User>(newModel, newUser);
                        int index = mKeys.indexOf(key);

                        mModels.set(index, newPair);

                        notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });*/
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                // A model was removed from the list. Remove it from our list and the name mapping
                String key = dataSnapshot.getKey();
                int index = mKeys.indexOf(key);

                mKeys.remove(index);
                mModels.remove(index);

                notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, final String previousChildName) {
                // A model changed position in the list. Update our list accordingly
                if (!dataSnapshot.getValue(String.class).equals("")) {
                    final String newModel = dataSnapshot.getValue(String.class);
                    mRefUser.orderByValue().equalTo(newModel).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String key = dataSnapshot.getKey();
                            User newUser = dataSnapshot.getValue(User.class);
                            Log.v("mencoba", newUser.getNama());
                            Pair<String, User> newPair = new Pair<String, User>(newModel, newUser);
                            int index = mKeys.indexOf(key);
                            mModels.remove(index);
                            mKeys.remove(index);
                            if (previousChildName == null) {
                                mModels.add(0, newPair);
                                mKeys.add(0, key);
                            } else {
                                int previousIndex = mKeys.indexOf(previousChildName);
                                int nextIndex = previousIndex + 1;
                                if (nextIndex == mModels.size()) {
                                    mModels.add(newPair);
                                    mKeys.add(key);
                                } else {
                                    mModels.add(nextIndex, newPair);
                                    mKeys.add(nextIndex, key);
                                }
                            }
                            notifyDataSetChanged();

                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {
                            Log.e("FirebaseListAdapter", "Listen was cancelled, no more updates will occur");
                        }
                    });
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e("FirebaseListAdapter", "Listen was cancelled, no more updates will occur");
            }

        });

    }


    /*public void cleanup() {
        // We're being destroyed, let go of our mListener and forget about all of the mModels
        mRef.removeEventListener(mListener);
        mModels.clear();
        mKeys.clear();
    }*/

            @Override
            public int getCount() {
                return mModels.size();
            }

            @Override
            public Object getItem(int i) {
                return mModels.get(i);
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                if (view == null) {
                    view = mInflater.inflate(mLayout, viewGroup, false);
                }

                Pair<String, User> model = mModels.get(i);
                Log.v("index", String.valueOf(i));
                //Log.v("model", model);
                populateView(view, model, mStatus, mFollow);
                return view;
            }

            /**
             * Each time the data at the given Firebase location changes, this method will be called for each item that needs
             * to be displayed. The arguments correspond to the mLayout and mModelClass given to the constructor of this class.
             * <p/>
             * Your implementation should populate the view using the data contained in the model.
             *
             * @param v     The view to populate
             * @param model The object containing the data used to populate the view
             */
            protected abstract void populateView(View v, Pair<String, User> model, String mStatus,Follow mFollow);
        }


