package twiscode.playpal;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.client.utilities.Pair;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

import twiscode.playpal.Utilities.ConfigManager;

/**
 * Created by Romario on 22/06/2016.
 */
public class FollowActivity extends AppCompatActivity {
    Firebase mFirebaseRef = new Firebase(ConfigManager.FIREBASE).child("follow");
    ListAdapterFollow mListAdapterFollow;
    Activity activity = this;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    String userIdOnline = user.getUid();
    Follow follow = new Follow();
    String status;
    String userId;
    String mStatus;
    Toolbar mToolbar;
    ProgressDialog mAuthProgressDialog;
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.follow_layout);


    }

    @Override
    protected void onStart() {
        super.onStart();
        setStatus(getIntent().getExtras().getString("status"));
        setUserId(getIntent().getExtras().getString("userId"));
        mStatus = getStatus().toLowerCase();
        //final String status = getIntent().getExtras().getString("status");
        //final String userId = getIntent().getExtras().getString("userId");
        ArrayList<String> followers = new ArrayList<String>();
        ArrayList<String> following = new ArrayList<String>();
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_action_name_white);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(status);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
        mFirebaseRef.child(userIdOnline).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.v("lala1", dataSnapshot.getRef().getPath().toString());
                Log.v("lala3", dataSnapshot.child("following").getValue().toString());
                Log.v("Lala2", userIdOnline);

                follow = dataSnapshot.getValue(Follow.class);
                Log.v("Lala", follow.getFollowing().get(0));
                final ListView listView = (ListView) findViewById(android.R.id.list);
                mListAdapterFollow = new ListAdapterFollow(getApplicationContext(),mFirebaseRef.child(userId).child(mStatus),activity,R.layout.follow_item, mStatus, follow);
                listView.setAdapter(mListAdapterFollow);
                if (listView != null) {
                    listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            //Toast.makeText(SearchPeoplePost1.this, String.valueOf(position)+" - "+String.valueOf(id), Toast.LENGTH_SHORT).show();
                            final Pair<String,User> selected = (Pair<String,User>) listView.getItemAtPosition(position);
                            final User select = selected.getSecond();
                            new Firebase(ConfigManager.FIREBASE).child("follow").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
                                @Override
                                public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                                    Follow follow = dataSnapshot.getValue(Follow.class);
                                    ArrayList<String> following = follow.getFollowing();
                                    Intent t = new Intent(FollowActivity.this ,ProfileUser.class);
                                    t.putStringArrayListExtra("following", following);
                                    t.putExtra("userId",select.getId());
                                    t.putExtra("username",select.getUsername());
                                    startActivity(t);
                                }

                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }
                            });
                        }
                    });
                }

                /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String select = (String) listView.getItemAtPosition(position);
                        Log.v("lala123", select);
                    }
                });*/

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //if (drawerToggle.onOptionsItemSelected(item)) {
        //   return true;
        //}

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
        finish();



    }

    public void makeConfirm(final String userIdCon, final String someoneId, final String someoneUsername, final Follow mFollow, final Follow follow2){
        new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
                .setTitle("Unfollow?")
                .setMessage("Do you really want to unfollow " + someoneUsername + " ?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        Follow newFollow = mFollow;
                        newFollow.following.remove(someoneId);

                        final Follow newFollow1 = newFollow;
                        dialogInterface("Unfollowing");
                        mFirebaseRef.child(someoneId).setValue(follow2);
                        mFirebaseRef.child(userIdCon).setValue(newFollow, new Firebase.CompletionListener() {
                            @Override
                            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                                refreshAdapter(newFollow1, "Unfollowing");
                            }
                        });
                        //firebaseRef.child(someone.getId()).child("Followers").child(userId).removeValue();






                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    public void dialogInterface(String fStatus){
        mAuthProgressDialog = new ProgressDialog(this);
        mAuthProgressDialog.setTitle("Loading");
        mAuthProgressDialog.setMessage(fStatus);
        mAuthProgressDialog.setCancelable(true);
        mAuthProgressDialog.show();
    }

    public void refreshAdapter(Follow newFollow, String fStatus){
        ListView listView = (ListView) findViewById(android.R.id.list);

        mListAdapterFollow = new ListAdapterFollow(getApplicationContext(),mFirebaseRef.child(userId).child(mStatus),activity,R.layout.follow_item, mStatus, newFollow);
        if (listView != null) {
            listView.setAdapter(mListAdapterFollow);
        }
        mListAdapterFollow.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                mAuthProgressDialog.dismiss();
            }

            @Override
            public void onInvalidated() {
                super.onInvalidated();
            }
        });

    }
}
