package twiscode.playpal;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import twiscode.playpal.Utilities.ConfigManager;


/**
 * Created by aldo on 29/03/2016.
 */
public class OneFragment extends Fragment {
    private FloatingActionButton fab;
    ConfigManager appManager;
    Firebase mRef = new Firebase(appManager.FIREBASE).child("posts");
    Firebase fRef = new Firebase(appManager.FIREBASE).child("follow");
    String userID = FirebaseAuth.getInstance().getCurrentUser().getUid();

    private ListAdapterPost mPostListAdapter;
    private int preLast;
    List<String> mUsername;
    List<Follow> follows;
    List<Follow> followTmp;
   // private static RecyclerView.Adapter adapter;
    //private RecyclerView.LayoutManager layoutManager;
    //private static RecyclerView recyclerView;
    //private static ArrayList<DataModel> data;{
        // Required empty public constructor
    //}
    Firebase usernameRef = new Firebase(appManager.FIREBASE).child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("username");
    View root;
    Activity activity;
    ProgressDialog dialog;
    SwipeRefreshLayout refresher;
    ListView listView;
    RelativeLayout noPost;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mUsername = new ArrayList<String>();
        follows = new ArrayList<Follow>();
        followTmp = new ArrayList<Follow>();
        activity = this.getActivity();
        dialog = new ProgressDialog(activity);
        dialog.setMessage("Loading your news feed");
        dialog.show();

        iniAttach();

    }

    private void iniAttach()
    {
        usernameRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final String username = dataSnapshot.getValue().toString();
                mUsername.add(0,username);
                fRef.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Follow follow = dataSnapshot.getValue(Follow.class);
                        follows.add(0,follow);
                        final ArrayList<String> following = follows.get(0).getFollowing();

                        mPostListAdapter = new ListAdapterPost(getContext(), mRef.orderByChild("timeStamp"), activity, R.layout.news_item, username, "fragment", userID, following);
                        listView.setAdapter(mPostListAdapter);


                        mPostListAdapter.registerDataSetObserver(new DataSetObserver() {
                            @Override
                            public void onChanged() {
                                super.onChanged();
                                fab.setVisibility(View.VISIBLE);
                                if(listView.getAdapter()!=null){
                                    if(listView.getAdapter().getCount()==0){
                                        noPost.setVisibility(View.VISIBLE);
                                    }else{
                                        noPost.setVisibility(View.GONE);
                                    }
                                }
                                Handler handler = new Handler();
                                handler.postDelayed(new Runnable() {
                                    public void run() {
                                        dialog.dismiss();
                                    }}, 1000);
                            }

                            @Override
                            public void onInvalidated() {
                                super.onInvalidated();
                                dialog.dismiss();
                            }
                        });
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            public void run() {
                if(listView.getAdapter()!=null) {
                    fab.setVisibility(View.VISIBLE);
                    if (listView.getAdapter() != null) {
                        if (listView.getAdapter().getCount() == 0) {
                            noPost.setVisibility(View.VISIBLE);
                        } else {
                            noPost.setVisibility(View.GONE);
                        }
                    }
                    dialog.dismiss();
                }
            }
        }, 3500);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        MainActivity activity1 = (MainActivity) this.getActivity();
        if(!isNetworkAvailable())
        {
            Toast.makeText(getContext(), "No Network Connection!", Toast.LENGTH_SHORT).show();
            dialog.dismiss();
        }
        else
            dialog.show();
        root = inflater.inflate(R.layout.fragment_one, container, false);
        refresher = (SwipeRefreshLayout) root.findViewById(R.id.refresher);
        noPost = (RelativeLayout) root.findViewById(R.id.no_post);
        noPost.setVisibility(View.GONE);
        fab = (FloatingActionButton) root.findViewById(R.id.fab);
        fRef.child(userID).addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Follow follow = dataSnapshot.getValue(Follow.class);
                follows.add(0,follow);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        fab.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                if(!isNetworkAvailable())
                {
                    Toast.makeText(getContext(), "No Network Connection!", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    refresher.setRefreshing(false);
                }
                else {
                    Intent intent = new Intent(getActivity(), PostActivity.class);
                    startActivity(intent);
                }
            }
        });
        listView = (ListView) root.findViewById(android.R.id.list);
        listView.setAdapter(mPostListAdapter);
        if(listView.getAdapter()!=null){
            fab.setVisibility(View.VISIBLE);
            if(listView.getAdapter().getCount()==0){
                noPost.setVisibility(View.VISIBLE);
            }else{
                noPost.setVisibility(View.GONE);
            }
            dialog.dismiss();
        }
        refresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!isNetworkAvailable())
                {
                    Toast.makeText(getContext(), "No Network Connection!", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    refresher.setRefreshing(false);
                }
                else {
                    iniAttach();
                    //Log.v("mUsername",mUsername.toString());
                    if(!mUsername.isEmpty()) {
                        String username = mUsername.get(0);
                        ArrayList<String> following = follows.get(0).getFollowing();
                        mPostListAdapter = new ListAdapterPost(getContext(), mRef.orderByChild("timeStamp"), activity, R.layout.news_item, username, "fragment", userID, following);
                        listView.setAdapter(mPostListAdapter);
                        mPostListAdapter.registerDataSetObserver(new DataSetObserver() {
                            @Override
                            public void onChanged() {
                                super.onChanged();
                                refresher.setRefreshing(false);
                                fab.setVisibility(View.VISIBLE);
                            }
                        });
                    }
                    else
                        refresher.setRefreshing(false);
                }
            }
        });







        /*recyclerView = (RecyclerView) root.findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());

        data = new ArrayList<DataModel>();
        for (int i = 0; i < MyData.nameArray.length; i++) {
            data.add(new DataModel(
                    MyData.nameArray[i],
                    MyData.hourArray[i],
                    MyData.drawableArray[i],
                    MyData.dateArray[i],
                    MyData.commentArray[i]
            ));
        }

        adapter = new CustomAdapter(getContext(), data);

        recyclerView.setAdapter(adapter);
        */
        // Inflate the layout for this fragment




        return root;
    }




}
