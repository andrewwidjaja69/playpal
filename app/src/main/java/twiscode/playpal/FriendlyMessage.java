package twiscode.playpal;

/**
 * Created by Crusader on 6/17/2016.
 */
public class FriendlyMessage {
    private String text;
    private String username;
    private String profpic;
    private String timestamp;
    private String name;

    public FriendlyMessage() {
    }

    public FriendlyMessage(String text, String username, String profpic, String timestamp, String name) {
        this.text = text;
        this.username = username;
        this.profpic = profpic;
        this.timestamp = timestamp;
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfpic() {
        return profpic;
    }

    public void setProfpic(String profpic) {
        this.profpic = profpic;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
