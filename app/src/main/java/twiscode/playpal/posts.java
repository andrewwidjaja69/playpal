package twiscode.playpal;

import java.util.ArrayList;

/**
 * Created by Romario on 14/06/2016.
 */
public class posts {
    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getGoingTo() {
        return goingTo;
    }

    public ArrayList<String> getLikers() {
        return likers;
    }

    public void setLikers(ArrayList<String> likers) {
        this.likers = likers;
    }

    public void setGoingTo(String goingTo) {
        this.goingTo = goingTo;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public ArrayList<String> getWithWho() {
        return withWho;
    }

    public void setWithWho(ArrayList<String> withWho) {
        this.withWho = withWho;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getPrivateStatus() {
        return privateStatus;
    }

    public void setPrivateStatus(String privateStatus) {
        this.privateStatus = privateStatus;
    }

    public posts(String goingTo, String imageURL, ArrayList<String> withWho, String dateTime, String privateStatus, String userId, String postId,Long timeStamp) {
        if(goingTo != null){this.goingTo = goingTo;}
        if(imageURL != null){this.imageURL = imageURL;}
        if(withWho != null){this.withWho = withWho;}
        if(dateTime != null){this.dateTime = dateTime;}
        if(privateStatus != null){this.privateStatus = privateStatus;}
        if(userId != null){
            this.userId = userId;
        }
        if(postId != null){
            this.postId = postId;
        }
        if(timeStamp!=null){
            this.timeStamp = timeStamp;
        }
    }

    private String goingTo = null;

    public posts() {
    }
    private boolean statusDeleted = false;
    private Long timeStamp = Long.valueOf(0);
    private String imageURL = "";
    private ArrayList<String> withWho = null;
    private String dateTime = "";
    private String privateStatus = "";
    private ArrayList<String> likers = null;
    private String postId = null;
    private ArrayList<String> waitingRecipients = null;

    public boolean isStatusDeleted() {
        return statusDeleted;
    }

    public void setStatusDeleted(boolean statusDeleted) {
        this.statusDeleted = statusDeleted;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getUserId() {
        return userId;
    }


    public void setUserId(String userId) {
        this.userId = userId;
    }


    private String userId = "";

    public ArrayList<String> getWaitingRecipients() {
        return waitingRecipients;
    }

    public void setWaitingRecipients(ArrayList<String> waitingRecipients) {
        this.waitingRecipients = waitingRecipients;
    }
}
