package twiscode.playpal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;

import twiscode.playpal.Utilities.ConfigManager;

/**
 * Created by Romario on 30/06/2016.
 */
public class SearchPeopleActivity extends AppCompatActivity{
    private ImageView btn;
    private ActionBar actionBar;
    private Toolbar toolbar;
    private String charSequence;
    private int numSelected = 0;
    ConfigManager appManager;
    // TODO: change this to your own Firebase URL

    private String mUsername;
    private Firebase mFirebaseRef;
    private ValueEventListener mConnectedListener;
    private ChatListAdapter mChatListAdapter;
    Context mContext;
    ArrayList<String> selected = new ArrayList<String>();
    ArrayList<String> following = new ArrayList<String>();
    ArrayList<String> followers = new ArrayList<String>();
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_post_friends);

        // Make sure we have a mUsername
        //setupUsername();

        //setTitle("Chatting as " + mUsername);

        // Setup our Firebase mFirebaseRef
        mFirebaseRef = new Firebase(appManager.FIREBASE).child("users");

        RelativeLayout selectedLayout = (RelativeLayout) findViewById(R.id.selected_layout);
        selectedLayout.setVisibility(View.GONE);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_18dp);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Search People");
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        // Setup our input methods. Enter key on the keyboard or pushing the send button
        //EditText inputText = (EditText) findViewById(R.id.messageInput);
        /*toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //getActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
           */

        //actionBar = getSupportActionBar();
        //getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        LayoutInflater mInflater = LayoutInflater.from(this);

        //View mCustomView = mInflater.inflate(R.layout.actionbar_search, null);
        //actionBar.setCustomView(mCustomView);
        //actionBar.setDisplayShowCustomEnabled(true);

        btn = (ImageView) findViewById(R.id.cross1);
        btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                //Intent i = new Intent(SearchPeoplePost.this, PostActivity.class);

                //startActivity(i);
                //overridePendingTransition(R.anim.slide_out, R.anim.slide_in);

                finish();
            }
        });

        /*inputText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                if (actionId == EditorInfo.IME_NULL && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    sendMessage();
                }
                return true;
            }
        });*/

        /*findViewById(R.id.sendButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMessage();
            }
        });*/

    }

    @Override
    public void onStart() {
        super.onStart();
        // Setup our view and list adapter. Ensure it scrolls to the bottom as data changes
        final ListView listView = (ListView) findViewById(android.R.id.list);
        final EditText inputSearch = (EditText) findViewById(R.id.edit_text_search);
        TextView selectedText = (TextView) findViewById(R.id.selected_number);
        final ImageView done = (ImageView) findViewById(R.id.done_search);
        following = new ArrayList<String>();
        followers = new ArrayList<String>();
        selected = new ArrayList<String>();
        mContext = getApplicationContext();
        if (selected != null && !selected.contains("")) {
            numSelected = selected.size();
        }else{
            numSelected = 0;
        }
        if (selectedText != null && numSelected == 0) {
            selectedText.setText(numSelected+" Selected");
            selectedText.setVisibility(View.INVISIBLE);
        }else{
            selectedText.setText(numSelected+" Selected");
            selectedText.setVisibility(View.VISIBLE);
        }
        if (done != null && numSelected == 0) {
            done.setVisibility(View.INVISIBLE);
        }else{
            done.setVisibility(View.VISIBLE);
        }


        // Tell our list adapter that we only want 50 messages at a time
        mChatListAdapter = new ChatListAdapter(getApplicationContext(), mFirebaseRef.orderByChild("uppername").limitToFirst(20), this, R.layout.people_item, mUsername, selected, following, followers, "status", "");

        if (listView != null) {
            listView.setAdapter(mChatListAdapter);
        }

        if (inputSearch != null) {

            inputSearch.addTextChangedListener(new TextWatcher() {

                @Override
                public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                    if (listView != null) {
                        charSequence = cs.toString();
                        Log.v("coba", cs.toString());
                        mChatListAdapter = new ChatListAdapter(getApplicationContext(),mFirebaseRef.orderByChild("uppername").limitToFirst(20), SearchPeopleActivity.this, R.layout.people_item, mUsername, selected, following,followers, "status",charSequence);
                        if(mChatListAdapter != null) {
                            listView.setAdapter(mChatListAdapter);

                            //Toast.makeText(SearchPeoplePost1.this, "There is no username start with "+cs.toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) { }

                @Override
                public void afterTextChanged(Editable arg0) {}
            });
        }
        mChatListAdapter.registerDataSetObserver(new DataSetObserver() {

            @Override
            public void onChanged() {
                super.onChanged();
                if (listView != null) {
                    listView.setSelection(mChatListAdapter.getCount() - 1);
                }
            }
        });

        if (listView != null) {
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    //Toast.makeText(SearchPeoplePost1.this, String.valueOf(position)+" - "+String.valueOf(id), Toast.LENGTH_SHORT).show();
                    final User select = (User) listView.getItemAtPosition(position);
                    new Firebase(appManager.FIREBASE).child("follow").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
                        @Override
                        public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                            Follow follow = dataSnapshot.getValue(Follow.class);
                            ArrayList<String> following = follow.getFollowing();
                            Intent t = new Intent(SearchPeopleActivity.this ,ProfileUser.class);
                            t.putStringArrayListExtra("following", following);
                            t.putExtra("userId",select.getId());
                            t.putExtra("username",select.getUsername());
                            startActivity(t);
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {

                        }
                    });
                }
            });
        }
        btn = (ImageView) findViewById(R.id.cross1);
        btn.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {

                inputSearch.setText("");
                //Intent i = new Intent(SearchPeoplePost.this, PostActivity.class);

                //startActivity(i);
                //overridePendingTransition(R.anim.slide_out, R.anim.slide_in);

                //finish();
            }
        });
        if (done != null) {
            done.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent returnIntent = new Intent();
                    if(selected.size()>1){
                        selected.remove("");
                    }
                    returnIntent.putStringArrayListExtra("result",selected);
                    setResult(Activity.RESULT_OK,returnIntent);
                    finish();
                }
            });
        }


        // Finally, a little indication of connection status
       /* mConnectedListener = mFirebaseRef.getRoot().child(".info/connected").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean connected = (Boolean) dataSnapshot.getValue();
                if (connected) {
                    Toast.makeText(MainActivity.this, "Connected to Firebase", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Disconnected from Firebase", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                // No-op
            }
        });
    }*/
/*
    @Override
    public void onStop() {
        super.onStop();
        mFirebaseRef.getRoot().child(".info/connected").removeEventListener(mConnectedListener);
        mChatListAdapter.cleanup();
    }
*/
    /*private void setupUsername() {
        SharedPreferences prefs = getApplication().getSharedPreferences("ChatPrefs", 0);
        mUsername = prefs.getString("username", null);
        if (mUsername == null) {
            Random r = new Random();
            // Assign a random user name if we don't have one saved.
            mUsername = "JavaUser" + r.nextInt(100000);
            prefs.edit().putString("username", mUsername).commit();
        }
    }*/

    /*private void sendMessage() {
        EditText inputText = (EditText) findViewById(R.id.messageInput);
        String input = inputText.getText().toString();
        if (!input.equals("")) {
            // Create our 'model', a Chat object
            User user = new user(input, mUsername);
            // Create a new, auto-generated child of that chat location, and save our chat data there
            mFirebaseRef.push().setValue(chat);
            inputText.setText("");
        }
    }*/
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //if (drawerToggle.onOptionsItemSelected(item)) {
        //   return true;
        //}

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
        finish();



    }
}
