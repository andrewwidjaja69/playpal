package twiscode.playpal;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Romario on 16/06/2016.
 */
public abstract class FirebaseListAdapter extends BaseAdapter {

    private Query mRef;
    private Class<User> mModelClass;
    private int mLayout;
    private LayoutInflater mInflater;
    private ArrayList<String> mSelected;
    private List<User> mModels;
    private List<String> mKeys;
    private ChildEventListener mListener;
    private ArrayList<String> following;
    String mStatus;
    String mfilter;
    public static <User> void append(List<User> list, Class<? extends User> cls) throws Exception {
        User elem = cls.newInstance();
        list.add(elem);
    }
    User createContents(Class<User> clazz) throws IllegalAccessException, InstantiationException {
        return clazz.newInstance();
    }




    /**
     * @param mRef        The Firebase location to watch for data changes. Can also be a slice of a location, using some
     *                    combination of <code>limit()</code>, <code>startAt()</code>, and <code>endAt()</code>,
     * @param mModelClass Firebase will marshall the data at a location into an instance of a class that you provide
     * @param mLayout     This is the mLayout used to represent a single list item. You will be responsible for populating an
     *                    instance of the corresponding view with the data from an instance of mModelClass.
     * @param activity    The activity containing the ListView
     */
    public FirebaseListAdapter(Query mRef, final Class<User> mModelClass, int mLayout, Activity activity, ArrayList<String> selected, final ArrayList<String> following,final ArrayList<String> followers, String status, String filter) {
        this.mRef = mRef;
        this.mModelClass = mModelClass;
        this.mLayout = mLayout;
        this.mSelected = selected;
        mInflater = activity.getLayoutInflater();
        mModels = new ArrayList<User>();
        mKeys = new ArrayList<String>();
        this.mStatus = status;
        this.mfilter = filter;




        // Look for all child events. We will then map them to our own internal ArrayList, which backs ListView

        /*mListener = this.mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> users = (HashMap<String, Object>) dataSnapshot.getValue();

                for (Object user : users.values()) {
                    HashMap<String, Object> userMap = (HashMap<String, Object>) user;
                    String userNumber = (String) userMap.remove("uid");
                    if (!mModels.contains(userNumber)) {
                        String name = (String) userMap.remove("nama");
                        Log.v("Data", name);
                        String username = (String) userMap.remove("username");
                        Log.v("Data", username);
                        User user1 = new User(userNumber, name, username, "", "email", "", "", "", "");
                        mModels.add(user1);

                    }
                }
                // thread executing here can get info from database and make subsequent call
                Collections.addAll(mModels);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });*/

        mListener = this.mRef.addChildEventListener(new ChildEventListener() {


            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
                Log.v("mencobaPath",dataSnapshot.getRef().getPath().toString());
                User model = dataSnapshot.getValue(FirebaseListAdapter.this.mModelClass);
                String key = dataSnapshot.getKey();
                if(mStatus.equals("statusPost")) {
                    if ((following.contains(model.getId())||followers.contains(model.getId()))&&(model.getUppername().contains(mfilter.toUpperCase())||model.getUsername().toLowerCase().contains(mfilter.toLowerCase()))) {
                        //System.out.println(key + " was " +  + " meters tall");
                        Log.v("Child Added", key);
                        // Insert into the correct location, based on previousChildName
                        if (previousChildName == null) {
                            Log.v("Ini Null", model.toString());
                            mModels.add(0, model);
                            Log.v("Nyoba models nul", mModels.toString());
                            mKeys.add(0, key);
                        } else {
                            Log.v("Nggak Null", model.toString());
                            int previousIndex = mKeys.indexOf(previousChildName);
                            int nextIndex = previousIndex + 1;
                            if (nextIndex == mModels.size()) {
                                mModels.add(model);
                                Log.v("Nyoba models 1", mModels.toString());
                                mKeys.add(key);
                            } else {
                                mModels.add(nextIndex, model);
                                Log.v("Nyoba models 2", mModels.toString());
                                mKeys.add(nextIndex, key);
                            }
                        }

                        notifyDataSetChanged();
                    }
                }else{
                    Log.v("Child Added", key);
                    // Insert into the correct location, based on previousChildName
                    if(model.getUppername().contains(mfilter.toUpperCase())||model.getUsername().toLowerCase().contains(mfilter.toLowerCase())){
                    if (previousChildName == null) {
                        Log.v("Ini Null", model.toString());
                        mModels.add(0, model);
                        Log.v("Nyoba models nul", mModels.toString());
                        mKeys.add(0, key);
                    } else {
                        Log.v("Nggak Null", model.toString());
                        int previousIndex = mKeys.indexOf(previousChildName);
                        int nextIndex = previousIndex + 1;
                        if (nextIndex == mModels.size()) {
                            mModels.add(model);
                            Log.v("Nyoba models 1", mModels.toString());
                            mKeys.add(key);
                        } else {
                            mModels.add(nextIndex, model);
                            Log.v("Nyoba models 2", mModels.toString());
                            mKeys.add(nextIndex, key);
                        }
                    }

                    notifyDataSetChanged();
                    }
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                // One of the mModels changed. Replace it in our list and name mapping
                String key = dataSnapshot.getKey();
                User newModel = dataSnapshot.getValue(FirebaseListAdapter.this.mModelClass);
                int index = mKeys.indexOf(key);

                mModels.set(index, newModel);

                notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                // A model was removed from the list. Remove it from our list and the name mapping
                String key = dataSnapshot.getKey();
                int index = mKeys.indexOf(key);

                mKeys.remove(index);
                mModels.remove(index);

                notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {

                // A model changed position in the list. Update our list accordingly

                String key = dataSnapshot.getKey();
                User newModel = dataSnapshot.getValue(FirebaseListAdapter.this.mModelClass);
                if(following.contains(newModel.getId())){
                int index = mKeys.indexOf(key);
                mModels.remove(index);
                mKeys.remove(index);
                if (previousChildName == null) {
                    mModels.add(0, newModel);
                    mKeys.add(0, key);
                } else {
                    int previousIndex = mKeys.indexOf(previousChildName);
                    int nextIndex = previousIndex + 1;
                    if (nextIndex == mModels.size()) {
                        mModels.add(newModel);
                        mKeys.add(key);
                    } else {
                        mModels.add(nextIndex, newModel);
                        mKeys.add(nextIndex, key);
                    }
                }
                notifyDataSetChanged();
                }
            }


            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e("FirebaseListAdapter", "Listen was cancelled, no more updates will occur");
            }

        });
    }

    /*public void cleanup() {
        // We're being destroyed, let go of our mListener and forget about all of the mModels
        mRef.removeEventListener(mListener);
        mModels.clear();
        mKeys.clear();
    }*/

    @Override
    public int getCount() {
        return mModels.size();
    }

    @Override
    public Object getItem(int i) {
        return mModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = mInflater.inflate(mLayout, viewGroup, false);
        }

        User model = mModels.get(i);
        // Call out to subclass to marshall this model into the provided view
        populateView(view, model, mSelected);
        return view;
    }

    /**
     * Each time the data at the given Firebase location changes, this method will be called for each item that needs
     * to be displayed. The arguments correspond to the mLayout and mModelClass given to the constructor of this class.
     * <p/>
     * Your implementation should populate the view using the data contained in the model.
     *
     * @param v     The view to populate
     * @param model The object containing the data used to populate the view
     */
    protected abstract void populateView(View v, User model, ArrayList<String> selected);
}

