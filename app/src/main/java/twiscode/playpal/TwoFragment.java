package twiscode.playpal;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.database.DataSetObserver;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bartoszlipinski.recyclerviewheader2.RecyclerViewHeader;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;
import java.util.List;

import twiscode.playpal.Utilities.ConfigManager;

/**
 * Created by aldo on 29/03/2016.
 */
public class TwoFragment extends Fragment implements View.OnClickListener {

    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private static RecyclerView recyclerView;
    private static ArrayList<DataModel> data;
    private RecyclerViewHeader recyclerHeader;
    private RelativeLayout show_follow_req;
    Firebase mRef = new Firebase(ConfigManager.FIREBASE).child("activity").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
    Firebase mFirebaseRef = new Firebase(ConfigManager.FIREBASE).child("follow");
    private ListAdapterActivity mActivityListAdapter;
    MainActivity mActivity;
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    String userIdOnline = user.getUid();
    Follow follow = new Follow();
    String status;
    String userId;
    String mStatus;
    ProgressDialog mAuthProgressDialog;
    Context context;
    Firebase ref;
    Activity activity;
    View root;
    ListView listView;
    SwipeRefreshLayout refresher;
    List<Follow> follows;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mActivity = (MainActivity) getActivity();
        this.context = getContext();
        this.activity = this.getActivity();
        follows = new ArrayList<Follow>();

        ref = new Firebase(ConfigManager.FIREBASE).child("follow").child(FirebaseAuth.getInstance().getCurrentUser().getUid());
        ref.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Follow follow = dataSnapshot.getValue(Follow.class);
                follows.add(0,follow);
                mActivityListAdapter = new ListAdapterActivity(getContext(), mRef.orderByChild("timeStamp"), activity, R.layout.activity_items, follows.get(0), TwoFragment.this);
                listView.setAdapter(mActivityListAdapter);
                mActivityListAdapter.registerDataSetObserver(new DataSetObserver() {
                    @Override
                    public void onChanged() {
                        super.onChanged();
                        setListViewHeightBasedOnChildren(listView);
                        //
                    }

                    @Override
                    public void onInvalidated() {
                        super.onInvalidated();
                    }
                });
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) activity.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        root = inflater.inflate(R.layout.fragment_two, container, false);
        if(!isNetworkAvailable())
        {
            Toast.makeText(getContext(), "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
        //show_follow_req = (RelativeLayout) root.findViewById(R.id.show_follow_req);
        refresher = (SwipeRefreshLayout) root.findViewById(R.id.refresher);
        refresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(isNetworkAvailable()) {
                    if(!follows.isEmpty()) {
                        mActivityListAdapter = new ListAdapterActivity(getContext(), mRef.orderByChild("timeStamp"), activity, R.layout.activity_items, follows.get(0), TwoFragment.this);
                        listView.setAdapter(mActivityListAdapter);
                        mActivityListAdapter.registerDataSetObserver(new DataSetObserver() {
                            @Override
                            public void onChanged() {
                                super.onChanged();
                                refresher.setRefreshing(false);
                            }
                        });
                    }
                    else
                        refresher.setRefreshing(false);
                }
                else
                {
                    Toast.makeText(getContext(), "No Network Connection!", Toast.LENGTH_SHORT).show();
                    refresher.setRefreshing(false);
                }
            }
        });
        listView = (ListView) root.findViewById(android.R.id.list);
        listView.setAdapter(mActivityListAdapter);


        return root;
    }

    @Override
    public void onClick(View view) {
//        if(view.getId() == R.id.show_follow_req){
//            FragmentTransaction transaction = getFragmentManager()
//                    .beginTransaction();
//            /*
//             * When this container fragment is created, we fill it with our first
//             * "real" fragment
//             */
//            transaction.replace(R.id.fragment_two_container, new FragmentFollowReq());
//            transaction.commit();
//        }
    }

    public void makeConfirm(final String userIdCon, final String someoneId, final String someoneUsername, final Follow mFollow, final Follow follow2){

        Follow newFollow = mFollow;
        new AlertDialog.Builder(getActivity(), R.style.AppCompatAlertDialogStyle)
                .setTitle("Unfollow?")
                .setMessage("Do you really want to unfollow " + someoneUsername + " ?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {

                        Follow newFollow = mFollow;
                        newFollow.following.remove(someoneId);

                        final Follow newFollow1 = newFollow;
                        dialogInterface("Unfollowing");
                        mFirebaseRef.child(someoneId).setValue(follow2);
                        mFirebaseRef.child(userIdCon).setValue(newFollow, new Firebase.CompletionListener() {
                            @Override
                            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                                refreshAdapter(newFollow1, "Unfollowing");
                            }
                        });
                        //firebaseRef.child(someone.getId()).child("Followers").child(userId).removeValue();






                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }

    public void dialogInterface(String fStatus){
        mAuthProgressDialog = new ProgressDialog(context);
        mAuthProgressDialog.setTitle("Loading");
        mAuthProgressDialog.setMessage(fStatus);
        mAuthProgressDialog.setCancelable(true);
        mAuthProgressDialog.show();
    }

    public void refreshAdapter(Follow newFollow, String fStatus){
        ListView listView = (ListView) this.getView().findViewById(android.R.id.list);

        mActivityListAdapter = new ListAdapterActivity(context, mRef.orderByChild("timeStamp"), this.getActivity(), R.layout.activity_items, newFollow, TwoFragment.this);
        if (listView != null) {
            listView.setAdapter(mActivityListAdapter);
        }
        mActivityListAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                mAuthProgressDialog.dismiss();
            }

            @Override
            public void onInvalidated() {
                super.onInvalidated();
            }
        });

    }

    public static void setListViewHeightBasedOnChildren(ListView listView)
    {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight=0;
        View view = null;

        for (int i = 0; i < listAdapter.getCount(); i++)
        {
            view = listAdapter.getView(i, view, listView);

            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
                        ViewGroup.LayoutParams.MATCH_PARENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();

        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));
        Log.v("cobalaka", String.valueOf(params.height));
        listView.setLayoutParams(params);
        listView.requestLayout();

    }
}



