package twiscode.playpal;

import android.app.Activity;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.client.Query;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Romario on 16/06/2016.
 */
public class ChatListAdapter extends FirebaseListAdapter {

    // The mUsername for this client. We use this to indicate which messages originated from this user
    private String mUsername;
    private android.content.Context context;
    public ChatListAdapter(android.content.Context context, Query ref, Activity activity, int layout, String mUsername, ArrayList<String> selected, ArrayList<String> following,ArrayList<String> followers, String status, String filter) {
        super(ref, User.class, layout, activity, selected, following,followers, status, filter);
        this.context = context;
        this.mUsername = mUsername;
    }

    /**
     * Bind an instance of the <code>Chat</code> class to our view. This method is called by <code>FirebaseListAdapter</code>
     * when there is a data change, and we are given an instance of a View that corresponds to the layout that we passed
     * to the constructor, as well as a single <code>Chat</code> instance that represents the current data to bind.
     *
     * @param view A view instance corresponding to the layout we passed to the constructor.
     * @param user An instance representing the current state of a chat message
     */
    @Override
    protected void populateView(View view, User user, ArrayList<String> selected) {
        // Map a Chat object to an entry in our listview
        String nama = user.getNama();
        String username1 = user.getUsername();
        String profpicUrl = user.getProfpic();
        Log.v("coba username", username1);
        Log.v("ini coba Nama",nama);
        TextView authorText = (TextView) view.findViewById(R.id.people_item_nama);
        ImageView check = (ImageView) view.findViewById(R.id.check);
        ImageView profpic = (ImageView)view.findViewById(R.id.people_item_image);
        profpic.setImageResource(R.drawable.ayam);
        if(!profpicUrl.isEmpty()){
            Picasso.with(context).load(profpicUrl).transform(new CircleTransform()).into(profpic);
        }else{
            Picasso.with(context).load(R.drawable.ayam).into(profpic);
        }
        authorText.setText(nama);
        if(selected.contains(username1)){
            view.setBackgroundColor(Color.parseColor("#dddddd"));
            check.setVisibility(View.VISIBLE);
        }else{
            view.setBackgroundColor(Color.parseColor("#ffffff"));
            check.setVisibility(View.INVISIBLE);
        }
        // If the message was sent by this user, color it differently
        /*if (author != null && author.equals(mUsername)) {
            authorText.setTextColor(Color.RED);
        } else {
            authorText.setTextColor(Color.BLUE);
        }*/
        TextView username = (TextView) view.findViewById(R.id.people_item_username);
        username.setText(username1);
        //(TextView) view.findViewById(R.id.email)).setText(user.getUsername();
    }


}