package twiscode.playpal;

/**
 * Created by Romario on 22/06/2016.
 */

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.firebase.client.utilities.Pair;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import twiscode.playpal.Utilities.ConfigManager;

public class ListAdapterFollow extends FirebaseListAdapterFollow {

    // The mUsername for this client. We use this to indicate which messages originated from this user
    private String mUsername;
    Firebase mFirebaseRefCustom = new Firebase(ConfigManager.FIREBASE).child("users");
    Context context;
    Firebase firebaseRefRoot = new Firebase(ConfigManager.FIREBASE);
    Firebase firebaseRef = new Firebase(ConfigManager.FIREBASE).child("follow");
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    String userId = user.getUid();
    FollowActivity mActivity;

    public ListAdapterFollow(Context context, Query ref, Activity activity, int layout, String status, Follow follow) {
        super(ref, String.class, layout, activity, status, follow);
        this.context = context;
        this.mActivity = (FollowActivity) activity;
    }

    /**
     * Bind an instance of the <code>Chat</code> class to our view. This method is called by <code>FirebaseListAdapter</code>
     * when there is a data change, and we are given an instance of a View that corresponds to the layout that we passed
     * to the constructor, as well as a single <code>Chat</code> instance that represents the current data to bind.
     *
     * @param view   A view instance corresponding to the layout we passed to the constructor.
     * @param follow An instance representing the current state of a chat message
     */
    @Override
    protected void populateView(final View view, final Pair<String, User> follow, final String status, final Follow mFollow) {
        //Firebase newMFirebaseRefCustom = mFirebaseRefCustom.child(follow);
//        Log.v("FollowText", follow);
//        Log.v("StatusText", status);
//        String lala = mFollow.getFollowers().get(0);
//        String lala2 = mFollow.getFollowers().get(1);
//        Log.v("get", lala);
//        Log.v("get2", lala2);
        final User someone = follow.getSecond();
        String profpicUrl = someone.getProfpic();
        ImageView profpic = (ImageView) view.findViewById(R.id.follow_item_image);
        TextView nama = (TextView) view.findViewById(R.id.follow_item_nama);
        TextView username = (TextView) view.findViewById(R.id.follow_item_username);
        RelativeLayout followBtn = (RelativeLayout) view.findViewById(R.id.follow_item_button);
        TextView followBtnText = (TextView) view.findViewById(R.id.follow_item_button_text);
        if(!profpicUrl.isEmpty()){
            Picasso.with(context).load(profpicUrl).transform(new CircleTransform()).into(profpic);
        }else{
            Picasso.with(context).load(R.drawable.ayam).into(profpic);
        }
        Log.v("cobaUser", someone.getNama());
        String namaText = someone.getNama();
        String usernameText = someone.getUsername();
        nama.setText(namaText);
        username.setText(usernameText);
        Log.v("inilagi", nama.getText().toString());
        Log.v("mFollow", mFollow.getFollowing().toString());
        Log.v("mFollowID", someone.getId());
            if (mFollow.getFollowing().contains(someone.getId())) {
                followBtn.setBackgroundResource(R.drawable.button_following);
                followBtnText.setText("Unfollow");
                followBtnText.setTextColor(Color.WHITE);
                followBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (mActivity!=null) {
                            firebaseRef.child(someone.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    Follow follow2 = dataSnapshot.getValue(Follow.class);
                                    ArrayList<String> followers = follow2.getFollowers();
                                    followers.remove(userId);
                                    follow2.setFollowers(followers);
                                    //firebaseRef.child(someone.getId()).setValue(follow2);
                                    mActivity.makeConfirm(userId, someone.getId(), someone.getUsername(), mFollow, follow2);
                                }

                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }
                            });
                        }

                    }
                });

            } else {

                followBtn.setBackgroundResource(R.drawable.button_follow);
                followBtnText.setText("+ Follow");
                followBtnText.setTextColor(view.getResources().getColor(R.color.errorColor));
                followBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ArrayList<String> following = mFollow.getFollowing();
                        Follow follow1 = mFollow;
                        following.add(someone.getId());
                        follow1.setFollowing(following);
                        firebaseRef.child(userId).setValue(follow1);
                        firebaseRef.child(someone.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Follow follow2 = dataSnapshot.getValue(Follow.class);
                                ArrayList<String> followers = follow2.getFollowers();
                                followers.add(userId);
                                follow2.setFollowers(followers);
                                firebaseRef.child(someone.getId()).setValue(follow2);
                                Firebase pushFirebaseRef = firebaseRefRoot.child("activity").child(someone.getId()).push();
                                final String idActivity = pushFirebaseRef.getKey();
                                Long timeStamp = System.currentTimeMillis()/1000*(-1);
                                Activities newActivity = new Activities(someone.getId(),"Started following you",FirebaseAuth.getInstance().getCurrentUser().getUid(),"follow","",idActivity,timeStamp);
                                pushFirebaseRef.setValue(newActivity);
                            }

                            @Override
                            public void onCancelled(FirebaseError firebaseError) {

                            }
                        });
                        mActivity.dialogInterface("Following");
                        mActivity.refreshAdapter(follow1, "Following...");
                    }
                });
            }


    }

}

        // Map a Chat object to an entry in our listview
//        String nama = user.getNama();
//        String username1 = user.getUsername();
//        Log.v("coba username", username1);
//        Log.v("ini coba Nama",nama);
//        TextView authorText = (TextView) view.findViewById(R.id.people_item_nama);
//        ImageView check = (ImageView) view.findViewById(R.id.check);
//        authorText.setText(nama);
//        if(selected.contains(username1)){
//            view.setBackgroundColor(Color.parseColor("#dddddd"));
//            check.setVisibility(view.VISIBLE);
//        }else{
//            view.setBackgroundColor(Color.parseColor("#ffffff"));
//            check.setVisibility(view.INVISIBLE);
//        }
//        // If the message was sent by this user, color it differently
//        /*if (author != null && author.equals(mUsername)) {
//            authorText.setTextColor(Color.RED);
//        } else {
//            authorText.setTextColor(Color.BLUE);
//        }*/
//        TextView username = (TextView) view.findViewById(R.id.people_item_username);
//        username.setText(username1);
//        //(TextView) view.findViewById(R.id.email)).setText(user.getUsername();
//
