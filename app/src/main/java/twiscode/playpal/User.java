package twiscode.playpal;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;
public class User {
    private String nama;
    private String username;
    private String profpic;
    private String email;
    private String phone;
    private String gender;
    private String birthdate;
    private String id;
    private String uppername;
    private Integer rating;

    public User()
    {}
    @JsonIgnoreProperties
    public User(String id, String nama, String username, String profpic, String email, String birthdate, String phone, String gender, Integer rating) {
        this.nama = nama;
        this.username = username;
        this.profpic = profpic;
        this.email = email;
        this.id = id;
        this.birthdate = birthdate;
        this.phone = phone;
        this.gender = gender;
        this.uppername = nama.toUpperCase();
        this.rating = rating;
    }
    @JsonIgnoreProperties
    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("id", id);
        result.put("nama", nama);
        result.put("username", username);
        result.put("birthdate", birthdate);
        result.put("phone", phone);
        result.put("gender", gender);
        result.put("email", email);
        result.put("profpic", profpic);
        result.put("uppername",uppername);
        result.put("rating",rating);
        //result.put("birthday", birthday);
        return result;
    }
    @Exclude
    public Map<String, Object> toMapRate() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("rating",rating);
        return result;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getProfpic() {
        return profpic;
    }

    public void setProfpic(String profpic) {
        this.profpic = profpic;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(String birthdate) {
        this.birthdate = birthdate;
    }

    public String getPhone(){return phone;}

    public void setPhone(String phone){this.phone=phone;}

    public String getGender(){return gender;}

    public void setGender(String gender){this.gender = gender;}

    public String getUppername(){return uppername;}

    public void setUppername(String uppername){this.uppername = uppername;}

    public Integer getRating(){return rating;}

    public void setRating(Integer rating){this.rating = rating;}
}