package twiscode.playpal;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.firebase.client.Query;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import twiscode.playpal.Utilities.ConfigManager;

/**
 * Created by Crusader on 6/27/2016.
 */
public class ListAdapterLobby extends FirebaseListAdapterLobby{
    private String mUsername;
    Firebase mFirebaseRefCustom = new Firebase(ConfigManager.FIREBASE).child("users");
    Context context;
    Firebase firebaseRef = new Firebase(ConfigManager.FIREBASE).child("message");
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    String userId = user.getUid();
    LobbyChatActivity mActivity;
    public ListAdapterLobby(Context context, Query ref, Activity activity, int layout, String username) {
        super(ref, String.class, layout, activity, username);
        this.context = context;
        this.mActivity = (LobbyChatActivity) activity;
    }
    @Override
    protected void populateView(final View view, final posts follow) {
        //Firebase newMFirebaseRefCustom = mFirebaseRefCustom.child(follow);
//        Log.v("FollowText", follow);
//        Log.v("StatusText", status);
//        String lala = mFollow.getFollowers().get(0);
//        String lala2 = mFollow.getFollowers().get(1);
//        Log.v("get", lala);
//        Log.v("get2", lala2);
        //final User someone = follow.;
        String profpicUrl = follow.getImageURL();
        ImageView profpic = (ImageView) view.findViewById(R.id.follow_item_image);
        TextView nama = (TextView) view.findViewById(R.id.follow_item_nama);
        TextView username = (TextView) view.findViewById(R.id.follow_item_username);
        RelativeLayout followBtn = (RelativeLayout) view.findViewById(R.id.follow_item_button);
        TextView followBtnText = (TextView) view.findViewById(R.id.follow_item_button_text);
        if(profpicUrl!="")
        {
            Picasso.with(context).load(profpicUrl).transform(new CircleTransform()).into(profpic);
        }
        else
        {
            profpic.setImageResource(R.drawable.ayam);
        }
        //Picasso.with(context).load(profpicUrl).into(profpic);
        Log.v("cobaUser", follow.getGoingTo());

        String namaText = follow.getGoingTo();
        String[] split = new String[100];
        if(namaText!=null)
        {
            split = namaText.split(", ");
        }
        String usernameText = follow.getDateTime();
        nama.setText(split[0]);
        username.setText(usernameText);
        Log.v("inilagi", nama.getText().toString());
        //Log.v("mParticipant", mParticipant.getUsername().toString());
    }
}
