package twiscode.playpal;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.app.ListActivity;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Random;

import twiscode.playpal.Utilities.ConfigManager;

public class ChatActivity2 extends AppCompatActivity{
    private String mUsername,mNama,mPhotoUrl;
    private Firebase mFirebaseRef;
    private ValueEventListener mConnectedListener;
    private ListAdapterChat mChatListAdapter;
    private DatabaseReference mDatabase;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private FirebaseAuth mAuth;
    private String roomid,namaroom,wakturoom;
    private TextView judulRoom,judulRoom2,roomtime,leavebtn;
    private Button btnleef;
    private EditText mMessageEditText;
    private Button mSendButton;
    private FrameLayout iniFrame;
    private Toolbar mToolbar;
    private String username,iniUsername;
    private ListView listView;
    ConfigManager appManager;
    Firebase firebaseRefUser = new Firebase(appManager.FIREBASE).child("users");
    Firebase firebaseRefChat = new Firebase(appManager.FIREBASE).child("message");
    Firebase firebaseRefPost = new Firebase(appManager.FIREBASE).child("posts");
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat2);
        Intent intent = getIntent();
        username = intent.getStringExtra("user");
        //Log.v("usernameskrg",mUsername);
        roomid = intent.getStringExtra("idroom");
        firebaseRefPost.child(roomid).child("goingTo").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String snap = dataSnapshot.getValue(String.class);
                namaroom = snap;
                judulRoom2.setText(namaroom);
                //roomtime.setText(wakturoom);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        firebaseRefPost.child(roomid).child("dateTime").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String snap = dataSnapshot.getValue(String.class);
                wakturoom = snap;
                roomtime.setText(wakturoom);
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        //namaroom = intent.getStringExtra("nroom");
        //namaroom2 = namaroom;
        //wakturoom = intent.getStringExtra("wroom");
        //Log.v("waktu",wakturoom);
        judulRoom = (TextView) findViewById(R.id.title_text);
        judulRoom2 = (TextView) findViewById(R.id.title_text2);
        roomtime = (TextView) findViewById(R.id.textView5);
        mMessageEditText = (EditText) findViewById(R.id.messageEditText);
        //leavebtn = (TextView) findViewById(R.id.leave);
        listView = (ListView) findViewById(R.id.list);
        btnleef = (Button) findViewById(R.id.leave);
        mMessageEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
                listView.post(new Runnable(){
                    public void run() {
                        listView.setSelection(listView.getCount() - 1);
                    }});
                if (actionId == EditorInfo.IME_NULL && keyEvent.getAction() == KeyEvent.ACTION_DOWN) {
                    sendMessage();
                }
                return true;
            }
        });
        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    mSendButton.setEnabled(true);
                    mSendButton.setBackgroundResource(R.color.colorPrimary);
                    mSendButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                } else {
                    mSendButton.setEnabled(false);

                    mSendButton.setBackgroundResource(R.color.greyForText);
                    mSendButton.setTextColor(ContextCompat.getColor(getApplicationContext(), R.color.white));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
        mSendButton = (Button) findViewById(R.id.sendButton);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    listView.post(new Runnable() {
                        public void run() {
                            listView.setSelection(listView.getCount() - 1);
                        }
                    });
                    sendMessage();
                }
                else
                    Toast.makeText(ChatActivity2.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });
        firebaseRefPost.child(roomid).child("userId").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String iniiduser = dataSnapshot.getValue(String.class);
                Log.v("iduuser",iniiduser);
                if(iniiduser.equals(FirebaseAuth.getInstance().getCurrentUser().getUid()))
                {
                    btnleef.setVisibility(View.GONE);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        btnleef.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    firebaseRefUser.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("username").addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final String idPost = roomid;
                            firebaseRefChat.child(idPost).child("username").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    ArrayList<String> yangikut = (ArrayList<String>) dataSnapshot.getValue();
                                    for (int x = 0; x < yangikut.size(); x++) {
                                        Log.v("yangikut", yangikut.get(x));
                                    }

                                    if (yangikut.contains(mUsername)) {
                                        yangikut.remove(mUsername);
                                    }
                                    if (yangikut.contains("")) {
                                        yangikut.remove("");
                                    }
                                    if (yangikut.isEmpty()) {
                                        yangikut.add("");
                                    }
                                    firebaseRefChat.child(idPost).child("username").setValue(yangikut);
                                }

                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }
                            });
                            firebaseRefPost.child(idPost).child("withWho").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    ArrayList<String> yangikut = (ArrayList<String>) dataSnapshot.getValue();
                                    if (yangikut.contains(mUsername)) {
                                        yangikut.remove(mUsername);
                                    }
                                    if (yangikut.contains("")) {
                                        yangikut.remove("");
                                    }
                                    if (yangikut.isEmpty()) {
                                        yangikut.add("");
                                    }
                                    firebaseRefPost.child(idPost).child("withWho").setValue(yangikut);
                                }

                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }
                            });
                            finish();
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {

                        }
                    });
                }
                else
                    Toast.makeText(ChatActivity2.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return;
        } else {
            mDatabase.child("users").child(mFirebaseUser.getUid()).addListenerForSingleValueEvent(
                    new com.google.firebase.database.ValueEventListener() {
                        @Override
                        public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                            // Get user value
                            User user = dataSnapshot.getValue(User.class);
                            //String name = user.getNama();
                            mUsername = user.getUsername();
                            mNama = user.getNama();
                            //mUsername = getusername;
                            //String birthdate = user.getBirthdate();
                            //String email = user.getEmail();
                            //String number = user.getPhone();
                            //String gender = user.getGender();
                            mPhotoUrl = user.getProfpic();
                            //Uri myuri = Uri.parse(imageurl);
                            /*nickText.setText(name);
                            userText.setText(getusername);
                            birthText.setText(birthdate);
                            emailText.setText(email);
                            numbText.setText(number);
                            genderText.setText(gender);*/
                            //Toast.makeText(edit_profile.this, mAuth.getCurrentUser().getProviderId(),Toast.LENGTH_SHORT).show();
                            setList();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.w("!Error", "getUser:onCancelled", databaseError.toException());
                        }
                    }
            );
            mToolbar = (Toolbar) findViewById(R.id.toolbar1);
            mToolbar.setNavigationIcon(R.drawable.ic_action_name_white);
            setSupportActionBar(mToolbar);

            getSupportActionBar().setTitle(null);
            // Get a support ActionBar corresponding to this toolbar
            ActionBar ab = getSupportActionBar();

            // Enable the Up button
            ab.setDisplayHomeAsUpEnabled(true);

        }
        mFirebaseRef = new Firebase(ConfigManager.FIREBASE).child("message").child(roomid).child("chat");
        iniFrame = (FrameLayout) findViewById(R.id.iniFrame);
        iniFrame.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent j = new Intent(ChatActivity2.this, ChatDetailsActivity.class);
                //roomid = intent.getStringExtra("idroom");
                //namaroom = intent.getStringExtra("nroom");
                // namaroom2 = intent.getStringExtra("nroom");
                //wakturoom
                j.putExtra("idroom",roomid);
                j.putExtra("nroom",namaroom);
                j.putExtra("wroom",wakturoom);
                startActivityForResult(j,2);
                //finish();
                //o.putExtra("wroom",post.getDateTime());
            }
        });
//        listView.setOnClickListener(null);
        //mFirebaseRef = mDatabase.child("message").child(roomid).child("chat");
    }
    @Override
    protected void onStart() {
        super.onStart();
        if(!isNetworkAvailable())
        {
            Toast.makeText(ChatActivity2.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    protected void onResume() {
        super.onResume();
        if(!isNetworkAvailable())
        {
            Toast.makeText(ChatActivity2.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //Intent j = new Intent(ChatActivity2.this, LobbyChatActivity.class);
                //j.putExtra("username",mUsername);
                //startActivity(j);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        //Intent i = new Intent(ChatActivity2.this, LobbyChatActivity.class);
        //i.putExtra("username",mUsername);
        //startActivity(i);
        //overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
        finish();
    }
    public void setList() {
        // Setup our view and list adapter. Ensure it scrolls to the bottom as data changes
        listView.setDivider(null);
        listView.post(new Runnable(){
            public void run() {
                listView.setSelection(listView.getCount() - 1);
            }});
        // Tell our list adapter that we only want 50 messages at a time
        mChatListAdapter = new ListAdapterChat(mFirebaseRef, this, R.layout.item_message2, mUsername);
        listView.setAdapter(mChatListAdapter);
        mChatListAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(mChatListAdapter.getCount() - 1);
            }
        });
    }
    private void sendMessage() {
        EditText inputText = (EditText) findViewById(R.id.messageEditText);
        String input = inputText.getText().toString();
        if (!input.equals("")) {
            // Create our 'model', a Chat object
            //String text, String username, String profpic, String timestamp, String name
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
            FriendlyMessage chat = new FriendlyMessage(input, mUsername, mPhotoUrl, timeStamp, mNama);
            // Create a new, auto-generated child of that chat location, and save our chat data there
            mFirebaseRef.push().setValue(chat);
            inputText.setText("");
        }
    }
}
