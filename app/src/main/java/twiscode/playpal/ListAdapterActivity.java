package twiscode.playpal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import twiscode.playpal.Utilities.ConfigManager;

/**
 * Created by Romario on 29/06/2016.
 */
public class ListAdapterActivity extends FirebaseListAdapterActivity{
    Context context;
    TwoFragment mActivity;
    Firebase firebaseRefRoot = new Firebase(ConfigManager.FIREBASE);

    public ListAdapterActivity(Context context, Query ref, Activity activity, int layout, Follow mFollow, TwoFragment mFragment) {
        super(ref, Activities.class, layout, activity, mFollow);
        this.context = context;
        mActivity =  mFragment;
    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    @Override
    protected void populateView(final View view, final Activities activity, final Follow mFollow) {

        if(activity.getActivity().equals("Welcome")){
            final TextView statusReq = (TextView) view.findViewById(R.id.status_req);
            statusReq.setVisibility(View.INVISIBLE);
            final TextView namaActivity = (TextView) view.findViewById(R.id.nama_activity);
            final TextView activityText = (TextView) view.findViewById(R.id.activity_text);
            final LinearLayout centerActivity = (LinearLayout) view.findViewById(R.id.activity_center_layout);
            final LinearLayout leftActvity = (LinearLayout) view.findViewById(R.id.activity_left_layout);
            leftActvity.setVisibility(View.GONE);
            centerActivity.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT,5f));
            activityText.setText("Start your amazing journey with all of your pals around the world using PlayPal!!!");
            namaActivity.setText("Welcome to You by PlayPal team");
            final RelativeLayout rightActivity = (RelativeLayout) view.findViewById(R.id.activity_right_layout);
            rightActivity.setVisibility(View.INVISIBLE);
            final TextView place = (TextView) view.findViewById(R.id.place_activity);
            place.setVisibility(View.GONE);
            final RelativeLayout followBtn = (RelativeLayout) view.findViewById(R.id.follow_button_activity);
            followBtn.setVisibility(View.GONE);
            final ImageView imageNews = (ImageView) view.findViewById(R.id.image_news_activity);
            imageNews.setVisibility(View.GONE);
            final LinearLayout layoutJoinReq = (LinearLayout) view.findViewById(R.id.layout_join_req_activity);
            layoutJoinReq.setVisibility(View.GONE);

        }else {
            final Firebase firebaseUserRef = new Firebase(ConfigManager.FIREBASE).child("users").child(activity.getUserIdRequesting());
            final Firebase firebaseUserRef1 = new Firebase(ConfigManager.FIREBASE).child("users");
            final Firebase firebaseRef = new Firebase(ConfigManager.FIREBASE);
            final Firebase firebasePostRef = firebaseRef.child("posts");
            final TextView statusReq = (TextView) view.findViewById(R.id.status_req);
            statusReq.setVisibility(View.INVISIBLE);
            final ImageView profpic = (ImageView) view.findViewById(R.id.profpic_activity);
            final RelativeLayout followBtn = (RelativeLayout) view.findViewById(R.id.follow_button_activity);
            final TextView namaActivity = (TextView) view.findViewById(R.id.nama_activity);
            final TextView activityText = (TextView) view.findViewById(R.id.activity_text);
            activityText.setText(activity.getActivity());
            followBtn.setVisibility(View.INVISIBLE);
            final TextView followBtnText = (TextView) view.findViewById(R.id.follow_button_activity_text);
            final ImageView imageNews = (ImageView) view.findViewById(R.id.image_news_activity);
            imageNews.setVisibility(View.INVISIBLE);
            final LinearLayout layoutJoinReq = (LinearLayout) view.findViewById(R.id.layout_join_req_activity);
            final ImageView rejectJoinReq = (ImageView) view.findViewById(R.id.reject_join_activity);
            final ImageView acceptJoinReq = (ImageView) view.findViewById(R.id.accept_join_activity);
            final TextView placeActivity = (TextView) view.findViewById(R.id.place_activity);
            placeActivity.setVisibility(View.GONE);
            layoutJoinReq.setVisibility(View.INVISIBLE);
            firebaseUserRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    final User someone = dataSnapshot.getValue(User.class);
                    if(!someone.getProfpic().isEmpty()){
                        Picasso.with(context).load(someone.getProfpic()).transform(new CircleTransform()).into(profpic);
                    }
                    namaActivity.setText(someone.getNama());
                    namaActivity.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent t = new Intent(context ,ProfileUser.class);
                            t.putStringArrayListExtra("following", mFollow.getFollowing());
                            t.putExtra("userId",someone.getId());
                            t.putExtra("username",someone.getUsername());
                            context.startActivity(t);
                        }
                    });

                    if (activity.getActivity().equals("Request to Join you at")) {
                        final Firebase firebasePostRef = new Firebase(ConfigManager.FIREBASE).child("posts").child(activity.getIdTarget());
                        placeActivity.setVisibility(View.VISIBLE);
                        firebasePostRef.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                final posts thisPost = dataSnapshot.getValue(posts.class);
                                placeActivity.setText(thisPost.getGoingTo());
                                placeActivity.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        if(!isNetworkAvailable()){
                                            Toast.makeText(context, "No Network Connection!", Toast.LENGTH_SHORT).show();
                                        }
                                        else {
                                            Intent intent = new Intent(context, PostedActivity.class);
                                            intent.putExtra("userId", thisPost.getUserId());
                                            intent.putExtra("postId", thisPost.getPostId());
                                            context.startActivity(intent);
                                        }
                                    }
                                });
                            }

                            @Override
                            public void onCancelled(FirebaseError firebaseError) {

                            }
                        });
                        if(!activity.getTarget().equals("Accepted") && !activity.getTarget().equals("Rejected")){
                            layoutJoinReq.setVisibility(View.VISIBLE);
                            rejectJoinReq.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!isNetworkAvailable()) {
                                        Toast.makeText(context, "No Network Connection!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        final Firebase firebasePostRef = new Firebase(ConfigManager.FIREBASE).child("posts").child(activity.getIdTarget());
                                        firebasePostRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                posts post = dataSnapshot.getValue(posts.class);
                                                ArrayList<String> waiting = post.getWaitingRecipients();
                                                waiting.remove(activity.getUserIdRequesting());
                                                post.setWaitingRecipients(waiting);
                                                firebasePostRef.setValue(post);
                                            }

                                            @Override
                                            public void onCancelled(FirebaseError firebaseError) {

                                            }
                                        });
                                        //di isi di sini
                                        Activities newActivity = activity;
                                        newActivity.setTarget("Rejected");
                                        firebaseRef.child("activity").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(activity.getIdActivity()).setValue(newActivity);
                                    }
                                }
                            });
                            acceptJoinReq.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!isNetworkAvailable()) {
                                        Toast.makeText(context, "No Network Connection!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        final Firebase firebasePostRef = new Firebase(ConfigManager.FIREBASE).child("posts").child(activity.getIdTarget());
                                        firebasePostRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                posts post = dataSnapshot.getValue(posts.class);
                                                ArrayList<String> waiting = post.getWaitingRecipients();
                                                waiting.remove(someone.getUsername());
                                                post.setWaitingRecipients(waiting);
                                                Log.v("cobawithWho", post.getWithWho().toString());
                                                ArrayList<String> withWho = post.getWithWho();
                                                withWho.add(someone.getUsername());
                                                Log.v("cobawithWhoLagi", withWho.toString());
                                                post.setWithWho(withWho);
                                                Log.v("cobaWithWho2", post.getWithWho().toString());
                                                final posts post2 = post;
                                                final posts post1 = post;
                                                final ArrayList<String> withWho1 = withWho;
                                                Log.v("cobaWithWho3", post1.getWithWho().toString());
                                                firebaseUserRef1.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("username").addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                        //Log.v("coba2lagi",dataSnapshot.getRef().getPath().toString());
                                                        String usernameUser = dataSnapshot.getValue(String.class);
                                                        //Log.v("cobacobalah", usernameUser);
                                                        firebasePostRef.setValue(post1);
                                                        ArrayList<String> withWhoAndMe = post2.getWithWho();
                                                        Log.v("cobaWithWho4", withWhoAndMe.toString());
                                                        //Log.v("cobacobalahLagi", withWhoAndMe.toString());
                                                        withWhoAndMe.add(usernameUser);
                                                        Firebase firebaseChatRef = new Firebase(ConfigManager.FIREBASE).child("message").child(activity.getIdTarget()).child("username");
                                                        firebaseChatRef.setValue(withWhoAndMe);

                                                    }

                                                    @Override
                                                    public void onCancelled(FirebaseError firebaseError) {

                                                    }
                                                });
                                            }

                                            @Override
                                            public void onCancelled(FirebaseError firebaseError) {

                                            }
                                        });
                                        //di isi di sini
                                        Activities newActivity = activity;
                                        newActivity.setTarget("Accepted");
                                        firebaseRef.child("activity").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child(activity.getIdActivity()).setValue(newActivity);
                                    }
                                }
                            });
                        }else{
                            statusReq.setVisibility(View.VISIBLE);
                            statusReq.setText(activity.getTarget());
                        }

                    } else if (activity.getActivity().equals("Started following you")) {
                        followBtn.setVisibility(View.VISIBLE);
                        final Firebase firebaseRef = new Firebase(ConfigManager.FIREBASE).child("follow");
                        if (mFollow.getFollowing().contains(someone.getId())) {
                            followBtn.setBackgroundResource(R.drawable.button_following);
                            followBtnText.setText("Unfollow");
                            followBtnText.setTextColor(Color.WHITE);
                            followBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!isNetworkAvailable()) {
                                        Toast.makeText(context, "No Network Connection!", Toast.LENGTH_SHORT).show();
                                    } else {
                                        firebaseRef.child(someone.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                Follow follow2 = dataSnapshot.getValue(Follow.class);
                                                ArrayList<String> followers = follow2.getFollowers();
                                                followers.remove(FirebaseAuth.getInstance().getCurrentUser().getUid());
                                                follow2.setFollowers(followers);
                                                //firebaseRef.child(someone.getId()).setValue(follow2);
                                                mActivity.makeConfirm(FirebaseAuth.getInstance().getCurrentUser().getUid(), someone.getId(), someone.getUsername(), mFollow, follow2);
                                            }

                                            @Override
                                            public void onCancelled(FirebaseError firebaseError) {

                                            }
                                        });
                                    }
                                }
                            });

                        } else {

                            followBtn.setBackgroundResource(R.drawable.button_follow);
                            followBtnText.setText("+ Follow");
                            followBtnText.setTextColor(view.getResources().getColor(R.color.errorColor));
                            followBtn.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (!isNetworkAvailable()) {
                                        Toast.makeText(context, "No Network Connection!", Toast.LENGTH_SHORT).show();
                                    }else {
                                        ArrayList<String> following = mFollow.getFollowing();
                                        Follow follow1 = mFollow;
                                        following.add(someone.getId());
                                        follow1.setFollowing(following);
                                        firebaseRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).setValue(follow1);
                                        firebaseRef.child(someone.getId()).addListenerForSingleValueEvent(new ValueEventListener() {
                                            @Override
                                            public void onDataChange(DataSnapshot dataSnapshot) {
                                                Follow follow2 = dataSnapshot.getValue(Follow.class);
                                                ArrayList<String> followers = follow2.getFollowers();
                                                followers.add(FirebaseAuth.getInstance().getCurrentUser().getUid());
                                                follow2.setFollowers(followers);
                                                firebaseRef.child(someone.getId()).setValue(follow2);
                                                Firebase pushFirebaseRef = firebaseRefRoot.child("activity").child(someone.getId()).push();
                                                final String idActivity = pushFirebaseRef.getKey();
                                                Long timeStamp = System.currentTimeMillis() / 1000 * (-1);
                                                Activities newActivity = new Activities(someone.getId(), "Started following you", FirebaseAuth.getInstance().getCurrentUser().getUid(), "follow", "", idActivity, timeStamp);
                                                pushFirebaseRef.setValue(newActivity);
                                            }

                                            @Override
                                            public void onCancelled(FirebaseError firebaseError) {

                                            }
                                        });
                                        mActivity.dialogInterface("Following");
                                        mActivity.refreshAdapter(follow1, "Following...");
                                    }
                                }
                            });
                        }
                    } else if (activity.getActivity().equals("Invite you to join")||activity.getActivity().equals("Has Joined you at")||activity.getActivity().equals("Has Join you at")||activity.getActivity().equals("Liked your post")) {
                        imageNews.setVisibility(View.VISIBLE);
                        firebasePostRef.child(activity.getIdTarget()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                final posts post = dataSnapshot.getValue(posts.class);
                                if(post == null)
                                {
                                    return;
                                }
                                else {
                                    String imageUrl = post.getImageURL();
                                    if (!imageUrl.isEmpty()) {
                                        Picasso.with(context).load(imageUrl).into(imageNews);
                                    }
                                    imageNews.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(context, PostedActivity.class);
                                            intent.putExtra("userId", post.getUserId());
                                            intent.putExtra("postId", post.getPostId());
                                            context.startActivity(intent);
                                        }
                                    });
                                }

                            }

                            @Override
                            public void onCancelled(FirebaseError firebaseError) {

                            }
                        });
                    }
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
        }
    }
}
