package twiscode.playpal;

/**
 * Created by michael on 29/03/2016.
 */
public class news_model {
    private String nama, alamat, jumlahlike, jumlahcomment, waktu;
    public news_model(String nama, String alamat, String jumlahlike, String jumlahcomment, String waktu) {
    this.nama=nama;
    this.alamat=alamat;
    this.jumlahcomment=jumlahcomment;
    this.jumlahlike=jumlahlike;
    this.waktu=waktu;}

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public void setJumlahlike(String jumlahlike) {
        this.jumlahlike = jumlahlike;
    }

    public void setJumlahcomment(String jumlahcomment) {
        this.jumlahcomment = jumlahcomment;
    }

    public void setWaktu(String waktu) {
        this.waktu = waktu;
    }

    public String getNama() {
        return nama;
    }

    public String getWaktu() {
        return waktu;
    }

    public String getJumlahcomment() {
        return jumlahcomment;
    }

    public String getJumlahlike() {
        return jumlahlike;
    }

    public String getAlamat() {
        return alamat;
    }

}

