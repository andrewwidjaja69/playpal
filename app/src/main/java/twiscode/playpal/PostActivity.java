package twiscode.playpal;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.location.Location;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import twiscode.playpal.Utilities.ConfigManager;


/**
 * Created by aldo on 04/04/2016.
 */
public class PostActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    Firebase firebaseRef;
    Firebase pushFirebaseRef;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl(ConfigManager.FIREBASE_STORAGE);
    private LinearLayout addpeople;
    private LinearLayout adddate;
    private FirebaseAuth mAuth;
    private int PICK_IMAGE_REQUEST = 1;
    private int PEOPLE_SELECTED = 2;
    private int CROP_IMAGE = 3;
    private LinearLayout addphoto;
    private ImageView addphotocontainer;
    private View line_addphoto;
    public static TextView SelectedDateView;
    private static int Year;
    private static int Month;
    private static int Day;
    private static int hour;
    private static int Minute;
    ArrayList<String> withWho = new ArrayList<String>();
    TextView addpeople_text;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private static final int REQUEST_WRITE_STORAGE = 2;

    String privateStatus = null;
    String textGointToString;
    private String datetime;
    public static String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private Uri selectedImage;
    String uriDownload = null;
    ConfigManager appManager;
    ProgressDialog mAuthProgressDialog;
    User someone = new User();
    ArrayList<String> meWithWho = new ArrayList<String>();
    byte[] dataFinal;
    AutoCompleteTextView atvPlaces;
    PlacesTask placesTask;
    ParserTask parserTask;
    private Uri mCropImagedUri;
    private Uri mFinalImageUri;
    LocationManager mLocationManager;
    float minDistance = (float) 1;
    long minTime = (long) 10;
    ArrayList<Double> lat = new ArrayList<Double>();
    ArrayList<Double> lon = new ArrayList<Double>();
    int w;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    /*final LocationListener mLocationListener = new LocationListener() {
        @Override
        public void onLocationChanged(final Location location) {
            //your code here
            lat.add(0, location.getLatitude());
            lon.add(0, location.getLongitude());
            Toast.makeText(PostActivity.this, String.valueOf(lat.get(0)+" - "+String.valueOf(lon.get(0))), Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };*/

    /**Crop the image
     * @return returns <tt>true</tt> if crop supports by the device,otherwise false*/
    private boolean performCropImage() {
        try {
            if (mFinalImageUri != null) {
                //call the standard crop action intent (the user device may not support it)
                Intent cropIntent = new Intent("com.android.camera.action.CROP");
                //indicate image type and Uri
                cropIntent.setDataAndType(mFinalImageUri, "image/*");
                //set crop properties
                cropIntent.putExtra("crop", "true");
                //indicate aspect of desired crop
                cropIntent.putExtra("scaleType", "centerCrop");
                cropIntent.putExtra("aspectX", 1);
                cropIntent.putExtra("aspectY", 1);
                cropIntent.putExtra("scale", true);
                cropIntent.putExtra("outputX", 800);
                cropIntent.putExtra("outputY", 800);
                //indicate output X and Y
                //retrieve data on return
                cropIntent.putExtra("return-data", true);

                File f = createNewFile("CROP_");
                try {
                    f.createNewFile();
                } catch (IOException ex) {
                    Log.e("io", ex.getMessage());
                }

                mCropImagedUri = Uri.fromFile(f);
                Log.v("coba3", mCropImagedUri.toString());
                cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImagedUri);
                //start the activity - we handle returning in onActivityResult
                startActivityForResult(cropIntent, CROP_IMAGE);
                return true;
            }
        } catch (ActivityNotFoundException anfe) {
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
            return false;
        }

        return false;
    }

    private File createNewFile(String prefix) {
        if (prefix == null || "".equalsIgnoreCase(prefix)) {
            prefix = "IMG_";
        }
        File newDirectory = new File(Environment.getExternalStorageDirectory() + "/mypics/");
        if (!newDirectory.exists()) {
            if (newDirectory.mkdir()) {
                Log.d(getApplicationContext().getClass().getName(), newDirectory.getAbsolutePath() + " directory created");
            }
        }
        File file = new File(newDirectory, (prefix + System.currentTimeMillis() + ".jpg"));
        if (file.exists()) {
            //this wont be executed
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        /*EditText textGoingTo = (EditText) findViewById(R.id.textView);
        textGointToString = textGoingTo.getText().toString();
        CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);
        if (checkBox.isChecked()) {
            privateStatus = "True";
        } else {
            privateStatus = "False";
        }
        Log.v("TEXT_GOING_1", textGointToString);
        outState.putString("TEXT_GOING",textGointToString);
        Log.v("TEXT_GOING", outState.getString("TEXT_GOING"));
        Toast.makeText(PostActivity.this, outState.getString("TEXT_GOING"), Toast.LENGTH_SHORT).show();*/
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        /*EditText textGoingTo = (EditText) findViewById(R.id.textView);
        textGoingTo.setText(savedInstanceState.getString("TEXT_GOING"));
        Toast.makeText(PostActivity.this, savedInstanceState.getString("TEXT_GOING"), Toast.LENGTH_SHORT).show();*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        firebaseRef = new Firebase(appManager.FIREBASE);
        withWho.add("");
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }

        /*if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            Toast.makeText(PostActivity.this, "Rejected", Toast.LENGTH_SHORT).show();

            return;
        }else {
            Toast.makeText(PostActivity.this, "Accepted", Toast.LENGTH_SHORT).show();

            mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
            mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime,
                    minDistance, mLocationListener);
        }*/
        setContentView(R.layout.post);
        atvPlaces = (AutoCompleteTextView) findViewById(R.id.atv_places);
        if (atvPlaces != null) {
            atvPlaces.setThreshold(1);
        }

        atvPlaces.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                placesTask = new PlacesTask();
                placesTask.execute(s.toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable s) {
                // TODO Auto-generated method stub
            }
        });

        SelectedDateView = (TextView) findViewById(R.id.selected_date);
        addpeople = (LinearLayout) findViewById(R.id.addpeople);
        addpeople_text = (TextView) findViewById(R.id.addpeople_text);
        addpeople.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                firebaseRef.child("follow").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Follow follow = dataSnapshot.getValue(Follow.class);
                        ArrayList<String> following = follow.getFollowing();
                        ArrayList<String> followers = follow.getFollowers();
                        Intent i = new Intent(getApplicationContext(), SearchPeoplePost1.class);
                        i.putStringArrayListExtra("array", withWho);
                        i.putStringArrayListExtra("following", following);
                        i.putStringArrayListExtra("followers", followers);
                        startActivityForResult(i, PEOPLE_SELECTED);
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });


            }
        });


        addphoto = (LinearLayout) findViewById(R.id.addphoto);
        addphoto.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if(isNetworkAvailable()) {
                    reqPermission();
                    Intent intent = new Intent();
                    // Show only images, no videos or anything else
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    // Always show the chooser (if there are multiple options available)
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
                }
                else
                    Toast.makeText(PostActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });
        addphotocontainer = (ImageView) findViewById(R.id.postImg);
        //ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) addphotocontainer.getLayoutParams();
        addphotocontainer.setMinimumHeight(w);
        addphotocontainer.setMaxHeight(w);
        //params.height = w;
        //addphotocontainer.setLayoutParams(params);
        addphotocontainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    reqPermission();
                    Intent intent = new Intent();
                    // Show only images, no videos or anything else
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    // Always show the chooser (if there are multiple options available)
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
                }
                else
                    Toast.makeText(PostActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });
        line_addphoto = (View) findViewById(R.id.line_addphoto);


    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    /** A method to download json data from url */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            //Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    protected void onStart() {
        if(!isNetworkAvailable()){
            Toast.makeText(PostActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
        else mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            lat.add(0,mLastLocation.getLatitude());
            lon.add(0,mLastLocation.getLongitude());
            //Toast.makeText(PostActivity.this, String.valueOf(lat.get(0))+" - "+String.valueOf(lon.get(0)), Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    // Fetches all places from GooglePlaces AutoComplete Web Service
    private class PlacesTask extends AsyncTask<String, Void, String>{

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
            String key = "key="+appManager.API_KEY;

            String input="";
            Log.v("mencobaInput", place[0].toString());
            input = "input=" + place[0].toString();

            // place type to be searched
            String types = "types=establishment";

            // Sensor enabled
            String sensor = "sensor=true";
            String radius = "radius=10000";
            String library = "libraries=places";

            String parameters = input+"&"+radius+"&"+sensor+"&"+key;

            if(lat.size()!=0 || lon.size()!=0) {
                String location = "location=" + String.valueOf(lat.get(0)) + "," + String.valueOf(lon.get(0));
                parameters = input+"&"+radius+"&"+sensor+"&"+location+"&"+key;
            }//String location = "location=-7.2868545,112.7100133";
            //"&"+location+
            // Building the parameters to the web service



            // Output format
            String output = "json";


            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"+output+"?"+parameters;
            url = url.replaceAll(" ","%20");
            Log.v("cobaUrl", url);
            try{
                // Fetching the data from we service
                data = downloadUrl(url);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            Log.v("cobaResult", result);
            // Creating ParserTask
            parserTask = new ParserTask();

            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }
    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>{

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try{
                jObject = new JSONObject(jsonData[0]);
                Log.v("cobaJSON", jObject.toString());
                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

            }catch(Exception e){
                Log.d("Exception",e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            String[] from = new String[] { "description"};
            int[] to = new int[] { android.R.id.text1 };
            // Creating a SimpleAdapter for the AutoCompleteTextView
            ArrayList<String> dataArray = new ArrayList<String>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    String res = result.get(i).toString();
                    String resFinal = res.substring(res.lastIndexOf("description=") + 12);
                    resFinal = resFinal.replace("}", "");
                    dataArray.add(resFinal);
                }
            }

            FilterWithSpaceAdapter<String> adapter1;
//...
            adapter1 = new FilterWithSpaceAdapter<String>(PostActivity.this,
                    android.R.layout.simple_list_item_1, dataArray);
            // Setting the adapter
            atvPlaces.setAdapter(adapter1);
        }
    }


    public void reqPermission()
    {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_WRITE_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(PostActivity.this, "The app was not allowed to read to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    //reload my activity with permission granted or use the features what required the permission
                } else
                {
                    Toast.makeText(PostActivity.this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
//                    SelectedDateView.setText("Selected Date: " + (month + 1) + "-" + day + "-" + year);
//            Toast.makeText(PostActivity.this,"Selected Date: " + (month + 1) + "-" + day + "-" + year, Toast.LENGTH_SHORT ).show();
            Day = day;
            Month = month;
            Year = year;
            showTimePickerDialog(view);
        }
    }

    public static TimePicker timePicker;

    public class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            hour = hourOfDay;
            Minute = minute;
            setDateTime();
        }
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }

    public void setDateTime() {
        String menittemp="00";
        String jamtemp="00";
        if(hour<10) {
            jamtemp = "0"+String.valueOf(hour);
        }
        else
        {
            jamtemp=String.valueOf(hour);
        }
        if(Minute<10) {
            menittemp = "0"+String.valueOf(Minute);
        }
        else
        {
            menittemp=String.valueOf(Minute);
        }
        SelectedDateView.setText("Selected Date: " + (MONTHS[Month]) + "-" + Day + "-" + Year + "-" + jamtemp + "." + menittemp);
        datetime = (MONTHS[Month]) + "-" + Day + "-" + Year + "-" + jamtemp + "." + menittemp;
//        Toast.makeText(PostActivity.this,"Selected Date: " + (Month + 1) + "-" + Day + "-" + Year + "-" + hour + "." + Minute, Toast.LENGTH_SHORT ).show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);


        if (requestCode == CROP_IMAGE && data!=null) {

            Bundle extras = data.getExtras();
            if(extras != null ) {

                Bitmap selectedBitmap = null;
                try {
                    selectedBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),mCropImagedUri);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    dataFinal = baos.toByteArray();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            addphoto.setVisibility(View.GONE);
            line_addphoto.setVisibility(View.GONE);
            addphotocontainer.setVisibility(View.VISIBLE);
                Log.v("cobaImage", dataFinal.toString());
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x;
                //int height = size.y;
            addphotocontainer.setImageBitmap(selectedBitmap);
                addphotocontainer.getLayoutParams().height = width;
                addphotocontainer.requestLayout();
//                    addphotocontainer.setImageBitmap(BitmapFactory.decodeFile(picturePath));

            }
        }
        if (requestCode == CROP_IMAGE && resultCode != RESULT_OK) {
            addphoto.setVisibility(View.VISIBLE);
        }

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){
            mFinalImageUri = data.getData();
            performCropImage();
            Log.v("mencobalagi",mCropImagedUri.toString());
            Log.v("mencobalagi2", mFinalImageUri.toString());

        }

        if (requestCode == PEOPLE_SELECTED) {
            if(resultCode == RESULT_OK){
                if (data != null) {
                    ArrayList<String> peopleSelected = data.getStringArrayListExtra("result");
                    StringBuilder sb = new StringBuilder();
                    if(peopleSelected.size()>2){
                        for(int i=0;i<2;i++){
                            sb.append(peopleSelected.get(i));
                            sb.append(", ");
                        }
                        sb.append("and ");
                        sb.append(peopleSelected.size()-2);
                        sb.append(" more");
                    }else{
                        for(int i=0;i<peopleSelected.size();i++){
                            sb.append(peopleSelected.get(i));
                            if(i!=peopleSelected.size()-1){
                                sb.append(", ");
                            }
                        }
                    }
                    addpeople_text.setText(sb.toString());
                    withWho = peopleSelected;
                    if(withWho.size()>1){
                        withWho.remove("");
                    }
                }
            }

        }
    }

    @Override
    public void onBackPressed() {
        //Intent i = new Intent(PostActivity.this, MainActivity.class);

        //startActivity(i);
        //overridePendingTransition(R.anim.slide_out, R.anim.slide_in);

        finish();
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }


    public void onButtonPostPressed(View v) {

        if(isNetworkAvailable()) {
            EditText textGoingTo = (EditText) findViewById(R.id.atv_places);
            textGointToString = textGoingTo.getText().toString();
            CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);
            final String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
            final Long ts = System.currentTimeMillis() / 1000 * (-1);
            final int size = withWho.size();
            //final String ts = tsLong.toString();

            if (checkBox.isChecked()) {
                privateStatus = "True";
            } else {
                privateStatus = "False";
            }

            Toast.makeText(PostActivity.this, privateStatus, Toast.LENGTH_SHORT).show();

            if (dataFinal != null) {
                StorageReference riversRef = storageRef.child("images/" + "IMG_CROP" + timeStamp);
                UploadTask uploadTask = riversRef.putBytes(dataFinal);

                // Register observers to listen for when the download is done or if it fails
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        uriDownload = downloadUrl.toString();
                        Toast.makeText(PostActivity.this, uriDownload, Toast.LENGTH_SHORT).show();

                        ArrayList<String> liker = new ArrayList<String>();


                        if (user != null) {
                            Toast.makeText(PostActivity.this, "Post Success!", Toast.LENGTH_SHORT).show();
                            //Toast.makeText(PostActivity.this, authData.getToken(), Toast.LENGTH_SHORT ).show();
                            Log.v("mencoba", firebaseRef.child("users").orderByChild(user.getUid()).startAt(user.getUid()).endAt(user.getUid() + "\uf8ff").getPath().toString());
                            pushFirebaseRef = firebaseRef.child("posts").push();
                            final String postId = pushFirebaseRef.getKey();
                            final posts post = new posts(textGointToString, uriDownload, withWho, datetime, privateStatus, user.getUid(), postId, ts);
                            liker.add("");
                            ArrayList<String> waiting = new ArrayList<String>();
                            waiting.add("");
                            post.setWaitingRecipients(waiting);
                            post.setLikers(liker);
                            pushFirebaseRef.setValue(post);
                            for (int i = 0; i < size; i++) {
                                String username = withWho.get(i);
                                if (username.isEmpty()) {

                                } else {
                                    firebaseRef.child("users").orderByChild("username").startAt(username).endAt(username + "\uf8ff").addChildEventListener(new ChildEventListener() {
                                        @Override
                                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                            User someone = dataSnapshot.getValue(User.class);
                                            Firebase pushFirebaseRef = firebaseRef.child("activity").child(someone.getId()).push();
                                            final String idActivity = pushFirebaseRef.getKey();
                                            Long timeStamp = System.currentTimeMillis() / 1000 * (-1);
                                            Activities newActivity = new Activities(someone.getId(), "Invite you to join", FirebaseAuth.getInstance().getCurrentUser().getUid(), "posts", postId, idActivity, timeStamp);
                                            pushFirebaseRef.setValue(newActivity);
                                        }

                                        @Override
                                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                                        }

                                        @Override
                                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onCancelled(FirebaseError firebaseError) {

                                        }
                                    });
                                }
                            }

                            firebaseRef.child("users").child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    someone = dataSnapshot.getValue(User.class);
                                    FriendlyMessage chat = new FriendlyMessage("Hey, let's join me going to " + post.getGoingTo(), someone.getUsername(), someone.getProfpic(), timeStamp, someone.getNama());
                                    meWithWho = withWho;
                                    meWithWho.add(someone.getUsername());
                                    Log.v("mencoba lgi", someone.getUsername());
                                    firebaseRef.child("message").child(postId).child("chat").push().setValue(chat);
                                    firebaseRef.child("message").child(postId).child("username").setValue(meWithWho);

                                }

                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }
                            });

                        } else {
                            Toast.makeText(PostActivity.this, "Failed to Post, please Login first!", Toast.LENGTH_SHORT).show();
                        }


                        //Intent i = new Intent(PostActivity.this, MainActivity.class);
                        //startActivity(i);
                        finish();

                    }
                });

                mAuthProgressDialog = new ProgressDialog(this);
                mAuthProgressDialog.setTitle("Loading");
                mAuthProgressDialog.setMessage("Posting your Post...");
                mAuthProgressDialog.setCancelable(false);
                mAuthProgressDialog.show();


            } else {
                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                ArrayList<String> liker = new ArrayList<String>();


                if (user != null) {
                    Toast.makeText(PostActivity.this, "Post Success!", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(PostActivity.this, authData.getToken(), Toast.LENGTH_SHORT ).show();
                    pushFirebaseRef = firebaseRef.child("posts").push();
                    final String postId = pushFirebaseRef.getKey();
                    final posts post = new posts(textGointToString, uriDownload, withWho, datetime, privateStatus, user.getUid(), postId, ts);
                    liker.add("");
                    ArrayList<String> waiting = new ArrayList<String>();
                    waiting.add("");
                    post.setWaitingRecipients(waiting);
                    post.setLikers(liker);
                    pushFirebaseRef.setValue(post);
                    for (int i = 0; i < size; i++) {
                        String username = withWho.get(i);
                        if (username.isEmpty()) {

                        } else {
                            firebaseRef.child("users").orderByChild("username").startAt(username).endAt(username + "\uf8ff").addChildEventListener(new ChildEventListener() {
                                @Override
                                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                    User someone = dataSnapshot.getValue(User.class);
                                    Firebase pushFirebaseRef = firebaseRef.child("activity").child(someone.getId()).push();
                                    final String idActivity = pushFirebaseRef.getKey();
                                    Long timeStamp = System.currentTimeMillis() / 1000 * (-1);
                                    Activities newActivity = new Activities(someone.getId(), "Invite you to join", FirebaseAuth.getInstance().getCurrentUser().getUid(), "posts", postId, idActivity, timeStamp);
                                    pushFirebaseRef.setValue(newActivity);
                                }

                                @Override
                                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                }

                                @Override
                                public void onChildRemoved(DataSnapshot dataSnapshot) {

                                }

                                @Override
                                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                }

                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }
                            });
                        }
                    }

                    firebaseRef.child("users").child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            someone = dataSnapshot.getValue(User.class);
                            FriendlyMessage chat = new FriendlyMessage("Hey, let's join me going to " + post.getGoingTo(), someone.getUsername(), someone.getProfpic(), timeStamp, someone.getNama());
                            meWithWho = withWho;
                            meWithWho.add(someone.getUsername());
                            meWithWho.remove("");
                            Log.v("mencoba lgi", someone.getUsername());
                            firebaseRef.child("message").child(postId).child("chat").push().setValue(chat);
                            firebaseRef.child("message").child(postId).child("username").setValue(meWithWho);
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {

                        }
                    });

                } else {
                    Toast.makeText(PostActivity.this, "Failed to Post, please Login first!", Toast.LENGTH_SHORT).show();
                }
                //Intent i = new Intent(PostActivity.this, MainActivity.class);
                //startActivity(i);
                finish();
            }
        }
        else
            Toast.makeText(PostActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
    }

}