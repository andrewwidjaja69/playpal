package twiscode.playpal;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class edit_profile extends AppCompatActivity{

    Uri downloadUrl;
    //String sdownload = "1";
    private final static int COARSE_LOCATION_RESULT = 100;
    private final static int FINE_LOCATION_RESULT = 101;
    private final static int CALL_PHONE_RESULT = 102;
    private final static int CAMERA_RESULT = 103;
    private final static int READ_CONTACTS_RESULT = 104;
    private final static int WRITE_EXTERNAL_RESULT = 105;
    private final static int RECORD_AUDIO_RESULT = 106;
    private final static int ALL_PERMISSIONS_RESULT = 107;
    private Uri selectedImage;
    private Toolbar toolbar;
    private Toolbar mToolbar;
    String imageurl;
    String getusername="";
    private Toolbar mToolbar1;
    private ActionBar actionBar;
    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private NavigationView navView;
    private ProgressDialog mAuthProgressDialog;
    private String uid;
    private EditText nickText;
    private EditText userText;
    private EditText birthText;
    private EditText bioText;
    private EditText emailText;
    private EditText numbText;
    private EditText genderText;
    private ImageView silangImage;
    private ImageView centangImage;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private String providerId;
    private ImageView iniImage;
    private static int RESULT_LOAD_IMAGE = 1;
    private static int CROP_IMAGE = 7;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 198;
    private static int Year;
    private static int Month;
    private static int Day;
    private static int hour;
    private static int Minute;
    private String datetime;
    StorageReference storageRef;
    public static String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private DatePickerDialog fromDatePickerDialog;
    private SimpleDateFormat dateFormatter;
    private Spinner spGender;
    private String genderType[] = {"Male","Female"};
    private ArrayAdapter<String> adapterGenderType;
    private String itemG;
    private Integer rating;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.edit_profile);
        Firebase.setAndroidContext(this);
        FirebaseStorage storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        storageRef = storage.getReferenceFromUrl("gs://sweltering-heat-4415.appspot.com");
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Lato-Regular.ttf");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        //silangImage = (ImageView) findViewById(R.id.silang);
        //centangImage = (ImageView) findViewById(R.id.centang);
        nickText = (EditText) findViewById(R.id.input_name);
        userText = (EditText) findViewById(R.id.input_username);
        //birthText = (EditText) findViewById(R.id.input_birthdate);
        emailText = (EditText) findViewById(R.id.input_email);
        numbText = (EditText) findViewById(R.id.input_phone);
        //genderText = (EditText) findViewById(R.id.input_gender);
        uid = mAuth.getCurrentUser().getUid();
        iniImage = (ImageView) findViewById(R.id.gambar);
        spGender = (Spinner) findViewById(R.id.spGender);
        adapterGenderType = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, genderType);
        adapterGenderType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGender.setAdapter(adapterGenderType);
        spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                // On selecting a spinner item
                itemG = adapter.getItemAtPosition(position).toString();

                // Showing selected spinner item
                //Toast.makeText(getApplicationContext(),
                 //       "Gender : " + itemG, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
        //int loader = R.drawable.loader;
        mAuthProgressDialog = new ProgressDialog(this);
        mAuthProgressDialog.setTitle("Loading");
        mAuthProgressDialog.setMessage("Updating your profile...");
        mAuthProgressDialog.setCancelable(true);
        //CollapsingToolbarLayout.setTitleEnabled(false);
        /*silangImage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (!validateForm()) {
                    return;
                }
                Intent i = new Intent(edit_profile.this, MainActivity.class);
                startActivity(i);
                //overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
                finish();
            }
        });
        centangImage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                updateDataUrl(nickText.getText().toString(),userText.getText().toString(),birthText.getText().toString()
                ,numbText.getText().toString(),genderText.getText().toString(),emailText.getText().toString());
            }
        });*/
        iniImage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    reqPermission();
                    Intent intent = new Intent();
                    // Show only images, no videos or anything else
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    // Always show the chooser (if there are multiple options available)
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_LOAD_IMAGE);
                }
                else
                    Toast.makeText(edit_profile.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            for (UserInfo profile : user.getProviderData()) {
                // Id of the provider (ex: google.com)
                providerId = profile.getProviderId();
            };
        }
        else
        {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return;
        }
        findViewsById();

        setDateTimeField();
        mToolbar = (Toolbar) findViewById(R.id.toolbar1);
        mToolbar.setNavigationIcon(R.drawable.ic_action_name_white);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
        // Get a support ActionBar corresponding to this toolbar
        final ActionBar ab = getSupportActionBar();
        // Enable the Up button

        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        if(isNetworkAvailable()) {
            mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            // Get user value
                            User user = dataSnapshot.getValue(User.class);
                            String name = user.getNama();
                            getusername = user.getUsername();
                            if (getusername.equals("")) {
                                ab.setDisplayHomeAsUpEnabled(false);
                            } else {
                                userText.setKeyListener(null);
                                ab.setDisplayHomeAsUpEnabled(true);
                            }
                            String birthdate = user.getBirthdate();
                            String email = user.getEmail();
                            String number = user.getPhone();
                            String gender = user.getGender();
                            imageurl = user.getProfpic();
                            rating = user.getRating();
                            Uri myuri = Uri.parse(imageurl);
                            nickText.setText(name);
                            userText.setText(getusername);
                            birthText.setText(birthdate);
                            emailText.setText(email);
                            numbText.setText(number);
                            spGender.setSelection((adapterGenderType.getPosition(gender)));
                            //genderText.setText(gender);
                            //Toast.makeText(edit_profile.this, user.getBirthdate(),Toast.LENGTH_SHORT).show();
                            //iniImage.setImageDrawable(LoadImageFromWebOperations(imageurl));
                            //Picasso.with(getApplicationContext()).setLoggingEnabled(true);
                            Picasso.with(getApplicationContext()).setIndicatorsEnabled(false);
                            //Toast.makeText(edit_profile.this, providerId,Toast.LENGTH_SHORT).show();
                            if (imageurl.equals("")) {

                            } else {
                                Picasso.with(getApplicationContext()).load(imageurl).into(iniImage);
                            }
                            //Picasso.with(getApplicationContext()).load("https://".concat(imageurl)).into(iniImage);
                            //Toast.makeText(edit_profile.this, name,
                            //        Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.w("!Error", "getUser:onCancelled", databaseError.toException());
                        }
                    });
        }
        else
            Toast.makeText(edit_profile.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        // Find the toolbar view inside the activity layout


    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    private void findViewsById() {
        birthText = (EditText) findViewById(R.id.input_birthdate);
        birthText.setInputType(InputType.TYPE_NULL);
        //birthText.requestFocus();
    }

    private void setDateTimeField() {
        birthText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    fromDatePickerDialog.show();
                }
            }
        });
        //fromDatePickerDialog.show();
        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                birthText.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_profile, menu);

        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.done:
                if(isNetworkAvailable()) {
                    updateDataUrl(nickText.getText().toString(), userText.getText().toString(), birthText.getText().toString()
                            , numbText.getText().toString(), itemG, emailText.getText().toString());
                    return true;
                }
                else
                    Toast.makeText(edit_profile.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
            case android.R.id.home:
                if(getusername!="" || getusername!=null) {
                    finish();
                    return true;
                }
                else
                {
                    Toast.makeText(edit_profile.this,"Please update your username first!", Toast.LENGTH_SHORT ).show();
                }
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, drawerLayout, mToolbar, R.string.drawer_open,  R.string.drawer_close);
    }

    public void setDateTime() {
        //SelectedDateView.setText("Selected Date: " + (MONTHS[Month]) + "-" + Day + "-" + Year + "-" + hour + "." + Minute);
        datetime = (MONTHS[Month]) + "-" + Day + "-" + Year + "-" + hour + "." + Minute;
//        Toast.makeText(PostActivity.this,"Selected Date: " + (Month + 1) + "-" + Day + "-" + Year + "-" + hour + "." + Minute, Toast.LENGTH_SHORT ).show();
    }
    public void reqPermission()
    {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(edit_profile.this, "The app was not allowed to read to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    //reload my activity with permission granted or use the features what required the permission
                } else
                {
                    Toast.makeText(edit_profile.this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CROP_IMAGE) {
            Bundle extras = data.getExtras();
            if(extras != null ) {

                Bitmap selectedBitmap = null;
                try {
                    selectedBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),selectedImage);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                //dataFinal = baos.toByteArray();
                //addphoto.setVisibility(View.GONE);
                //line_addphoto.setVisibility(View.GONE);
                //addphotocontainer.setVisibility(View.VISIBLE);
                //Log.v("cobaImage", dataFinal.toString());

                iniImage.setImageBitmap(selectedBitmap);
//                    addphotocontainer.setImageBitmap(BitmapFactory.decodeFile(picturePath));


            }
        }
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            selectedImage = data.getData();
            performCropImage();
            Picasso.with(this).load(selectedImage).into(iniImage);
        }
    }
    private boolean performCropImage(){
        try {
            if(selectedImage!=null){
                //call the standard crop action intent (the user device may not support it)
                Intent cropIntent = new Intent("com.android.camera.action.CROP");
                //indicate image type and Uri
                cropIntent.setDataAndType(selectedImage, "image/*");
                //set crop properties
                cropIntent.putExtra("crop", "true");
                //indicate aspect of desired crop
                cropIntent.putExtra("aspectX", 1);
                cropIntent.putExtra("aspectY", 1);
                cropIntent.putExtra("scale", true);
                //indicate output X and Y
                cropIntent.putExtra("outputX", 800);
                cropIntent.putExtra("outputY", 800);
                //retrieve data on return
                cropIntent.putExtra("return-data", true);

                File f = createNewFile("CROP_");
                try {
                    f.createNewFile();
                } catch (IOException ex) {
                    Log.e("io", ex.getMessage());
                }

                selectedImage = Uri.fromFile(f);
                Log.v("coba3",selectedImage.toString());
                cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, selectedImage);
                //start the activity - we handle returning in onActivityResult
                startActivityForResult(cropIntent, CROP_IMAGE);
                return true;
            }
        }
        catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
            return false;
        }
        return false;
    }
    private File createNewFile(String prefix){
        if(prefix==null || "".equalsIgnoreCase(prefix)){
            prefix="IMG_";
        }
        File newDirectory = new File(Environment.getExternalStorageDirectory()+"/mypics/");
        if(!newDirectory.exists()){
            if(newDirectory.mkdir()){
                Log.d(getApplicationContext().getClass().getName(), newDirectory.getAbsolutePath()+" directory created");
            }
        }
        File file = new File(newDirectory,(prefix+System.currentTimeMillis()+".jpg"));
        if(file.exists()){
            //this wont be executed
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }
    private boolean validateForm() {
        boolean valid = true;

        String isinama = nickText.getText().toString();
        if (TextUtils.isEmpty(isinama)) {
            nickText.setError("Required.");
            valid = false;
        } else {
            nickText.setError(null);
        }

        String isiusername = userText.getText().toString();
        if (TextUtils.isEmpty(isiusername)) {
            userText.setError("Required.");
            valid = false;
        } else {
            userText.setError(null);
        }

        return valid;
    }
    private void updateDataUrl(final String unama,final String uusername,final String ubirthday,final String unumber,final String ugender,final String uemail)
    {
        mAuthProgressDialog.show();
        if(selectedImage!=null){
            //Uri file = Uri.fromFile(new File("path/to/images/rivers.jpg"));
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
            StorageReference riversRef = storageRef.child("images/" + selectedImage.getLastPathSegment()+timeStamp);
            UploadTask uploadTask = riversRef.putFile(selectedImage);

// Register observers to listen for when the download is done or if it fails
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                    downloadUrl = taskSnapshot.getDownloadUrl();
                    String sdownload = downloadUrl.toString();
                    //Toast.makeText(edit_profile.this, downloadUrl.toString(),
                    //        Toast.LENGTH_SHORT).show();
                    updateData(unama,uusername,unumber,ugender,uemail,sdownload,ubirthday);
                    //Log.w("downloadURL",downloadUrl.toString());
                    mAuthProgressDialog.hide();
                }
            });
        }
        else
        {
            updateData(unama,uusername,unumber,ugender,uemail,imageurl,ubirthday);
            mAuthProgressDialog.hide();
        }

        //Log.w("downloadURL",downloadUrl.toString());
        //mAuthProgressDialog.hide();
    }
    private void updateData(final String unama, final String uusername, final String unumber, final String ugender, final String uemail, final String sdownload, final String ubirthday)
    {
        if (!validateForm()) {
            return;
        }
        Query queryRef = mDatabase.child("users").orderByChild("username").equalTo(uusername);
        queryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    // TODO: handle the case where the data already exists
                    if(userText.getKeyListener()!=null)
                    {
                        Toast.makeText(edit_profile.this, "username sudah ada",Toast.LENGTH_SHORT).show();
                        Log.v("iniuserref", dataSnapshot.getRef().toString());
                    }
                    else
                    {
                        User userf = new User(mAuth.getCurrentUser().getUid(),unama,uusername,sdownload,uemail,ubirthday,unumber,ugender,rating);
                        Map<String, Object> postValues = userf.toMap();
                        Map<String, Object> childUpdates = new HashMap<>();
                        childUpdates.put("/users/" + mAuth.getCurrentUser().getUid(), postValues);
                        mDatabase.updateChildren(childUpdates);
                        Toast.makeText(edit_profile.this, "Update Success!",
                                Toast.LENGTH_SHORT).show();
                        Intent i = new Intent(edit_profile.this, MainActivity.class);
                        startActivity(i);
                        //overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
                        finish();
                    }
                    //Toast.makeText(edit_profile.this, dataSnapshot.getKey(),Toast.LENGTH_SHORT).show();
                }
                else {
                    User userf = new User(mAuth.getCurrentUser().getUid(),unama,uusername,sdownload,uemail,ubirthday,unumber,ugender,rating);
                    Map<String, Object> postValues = userf.toMap();
                    Map<String, Object> childUpdates = new HashMap<>();
                    childUpdates.put("/users/" + mAuth.getCurrentUser().getUid(), postValues);
                    mDatabase.updateChildren(childUpdates);
                    Toast.makeText(edit_profile.this, "Update Success!",
                            Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(edit_profile.this, MainActivity.class);
                    startActivity(i);
                    //overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
                    finish();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        //Toast.makeText(edit_profile.this, sdownload,
         //       Toast.LENGTH_SHORT).show();

    }
    @Override
    public void onBackPressed() {
        if(getusername!="" || getusername!=null) {
            finish();
        }
        else
        {
            Toast.makeText(edit_profile.this,"Please update your username first!", Toast.LENGTH_SHORT ).show();
        }
    }
    @Override
    public void onPause() {
        super.onPause();
        mAuthProgressDialog.dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mAuthProgressDialog.dismiss();
    }
}