package twiscode.playpal;

import java.util.ArrayList;

/**
 * Created by Romario on 22/06/2016.
 */
public class Follow {
    public Follow(){

    }

    public Follow(ArrayList<String> following, ArrayList<String> followers, ArrayList<String> requestedFollow, ArrayList<String> requestingFollow) {
        this.following = following;
        this.followers = followers;
        this.requestedFollow = requestedFollow;
        this.requestingFollow = requestingFollow;
    }


    public ArrayList<String> getFollowing() {
        return following;
    }

    public void setFollowing(ArrayList<String> following) {
        this.following = following;
    }

    public ArrayList<String> getFollowers() {
        return followers;
    }

    public void setFollowers(ArrayList<String> followers) {
        this.followers = followers;
    }

    public ArrayList<String> getRequestedFollow() {
        return requestedFollow;
    }

    public void setRequestedFollow(ArrayList<String> requestFollow) {
        this.requestedFollow = requestFollow;
    }

    public ArrayList<String> getRequestingFollow() {
        return requestingFollow;
    }

    public void setRequestingFollow(ArrayList<String> requestingFollow) {
        this.requestingFollow = requestingFollow;
    }

    ArrayList<String> followers;
    ArrayList<String> following;
    ArrayList<String> requestedFollow;
    ArrayList<String> requestingFollow;

}
