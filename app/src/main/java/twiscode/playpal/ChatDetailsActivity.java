package twiscode.playpal;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.DataSetObserver;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

import twiscode.playpal.Utilities.ConfigManager;

public class ChatDetailsActivity extends AppCompatActivity {

    Toolbar mToolbar;
    private String roomid;
    private String namaroom;
    private String wakturoom;
    private TextView judulRoom;
    private TextView judulRoomText;
    private TextView tanggalText;
    private TextView jamText;
    private TextView alamatText,editbtn;
    private SharedPreferences mSharedPreferences;
    private RecyclerView mMessageRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private DatabaseReference mFirebaseDatabaseReference;
    private DatabaseReference mDatabase;
    Class<ArrayList<String>> mClass;
    Class<ArrayList<String>> mClass1;
    Firebase mFirebaseRef = new Firebase(ConfigManager.FIREBASE).child("message");
    Firebase firebaseRefPost = new Firebase(ConfigManager.FIREBASE).child("posts");
    ListAdapterParticipant mListAdapterParticipant;
    Activity activity = this;
    private LinearLayout layoutid;
    private Button editbuton;
    private Button deleteButton;
    Participant participant = new Participant();
    ProgressDialog dialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_chat_details);
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Lato-Regular.ttf");
        mToolbar = (Toolbar) findViewById(R.id.toolbar1);
        mToolbar.setNavigationIcon(R.drawable.ic_action_name_white);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        // Enable the Up button
        Intent intent = getIntent();
        roomid = intent.getStringExtra("idroom");
        //namaroom = intent.getStringExtra("nroom");
        firebaseRefPost.child(roomid).child("goingTo").addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                String snap = dataSnapshot.getValue(String.class);
                namaroom = snap;
                String[] split = new String[100];
                if(namaroom!=null)
                {
                    split = namaroom.split(", ");
                    judulRoomText.setText(split[0]);
                    alamatText.setText(namaroom);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        firebaseRefPost.child(roomid).child("dateTime").addValueEventListener(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                String snap = dataSnapshot.getValue(String.class);
                wakturoom = snap;
                String[] split2 = new String[100];
                if(wakturoom.equals(""))
                {
                    tanggalText.setText("Date not set");
                    jamText.setText("Time not set");
                }
                else {
                    split2 = wakturoom.split("-");
                    tanggalText.setText(split2[1] + " " + split2[0] + " " + split2[2]);
                    jamText.setText(split2[3]);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        //wakturoom = intent.getStringExtra("wroom");
        judulRoom = (TextView) findViewById(R.id.title_text);
        judulRoomText = (TextView) findViewById(R.id.judulRoom);
        tanggalText = (TextView) findViewById(R.id.tanggal);
        jamText = (TextView) findViewById(R.id.jam);
        alamatText = (TextView) findViewById(R.id.alamat);
        editbuton = (Button) findViewById(R.id.btn_edit);
        deleteButton = (Button) findViewById(R.id.btn_delete);
        judulRoom.setText("Details");

        firebaseRefPost.child(roomid).child("userId").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String userId = dataSnapshot.getValue(String.class);
                if(userId.equals(FirebaseAuth.getInstance().getCurrentUser().getUid())){
                    deleteButton.setVisibility(View.VISIBLE);
                    editbuton.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

        // New child entries
        Firebase.setAndroidContext(this);
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        //Query queryRef = mDatabase.child("posts").child(roomid).child("withWho");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        final ListView listView = (ListView) findViewById(android.R.id.list);
        ArrayList<String> participant1;
        mListAdapterParticipant = new ListAdapterParticipant(getApplicationContext(),mFirebaseRef.child(roomid).child("username"),activity,R.layout.participant_item);
        listView.setAdapter(mListAdapterParticipant);
        mListAdapterParticipant.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                setListViewHeightBasedOnChildren(listView);
            }

            @Override
            public void onInvalidated() {
                super.onInvalidated();
            }
        });
        editbuton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    Intent j = new Intent(ChatDetailsActivity.this, EditPostActivity.class);
                    //roomid = intent.getStringExtra("idroom");
                    //namaroom = intent.getStringExtra("nroom");
                    // namaroom2 = intent.getStringExtra("nroom");
                    //wakturoom
                    j.putExtra("idPost", roomid);
                    startActivityForResult(j, 2);
                }
                else
                    Toast.makeText(ChatDetailsActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();

                //idPost
            }
        });
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                makeConfirm();
            }
        });
        /*mFirebaseRef.child(roomid).child("username").addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
            @Override
            public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                Log.v("lala1", dataSnapshot.getRef().getPath().toString());
                //Log.v("lala3", dataSnapshot.child("following").getValue().toString());
                Log.v("Lala2", roomid);

                participant = dataSnapshot.getValue(Participant.class);
                Log.v("Lala", participant.getUsername().get(0));
                final

                /*listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        String select = (String) listView.getItemAtPosition(position);
                        Log.v("lala123", select);
                    }
                });            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });*/
        /*mDatabase.child("message").child(roomid).child("username").addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {
                Log.e("Count " ,""+snapshot.getChildrenCount());
                final ArrayList<String> list2 = (ArrayList<String>) snapshot.getValue();

                for (int i = 0; i<list2.size(); i++){
                    Toast.makeText(ChatDetailsActivity.this, list2.get(i),Toast.LENGTH_SHORT).show();
                    /*Query queryRef = mDatabase.child("users").orderByChild("username").equalTo(list2.get(i));

                    queryRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            if (dataSnapshot.exists()) {
                                // TODO: handle the case where the data already exists
                                User user = dataSnapshot.getValue(User.class);
                                Toast.makeText(ChatDetailsActivity.this, user.getNama(),Toast.LENGTH_SHORT).show();
                            }
                            else {

                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    mFirebaseAdapter = new FirebaseRecyclerAdapter<User, MessageViewHolder>(
                            User.class,
                            R.layout.item_message,
                            MessageViewHolder.class,
                            mFirebaseDatabaseReference.child("users").orderByChild("username").startAt(list2.get(i)).endAt(list2.get(i)+"\uf8ff")) {

                        @Override
                        protected void populateViewHolder(MessageViewHolder viewHolder,
                                                          User user, int position) {
                            Toast.makeText(ChatDetailsActivity.this, user.getNama(),Toast.LENGTH_SHORT).show();
                            Log.v("User name: ", user.getNama());
                            viewHolder.messageTextView.setText(user.getNama());
                            if (user.getProfpic() == null) {
                                viewHolder.messengerImageView
                                        .setImageDrawable(ContextCompat
                                                .getDrawable(ChatDetailsActivity.this,
                                                        R.drawable.ic_account_circle_black_36dp));
                            } else {
                                Glide.with(ChatDetailsActivity.this)
                                        .load(user.getProfpic())
                                        .into(viewHolder.messengerImageView);
                            }
                        }
                    };
                    mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);
                    mMessageRecyclerView.setAdapter(mFirebaseAdapter);
                    mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                        @Override
                        public void onItemRangeInserted(int positionStart, int itemCount) {
                            super.onItemRangeInserted(positionStart, itemCount);
                            int friendlyMessageCount = mFirebaseAdapter.getItemCount();
                            int lastVisiblePosition =
                                    mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                            // If the recycler view is initially being loaded or the
                            // user is at the bottom of the list, scroll to the bottom
                            // of the list to show the newly added message.
                            if (lastVisiblePosition == -1 ||
                                    (positionStart >= (friendlyMessageCount - 1) &&
                                            lastVisiblePosition == (positionStart - 1))) {
                                mMessageRecyclerView.scrollToPosition(positionStart);
                            }
                        }
                    });
                    Log.v("Member name", list2.get(i));
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
/*        mFirebaseAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int friendlyMessageCount = mFirebaseAdapter.getItemCount();
                int lastVisiblePosition =
                        mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the
                // user is at the bottom of the list, scroll to the bottom
                // of the list to show the newly added message.
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (friendlyMessageCount - 1) &&
                                lastVisiblePosition == (positionStart - 1))) {
                    mMessageRecyclerView.scrollToPosition(positionStart);
                }
            }
        });*/

    }

    public static void setListViewHeightBasedOnChildren(ListView listView)
    {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight=0;
        View view = null;

        for (int i = 0; i < listAdapter.getCount(); i++)
        {
            view = listAdapter.getView(i, view, listView);

            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
                        ViewGroup.LayoutParams.MATCH_PARENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();

        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));
        Log.v("cobalaka", String.valueOf(params.height));
        listView.setLayoutParams(params);
        listView.requestLayout();

    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onStop() {
        super.onStop();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                //Intent j = new Intent(ChatDetailsActivity.this, ChatActivity2.class);
                //roomid = intent.getStringExtra("idroom");
                //namaroom = intent.getStringExtra("nroom");
                // namaroom2 = intent.getStringExtra("nroom");
                //wakturoom
                /*j.putExtra("idroom",roomid);
                j.putExtra("nroom",namaroom);
                j.putExtra("wroom",wakturoom);
                startActivity(j);*/
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        //Intent i = new Intent(ChatActivity2.this, LobbyChatActivity.class);
        //i.putExtra("username",mUsername);
        //startActivity(i);
        //overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
        finish();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public void makeConfirm(){
        new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
                .setTitle("Delete Post")
                .setMessage("Do you really want to delete this post?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        if(isNetworkAvailable()) {
                            firebaseRefPost.child(roomid).child("statusDeleted").setValue(true, new Firebase.CompletionListener() {
                                @Override
                                public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                                    Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                    Toast.makeText(ChatDetailsActivity.this, "The post has been deleted", Toast.LENGTH_SHORT).show();
                                    finish();
                                }
                            });
                        }
                        else
                            Toast.makeText(ChatDetailsActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }
}
