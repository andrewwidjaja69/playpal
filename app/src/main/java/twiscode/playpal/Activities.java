package twiscode.playpal;

/**
 * Created by Romario on 28/06/2016.
 */
public class Activities {
    private String userIdRequested;
    private String activity;
    private String userIdRequesting;
    private String target;
    private String idTarget;
    private String idActivity;
    private Long timeStamp;

    public Activities(String userIdRequested, String activity, String userIdRequesting, String target, String idTarget, String idActivity, Long timeStamp) {
        this.userIdRequested = userIdRequested;
        this.activity = activity;
        this.userIdRequesting = userIdRequesting;
        this.target = target;
        this.idTarget = idTarget;
        this.idActivity = idActivity;
        this.timeStamp = timeStamp;
    }
    public Activities(){

    }

    public String getUserIdRequested() {
        return userIdRequested;
    }

    public void setUserIdRequested(String userIdRequested) {
        this.userIdRequested = userIdRequested;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getUserIdRequesting() {
        return userIdRequesting;
    }

    public void setUserIdRequesting(String userIdRequesting) {
        this.userIdRequesting = userIdRequesting;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getIdTarget() {
        return idTarget;
    }

    public void setIdTarget(String idTarget) {
        this.idTarget = idTarget;
    }

    public String getIdActivity() {
        return idActivity;
    }

    public void setIdActivity(String idActivity) {
        this.idActivity = idActivity;
    }

    public Long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(Long timeStamp) {
        this.timeStamp = timeStamp;
    }
}
