package twiscode.playpal;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import twiscode.playpal.Utilities.ConfigManager;

/**
 * Created by Romario on 24/06/2016.
 */
public class ProfileUser extends AppCompatActivity {
    ConfigManager appManager;
    Firebase mRef = new Firebase(appManager.FIREBASE).child("posts");
    Firebase mFirebaseRef = new Firebase(ConfigManager.FIREBASE).child("follow");
    Firebase firebaseRef = new Firebase(appManager.FIREBASE);
    private String mUsername;
    private String userId;
    ArrayList<String> followingPeople;
    private ListAdapterPost mPostListAdapter;
    Firebase userRef = new Firebase(appManager.FIREBASE).child("users");

    private String userIdOnline = FirebaseAuth.getInstance().getCurrentUser().getUid();
    Toolbar mToolbar;
    Dialog dialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_profile);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_action_name_white);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("Profile");
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    @Override
    protected void onStart() {
        super.onStart();
        userId = getIntent().getExtras().getString("userId");
        mUsername = getIntent().getExtras().getString("username");
        followingPeople = getIntent().getStringArrayListExtra("following");
        final ScrollView scrollView = (ScrollView) findViewById(R.id.layout_scroll);
        final LinearLayout followingBtn = (LinearLayout) findViewById(R.id.profile_following_button);
        final LinearLayout followersBtn = (LinearLayout) findViewById(R.id.profile_followers_button);
        final Firebase ref = new Firebase(appManager.FIREBASE).child("follow");
        final TextView textFollowing = (TextView) findViewById(R.id.text_following);
        final TextView textFollowers = (TextView) findViewById(R.id.text_follower);
        final TextView textNama = (TextView) findViewById(R.id.nama_profile);
        final TextView textUserneme = (TextView) findViewById(R.id.username_profile);
        final CircleImageView profpic = (CircleImageView) findViewById(R.id.prof_pic_profile);
        final Button followBtn = (Button) findViewById(R.id.button_follow_profile);
        final Button editProfile = (Button) findViewById(R.id.button_follow);
        final TextView postSum = (TextView) findViewById(R.id.post_sum);


        if (editProfile != null) {
            editProfile.setVisibility(View.VISIBLE);
        }
        if (followBtn != null) {
            followBtn.setVisibility(View.GONE);
        }
        if(!isNetworkAvailable())
        {
            editProfile.setVisibility(View.GONE);
            followBtn.setVisibility(View.GONE);
        }
        if(!userId.equals(FirebaseAuth.getInstance().getCurrentUser().getUid())){
            if (followBtn != null) {
                followBtn.setVisibility(View.VISIBLE);
            }
            if (editProfile != null) {
                editProfile.setVisibility(View.GONE);
            }
        }
        if (editProfile != null) {
            editProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent j = new Intent(ProfileUser.this, edit_profile.class);

                    startActivityForResult(j, 2);

                }
            });
        }

        userRef.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                Log.v("lala", user.getNama());

                textNama.setText(user.getNama());
                textUserneme.setText(user.getUsername());
                final String profpicUrl = user.getProfpic();
                if(!profpicUrl.equals("")){
                    Picasso.with(getApplicationContext()).load(profpicUrl).into(profpic);
                    if (profpic != null) {
                        profpic.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                showDialog(profpicUrl);
                            }
                        });
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


        ref.child(userId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                final Follow follows = dataSnapshot.getValue(Follow.class);
                final ArrayList<String> followers = follows.getFollowers();
                final ArrayList<String> following = follows.getFollowing();
                if(followers.contains(userIdOnline)){
                    followBtn.setText("Unfollow");
                    followBtn.setBackgroundResource(R.drawable.button_following);
                    followBtn.setTextColor(Color.WHITE);
                    followBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(isNetworkAvailable()) {
                                //ref.child(userIdOnline).child("following").setValue(followingPeople);
                                ArrayList<String> newFollowers = followers;
                                newFollowers.remove(userIdOnline);
                                //ref.child(userId).child("followers").setValue(newFollowers);

                                makeConfirm(userIdOnline, userId, textUserneme.getText().toString(), newFollowers);
                            }
                            else
                                Toast.makeText(ProfileUser.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }else{
                    followBtn.setText("+ Follow");
                    followBtn.setBackgroundResource(R.drawable.button_follow);
                    followBtn.setTextColor(getResources().getColor(R.color.colorPrimary));
                    followBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if(isNetworkAvailable()) {
                                followingPeople.add(userId);
                                ref.child(userIdOnline).child("following").setValue(followingPeople);
                                ArrayList<String> newFollowers = followers;
                                newFollowers.add(userIdOnline);
                                ref.child(userId).child("followers").setValue(newFollowers);
                                Firebase pushFirebaseRef = firebaseRef.child("activity").child(userId).push();
                                final String idActivity = pushFirebaseRef.getKey();
                                Long timeStamp = System.currentTimeMillis() / 1000 * (-1);
                                Activities newActivity = new Activities(userId, "Started following you", userIdOnline, "follow", "", idActivity, timeStamp);
                                pushFirebaseRef.setValue(newActivity);
                            }
                            else Toast.makeText(ProfileUser.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
                        }
                    });
                }
                //if(!followers.contains("")){
                textFollowers.setText(String.valueOf(followers.size()-1));
                //}
                //if(!following.contains("")){
                textFollowing.setText(String.valueOf(following.size()-1));
                //}
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
        if(!isNetworkAvailable()){
            Toast.makeText(ProfileUser.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
        followingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()) {
                    Intent i = new Intent(ProfileUser.this, FollowActivity.class);
                    i.putExtra("status", "Following");
                    i.putExtra("userId", userId);
                    startActivityForResult(i, 1);
                }
                else
                    Toast.makeText(ProfileUser.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });
        followersBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()){
                    Intent i = new Intent(ProfileUser.this, FollowActivity.class);
                    i.putExtra("status","Followers");
                    i.putExtra("userId", userId);
                    startActivityForResult(i, 1);
                }
                else
                    Toast.makeText(ProfileUser.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });
        final ListView listView = (ListView) findViewById(android.R.id.list);
        /*if (listView != null) {
            listView.setOnTouchListener(new View.OnTouchListener() {
                // Setting on Touch Listener for handling the touch inside ScrollView
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    // Disallow the touch request for parent scroll on touch of child view
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    return false;
                }
            });
        }*/
        Log.v("usernameLuar", mUsername);
        mPostListAdapter = new ListAdapterPost(this, mRef.orderByChild("timeStamp"), ProfileUser.this, R.layout.news_item, mUsername, "profile",userId, new ArrayList<String>());
        listView.setAdapter(mPostListAdapter);
        mPostListAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();

                setListViewHeightBasedOnChildren(listView, postSum);

            }

            @Override
            public void onInvalidated() {
                super.onInvalidated();
            }
        });

    }

    public static void setListViewHeightBasedOnChildren(ListView listView, TextView postSum)
    {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight=0;
        View view = null;

        for (int i = 0; i < listAdapter.getCount(); i++)
        {
            view = listAdapter.getView(i, view, listView);

            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
                        ViewGroup.LayoutParams.MATCH_PARENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();

        }
        int count = listAdapter.getCount();
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));
        Log.v("cobalaka", String.valueOf(params.height));
        listView.setLayoutParams(params);
        listView.requestLayout();
        if(listAdapter!=null&&!listAdapter.isEmpty()&&count!=0){
            String texting = String.valueOf(count) +" post";
            postSum.setText(texting);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //if (drawerToggle.onOptionsItemSelected(item)) {
        //   return true;
        //}

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
        finish();
    }

    public void makeConfirm(final String userIdCon, final String someoneId, final String someoneUsername, final ArrayList<String> follow2){
        new AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
                .setTitle("Unfollow?")
                .setMessage("Do you really want to unfollow " + someoneUsername + " ?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int whichButton) {
                        followingPeople.remove(someoneId);
                        ArrayList<String> newFollow = followingPeople;

                        mFirebaseRef.child(someoneId).child("followers").setValue(follow2);
                        mFirebaseRef.child(userIdCon).child("following").setValue(newFollow, new Firebase.CompletionListener() {
                            @Override
                            public void onComplete(FirebaseError firebaseError, Firebase firebase) {
                            }
                        });
                        //firebaseRef.child(someone.getId()).child("Followers").child(userId).removeValue();






                    }
                })
                .setNegativeButton(android.R.string.no, null).show();
    }



    private void showDialog(String profpic) {
        // custom dialog
        dialog = new Dialog(this);
        dialog.setContentView(R.layout.custom_dialog);
        RelativeLayout  layout = (RelativeLayout) dialog.findViewById(R.id.dialog_relative_layout);
        ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) layout.getLayoutParams();
        int widthPixels = getResources().getDisplayMetrics().widthPixels;
        Converter converter = new Converter();
        int minusHeight = converter.dpToPx(40,getApplicationContext());
        //int height = size.y;
        params.height = widthPixels - minusHeight;
        layout.setLayoutParams(params);
        layout.requestLayout();
        final ProgressBar imageDialog = (ProgressBar) dialog.findViewById(R.id.image_loading);
        imageDialog.setVisibility(View.VISIBLE);

        // set the custom dialog components - text, image and button

        ImageButton close = (ImageButton) dialog.findViewById(R.id.btnClose);
        final ImageView image =  (ImageView) dialog.findViewById(R.id.imageFull);
        Picasso.with(this).load(profpic).into(image, new Callback() {
            @Override
            public void onSuccess() {
                imageDialog.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });

        // Close Button
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //TODO Close button action
            }
        });

        // Buy Button


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();
    }



}
