package twiscode.playpal;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;

public class FeedbackActivity extends AppCompatActivity {

    StorageReference storageRef;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    TextView fromNamatxt;
    ImageView ssImage;
    EditText komentar;
    Toolbar mToolbar;
    Button sendBtn;
    String simpannama;
    private String imageurl = "";
    private static int RESULT_LOAD_IMAGE = 1;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private Uri downloadUrl;
    private Uri selectedImage;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback);
        fromNamatxt = (TextView) findViewById(R.id.fromnama);
        ssImage = (ImageView) findViewById(R.id.imageView2);
        sendBtn = (Button) findViewById(R.id.send);
        komentar = (EditText) findViewById(R.id.komentar);
        sendBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                olahData(simpannama,komentar.getText().toString());
            }
        });
        ssImage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                reqPermission();
                Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(i, RESULT_LOAD_IMAGE);
            }
        });
        mToolbar = (Toolbar) findViewById(R.id.toolbar1);
        mToolbar.setNavigationIcon(R.drawable.ic_action_name_white);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
        Firebase.setAndroidContext(this);
        FirebaseStorage storage = FirebaseStorage.getInstance();
        // Create a storage reference from our app
        storageRef = storage.getReferenceFromUrl("gs://sweltering-heat-4415.appspot.com");
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Lato-Regular.ttf");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        User user = dataSnapshot.getValue(User.class);
                        fromNamatxt.setText(user.getUsername());
                        simpannama = user.getUsername();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("!Error", "getUser:onCancelled", databaseError.toException());
                    }
                }
        );
    }
    public void reqPermission()
    {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();
            ssImage.setImageBitmap(BitmapFactory.decodeFile(picturePath));
            ssImage.setBackgroundColor(Color.TRANSPARENT);
        }
    }
    public void olahData(final String nama, final String comment)
    {
        if(selectedImage!=null){
            //Uri file = Uri.fromFile(new File("path/to/images/rivers.jpg"));
            String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
            StorageReference riversRef = storageRef.child("images/" + selectedImage.getLastPathSegment()+timeStamp);
            UploadTask uploadTask = riversRef.putFile(selectedImage);

// Register observers to listen for when the download is done or if it fails
            uploadTask.addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception exception) {
                    // Handle unsuccessful uploads
                }
            }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                    downloadUrl = taskSnapshot.getDownloadUrl();
                    String sdownload = downloadUrl.toString();
                    //Toast.makeText(edit_profile.this, downloadUrl.toString(),
                    //        Toast.LENGTH_SHORT).show();
                    setData(nama,comment,sdownload);
                    //Log.w("downloadURL",downloadUrl.toString());
                }
            });
        }
        else
        {
            setData(nama,comment,imageurl);
        }
    }
    private boolean validateForm() {
        boolean valid = true;

        String isicomment = komentar.getText().toString();
        if (TextUtils.isEmpty(isicomment)) {
            komentar.setError("Required.");
            valid = false;
        } else {
            komentar.setError(null);
        }
        return valid;
    }
    public void setData(String unama, String ucomment, String uimage)
    {
        if (!validateForm()) {
            return;
        }
        feedback feedback = new feedback(unama,ucomment,uimage);
        mDatabase.child("feedback").child(mAuth.getCurrentUser().getUid()).push().setValue(feedback);
        Toast.makeText(FeedbackActivity.this, "Thank You for Your Feedback.",Toast.LENGTH_SHORT).show();
        finish();
    }
}
