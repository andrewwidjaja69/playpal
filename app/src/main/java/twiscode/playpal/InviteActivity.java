package twiscode.playpal;

import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.ValueEventListener;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import android.util.Log;

public class InviteActivity extends AppCompatActivity {
    private ImageView btn;
    private DatabaseReference mDatabase;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth;
    private FloatingActionButton fab;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar mToolbar;
    private ActionBar actionBar;
    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private NavigationView navView;
    private Runnable asd;
    private static RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;
    private RelativeLayout sharebtn;
    private String linkdownloadandroid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);
        Firebase.setAndroidContext(this);
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Lato-Regular.ttf");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mDatabase.child("download").addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        download download = dataSnapshot.getValue(download.class);
                        linkdownloadandroid = download.getAndroid();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("Error:mDatabase", "getUser:onCancelled", databaseError.toException());
                    }
                });
        mAuth = FirebaseAuth.getInstance();

        //drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);


        //drawerToggle = setupDrawerToggle();

        // Tie DrawerLayout events to the ActionBarToggle
        //drawerLayout.setDrawerListener(drawerToggle);
        sharebtn = (RelativeLayout) findViewById(R.id.rel5);
        sharebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()) {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "Download and Become my Pal @PlayPal " + linkdownloadandroid);
                    sendIntent.setType("text/plain");
                    startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
                }
                else Toast.makeText(InviteActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });
        if(!isNetworkAvailable())
        {
            Toast.makeText(InviteActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
        /*navView = (NavigationView) findViewById(R.id.navigation);
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                if (menuItem.isChecked())
                    menuItem.setChecked(false);
                else
                    menuItem.setChecked(true);

                drawerLayout.closeDrawers();

                switch (menuItem.getItemId()) {
                    case R.id.nav_home:
                        Intent kk = new Intent(InviteActivity.this, MainActivity.class);
                        startActivity(kk);
                        finish();
                        return true;
                    case R.id.nav_setting:
                        Intent tt = new Intent(InviteActivity.this, SettingsActivity.class);
                        startActivity(tt);
                        finish();
                        return true;
                    case R.id.nav_my_profile:
                        Intent jj = new Intent(InviteActivity.this, edit_profile.class);
                        startActivity(jj);
                        finish();
                        return true;

                    case R.id.invite_friends:
                        return true;
                    case R.id.nav_logout:
            /* logout of Firebase */
                        /*mAuth.removeAuthStateListener(mAuthListener);
                        FirebaseAuth.getInstance().signOut();
                        LoginManager.getInstance().logOut();
                        Intent ii = new Intent(InviteActivity.this, LoginActivity.class);
                        startActivity(ii);
                        finish();
                        return true;
                }
                return true;
            }
        });
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.rsz_11menu_toggle);
        setSupportActionBar(mToolbar);

        actionBar = getSupportActionBar();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        LayoutInflater mInflater = LayoutInflater.from(this);
        View mCustomView = mInflater.inflate(R.layout.custom_actionbar_invite, null);
        actionBar.setCustomView(mCustomView);
        actionBar.setDisplayShowCustomEnabled(true);*/
        mToolbar = (Toolbar) findViewById(R.id.toolbar1);
        mToolbar.setNavigationIcon(R.drawable.ic_action_name_white);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //if (drawerToggle.onOptionsItemSelected(item)) {
        //   return true;
        //}

        switch (item.getItemId()) {
            case android.R.id.home:
                //Intent j = new Intent(InviteActivity.this, MainActivity.class);
                //startActivity(j);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }/*
    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, drawerLayout, mToolbar, R.string.drawer_open,  R.string.drawer_close);
    }*/
    @Override
    public void onBackPressed() {
        //Intent i = new Intent(InviteActivity.this, MainActivity.class);
        //startActivity(i);
        //overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
        finish();



    }




}
