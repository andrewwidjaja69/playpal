package twiscode.playpal;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.ScaleAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import twiscode.playpal.Utilities.ApplicationData;

/**
 * Created by aldo on 05/04/2016.
 */
public class Layout3Adapter extends RecyclerView.Adapter<Layout3Adapter.MyViewHolder> {

    private ArrayList<DataModel> dataSet;
    private static Context context;
    private static LinearLayout satu;
    private static LinearLayout done_like;
    private static LinearLayout going;
    private static LinearLayout btn_comment;

    public Layout3Adapter(Context context, ArrayList<DataModel> dataSet) {
        this.context = context;
        this.dataSet = dataSet;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtHour;
        TextView txtComment;
        ImageView imageView;
        ApplicationData appData;

        public MyViewHolder( final View itemView) {
            super(itemView);
//            this.txtName = (TextView) findViewById(R.id.textView5);
//            this.txtHour = (TextView) itemView.findViewById(R.id.textView6);
            this.txtComment = (TextView) itemView.findViewById(R.id.komen);
            this.imageView = (ImageView) itemView.findViewById(R.id.img_activity);
            final ImageView imageView = (ImageView) itemView.findViewById(R.id.img_activity);
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (imageView.getHeight()==400) {
                        int width = imageView.getWidth();
                        imageView.setLayoutParams(new LinearLayout.LayoutParams(width, width));
                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);

                    }else{
                        int width = imageView.getWidth();
                        int height = imageView.getHeight();
                        imageView.setLayoutParams(new LinearLayout.LayoutParams(width, 400));
                        imageView.setScaleType(ImageView.ScaleType.FIT_XY);


                    }
                }


            });
            satu = (LinearLayout) itemView.findViewById(R.id.satu);
            satu.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    view.setVisibility(View.GONE);
                    LinearLayout unlike = (LinearLayout) itemView.findViewById(R.id.done_like);
                    unlike.setVisibility(View.VISIBLE);
                }
            });

            done_like = (LinearLayout) itemView.findViewById(R.id.done_like);
            done_like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setVisibility(View.GONE);
                    LinearLayout unlike = (LinearLayout) itemView.findViewById(R.id.satu);
                    unlike.setVisibility(View.VISIBLE);
                }
            });

            going = (LinearLayout) itemView.findViewById(R.id.going1);
            going.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(context);
                    builder.setTitle(R.string.app_name);
                    builder.setMessage("Are you sure ?");
                    builder.setPositiveButton("Yes, of course", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ImageView iconGoing = (ImageView) itemView.findViewById(R.id.iconGoing);
                            TextView txtGoing = (TextView) itemView.findViewById(R.id.txtGoing);

                            if (txtGoing.getText().toString().equals("Going")){
                                iconGoing.setImageResource(R.drawable.goingpinkicon);
                                txtGoing.setText("Ungoing");
                            }else{
                                iconGoing.setImageResource(R.drawable.newgoingicon);
                                txtGoing.setText("Going");
                            }
                        }
                    });
                    builder.setNegativeButton("Cancel", null);
                    builder.show();
                }
            });
            btn_comment = (LinearLayout) itemView.findViewById(R.id.btn_comment);
            btn_comment.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Intent i=new Intent(view.getContext(),CommentActivity2.class);
                    view.getContext().startActivity(i);
                    appData.itemPosition = getAdapterPosition();
//                    Toast.makeText(view.getContext(), String.valueOf(()), Toast.LENGTH_SHORT).show();
                }

            });
        }
    }

    public Layout3Adapter(ArrayList<DataModel> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_layout3, parent, false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

//        TextView txtName = holder.txtName;
//        TextView txtHour = holder.txtHour;
        TextView txtComment = holder.txtComment;

//        txtName.setText(dataSet.get(listPosition).getName());
//        txtHour.setText(dataSet.get(listPosition).getHour());
        txtComment.setText(dataSet.get(listPosition).getComment());

        Picasso.with(context).load(dataSet.get(listPosition).getImage()).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

}
