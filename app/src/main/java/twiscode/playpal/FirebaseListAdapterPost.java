package twiscode.playpal;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.utilities.Pair;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import twiscode.playpal.Utilities.ConfigManager;

/**
 * Created by Romario on 16/06/2016.
 */
public abstract class FirebaseListAdapterPost extends BaseAdapter {
    ConfigManager appManager;
    private Query mRef;
    private Firebase mRefUser = new Firebase(appManager.FIREBASE).child("users");
    private Class<posts> mModelClassPost;
    private Class<User> mModelClassUser;
    private int mLayout;
    private LayoutInflater mInflater;
    private List<Pair<posts , User>> mModels;
    private List<String> mKeys;
    private ChildEventListener mListener;
    final User[] user = new User[1];
    String mStatus;
    String mUserId;
    ArrayList<String> following;



    /* public static <posts> void append(List<posts> list, Class<? extends posts> cls) throws Exception {
        posts elem = cls.newInstance();
        list.add(elem);
    }
    posts createContents(Class<posts> clazz) throws IllegalAccessException, InstantiationException {
        return clazz.newInstance();
    }*/




    /**
     * @param mRef        The Firebase location to watch for data changes. Can also be a slice of a location, using some
     *                    combination of <code>limit()</code>, <code>startAt()</code>, and <code>endAt()</code>,
     * @param mModelClassPost Firebase will marshall the data at a location into an instance of a class that you provide
     * @param mLayout     This is the mLayout used to represent a single list item. You will be responsible for populating an
     *                    instance of the corresponding view with the data from an instance of mModelClass.
     * @param activity    The activity containing the ListView
     */
    public FirebaseListAdapterPost(final Query mRef, final Class<posts> mModelClassPost, int mLayout, Activity activity, String stat, final String userId, final ArrayList<String> following) {
        this.mRef = mRef;
        this.mModelClassPost = mModelClassPost;
        this.mLayout = mLayout;
        mModelClassUser = User.class;
        mInflater = activity.getLayoutInflater();
        mModels = new ArrayList<Pair<posts , User>>();
        mKeys = new ArrayList<String>();
        this.mStatus = stat;
        this.mUserId = userId;
        this.following = following;




        // Look for all child events. We will then map them to our own internal ArrayList, which backs ListView

        /*mListener = this.mRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                HashMap<String, Object> postss = (HashMap<String, Object>) dataSnapshot.getValue();

                for (Object posts : postss.values()) {
                    HashMap<String, Object> postsMap = (HashMap<String, Object>) posts;
                    String postsNumber = (String) postsMap.remove("uid");
                    if (!mModels.contains(postsNumber)) {
                        String name = (String) postsMap.remove("nama");
                        Log.v("Data", name);
                        String postsname = (String) postsMap.remove("postsname");
                        Log.v("Data", postsname);
                        posts posts1 = new posts(postsNumber, name, postsname, "", "email", "", "", "", "");
                        mModels.add(posts1);

                    }
                }
                // thread executing here can get info from database and make subsequent call
                Collections.addAll(mModels);

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });*/

        this.mRef.addChildEventListener(new ChildEventListener() {


            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, final String previousChildName) {
                Log.v("mencobaPath",dataSnapshot.getRef().getPath().toString());
                final posts model = dataSnapshot.getValue(posts.class);
                final String key = dataSnapshot.getKey();
                Log.v("NyobaKey", key);
                //ArrayList<User> user1 = new ArrayList<User>();
                Log.v("nyobaPost", model.getGoingTo());

                mRefUser.orderByKey().startAt(model.getUserId()).endAt(model.getUserId()+"\uf8ff").addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        User newUser = dataSnapshot.getValue(User.class);
                        //user[0] = newUser;
                        //Log.v("mencoba", newUser.getNama());
                        Pair<posts,User> pair = new Pair<posts, User>(model, newUser);
                        //Log.v("coba luar", user[0].getUsername());
                        if(mStatus.equals("profile")){
                            if(newUser.getId().equals(userId)&&!model.isStatusDeleted()){
                                //System.out.println(key + " was " +  + " meters tall");
                                //Log.v("Child Added", key);
                                // Insert into the correct location, based on previousChildName
                                if (previousChildName == null) {
                                    //Log.v("Ini Null",model.toString());
                                    mModels.add(0, pair);
                                    //Log.v("Nyoba models nul",mModels.toString());
                                    mKeys.add(0, key);
                                } else {
                                    //Log.v("Nggak Null",model.toString());
                                    int previousIndex = mModels.size()-1;
                                    int nextIndex = previousIndex + 1;
                                    if (nextIndex == mModels.size()) {
                                        mModels.add(pair);
                                        //Log.v("Nyoba models 1",mModels.toString());
                                        mKeys.add(key);
                                    } else {
                                        mModels.add(nextIndex, pair);
                                        //Log.v("Nyoba models 2",mModels.toString());
                                        mKeys.add(nextIndex, key);
                                    }
                                }
                            }else{

                            }
                        }else {
                            if(!model.isStatusDeleted()&&(following.contains(newUser.getId())||newUser.getId().equals(FirebaseAuth.getInstance().getCurrentUser().getUid()))){
                            //System.out.println(key + " was " +  + " meters tall");
                            Log.v("Child Added", key);
                            // Insert into the correct location, based on previousChildName
                            if (previousChildName == null) {
                                //Log.v("Ini Null",model.toString());
                                mModels.add(0, pair);
                                //Log.v("Nyoba models nul",mModels.toString());
                                mKeys.add(0, key);
                            } else {
                                //Log.v("Nggak Null",model.toString());
                                int previousIndex = mModels.size()-1;
                                int nextIndex = previousIndex + 1;
                                if (nextIndex == mModels.size()) {
                                    mModels.add(pair);
                                    //Log.v("Nyoba models 1",mModels.toString());
                                    mKeys.add(key);
                                } else {
                                    mModels.add(nextIndex, pair);
                                    //Log.v("Nyoba models 2",mModels.toString());
                                    mKeys.add(nextIndex, key);
                                }
                            }
                            }
                        }
                        //Log.v("coba mModels", mModels.get(0).getFirst().getGoingTo());
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });
//                Pair<posts,User> pair = new Pair<posts, User>(model, user[0]);
//                Log.v("coba luar", user[0].getUsername());
//                String key = dataSnapshot.getKey();
//                //System.out.println(key + " was " +  + " meters tall");
//                Log.v("Child Added",key);
//                // Insert into the correct location, based on previousChildName
//                if (previousChildName == null) {
//                    //Log.v("Ini Null",model.toString());
//                    mModels.add(0, pair);
//                    //Log.v("Nyoba models nul",mModels.toString());
//                    mKeys.add(0, key);
//                } else {
//                    //Log.v("Nggak Null",model.toString());
//                    int previousIndex = mKeys.indexOf(previousChildName);
//                    int nextIndex = previousIndex + 1;
//                    if (nextIndex == mModels.size()) {
//                        mModels.add(pair);
//                        //Log.v("Nyoba models 1",mModels.toString());
//                        mKeys.add(key);
//                    } else {
//                        mModels.add(nextIndex, pair);
//                        //Log.v("Nyoba models 2",mModels.toString());
//                        mKeys.add(nextIndex, key);
//                    }
//                }
                //notifyDataSetChanged();
//                notifyDataSetChanged();
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                // One of the mModels changed. Replace it in our list and name mapping
                final posts model = dataSnapshot.getValue(posts.class);
                final String key = dataSnapshot.getKey();
                Log.v("NyobaKey", key);
                //ArrayList<User> user1 = new ArrayList<User>();
                Log.v("nyobaPost", model.getGoingTo());

                mRefUser.orderByKey().startAt(model.getUserId()).endAt(model.getUserId()+"\uf8ff").addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        User newUser = dataSnapshot.getValue(User.class);
                        user[0] = newUser;
                        //Log.v("mencoba", newUser.getNama());
                        Pair<posts,User> pair = new Pair<posts, User>(model, user[0]);
                        //Log.v("coba luar", user[0].getUsername());
                        if(mStatus.equals("profile")&&!model.isStatusDeleted()){
                            if(newUser.getId().equals(userId)){
                                //System.out.println(key + " was " +  + " meters tall");
                                //Log.v("Child Added", key);
                                // Insert into the correct location, based on previousChildName
                                int index = mKeys.indexOf(key);
                                mModels.set(index, pair);

                                //notifyDataSetChanged();

                            }else{

                            }
                        }else {
                            if(!model.isStatusDeleted()&&(following.contains(newUser.getId()) || newUser.getId().equals(FirebaseAuth.getInstance().getCurrentUser().getUid()))){
                                //System.out.println(key + " was " +  + " meters tall");
                                Log.v("Child Added", key);
                                int index = mKeys.indexOf(key);
                                // Insert into the correct location, based on previousChildName
                                mModels.set(index, pair);

                                //notifyDataSetChanged();
                            }
                        }
                        //Log.v("coba mModels", mModels.get(0).getFirst().getGoingTo());
                        notifyDataSetChanged();
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });


                //notifyDataSetChanged();

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

                // A model was removed from the list. Remove it from our list and the name mapping
                String key = dataSnapshot.getKey();
                int index = mKeys.indexOf(key);

                mKeys.remove(index);
                mModels.remove(index);

                notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, final String previousChildName) {

                // A model changed position in the list. Update our list accordingly
   /*             final String key = dataSnapshot.getKey();
                final posts newModel = dataSnapshot.getValue(posts.class);
                mRefUser.orderByValue().equalTo(newModel.getUserId()).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User newUser = dataSnapshot.getValue(User.class);
                        Log.v("mencoba", newUser.getNama());
                        Pair<posts,User> newPair = new Pair<posts, User>(newModel,user[0]);
                        int index = mKeys.indexOf(key);
                        mModels.remove(index);
                        mKeys.remove(index);
                        if (previousChildName == null) {
                            mModels.add(0, newPair);
                            mKeys.add(0, key);
                        } else {
                            int previousIndex = mKeys.indexOf(previousChildName);
                            int nextIndex = previousIndex + 1;
                            if (nextIndex == mModels.size()) {
                                mModels.add(newPair);
                                mKeys.add(key);
                            } else {
                                mModels.add(nextIndex, newPair);
                                mKeys.add(nextIndex, key);
                            }
                        }
                        notifyDataSetChanged();

                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });
                //Log.v("coba luar", user.getUsername());
*/
            }


            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e("FirebaseListAdapterPost", "Listen was cancelled, no more updates will occur");
            }

        });
    }

    /*public void cleanup() {
        // We're being destroyed, let go of our mListener and forget about all of the mModels
        mRef.removeEventListener(mListener);
        mModels.clear();
        mKeys.clear();
    }*/

    @Override
    public int getCount() {
        return mModels.size();
    }

    @Override
    public Object getItem(int i) {
        return mModels.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        if (view == null) {
            view = mInflater.inflate(mLayout, viewGroup, false);
        }
        Log.v("mencobaModels", mModels.toString());
        Log.v("nyobaIndex", String.valueOf(i));
        Pair<posts,User> model = mModels.get(i);
        Log.v("nyobaModel", model.getFirst().getGoingTo());
        // Call out to subclass to marshall this model into the provided view
        populateView(view, model);
        return view;
    }

    /**
     * Each time the data at the given Firebase location changes, this method will be called for each item that needs
     * to be displayed. The arguments correspond to the mLayout and mModelClass given to the constructor of this class.
     * <p/>
     * Your implementation should populate the view using the data contained in the model.
     *
     * @param v     The view to populate
     * @param model The object containing the data used to populate the view
     */
    protected abstract void populateView(View v, Pair<posts, User> model);
}

