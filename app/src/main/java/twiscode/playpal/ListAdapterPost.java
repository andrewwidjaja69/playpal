package twiscode.playpal;

/**
 * Created by Romario on 21/06/2016.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.firebase.client.utilities.Pair;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


public class ListAdapterPost extends FirebaseListAdapterPost {

    // The mUsername for this client. We use this to indicate which messages originated from this user
    private String mUsername;
    Context context;

    public ListAdapterPost(Context context, com.firebase.client.Query ref, Activity activity, int layout, String mUsername, String stat, String userId,  ArrayList<String> following) {
        super(ref, posts.class, layout, activity, stat, userId, following);
        this.context = context;
        this.mUsername = mUsername;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void populateView(View view, Pair<posts, User> postpair) {
        // Map a Chat object to an entry in our listview
        final posts post = postpair.getFirst();
        final User user = postpair.getSecond();
        final ImageView lock = (ImageView) view.findViewById(R.id.lock_icon);
        lock.setVisibility(View.GONE);
        Toolbar mToolbar = (Toolbar) view.findViewById(R.id.toolbar);
        mToolbar.setVisibility(View.GONE);
        if(post.getPrivateStatus().equals("True")){
            lock.setVisibility(View.VISIBLE);
        }
        final Firebase firebaseRef = new Firebase(appManager.FIREBASE);
        final ArrayList<String> waiting = post.getWaitingRecipients();
        final Firebase fbUpdateRef = new Firebase(appManager.FIREBASE).child("posts").child(post.getPostId());
        final Firebase fbUpdateRefChat = new Firebase(appManager.FIREBASE).child("message");
        String nama = user.getNama();
        final String privateStatus = post.getPrivateStatus();
        final int more = 0;
        String profpic = user.getProfpic();
        final String goingTo = post.getGoingTo();
        String atTime = post.getDateTime();
        String picture = post.getImageURL();
        Log.v("MencobaLagi", post.getImageURL());
        final ArrayList<String> withWho = post.getWithWho();
        final ArrayList<String> likers = post.getLikers();
        final int likeNumber = likers.size()-1;
        TextView textNama = (TextView) view.findViewById(R.id.people_name_news);
        textNama.setText(nama);
        textNama.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()) {
                    new Firebase(appManager.FIREBASE).child("follow").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
                        @Override
                        public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                            Follow follow = dataSnapshot.getValue(Follow.class);
                            ArrayList<String> following = follow.getFollowing();
                            Intent t = new Intent(context, ProfileUser.class);
                            t.putStringArrayListExtra("following", following);
                            t.putExtra("userId", user.getId());
                            t.putExtra("username", user.getUsername());
                            context.startActivity(t);
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {

                        }
                    });
                }
                else Toast.makeText(context, "No Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });
        ImageView profpicView = (ImageView) view.findViewById(R.id.prof_pic_news);
        if(!profpic.isEmpty()){
            Picasso.with(context).load(profpic).transform(new CircleTransform()).into(profpicView);
        }else{
            Picasso.with(context).load(R.drawable.ayam).into(profpicView);
        }
        profpicView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()) {
                    new Firebase(appManager.FIREBASE).child("follow").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
                        @Override
                        public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                            Follow follow = dataSnapshot.getValue(Follow.class);
                            ArrayList<String> following = follow.getFollowing();
                            Intent t = new Intent(context, ProfileUser.class);
                            t.putStringArrayListExtra("following", following);
                            t.putExtra("userId", user.getId());
                            t.putExtra("username", user.getUsername());
                            context.startActivity(t);
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {

                        }
                    });
                }
                else Toast.makeText(context, "No Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });
        final TextView textGoingTo = (TextView) view.findViewById(R.id.going_to_news);
        String text = "Going to "+"<b>"+goingTo+"</b>";
        String textAt ="At "+"<b><i>"+atTime+"</i></b>";
        final TextView textAtTime = (TextView) view.findViewById(R.id.at_news);
        textAtTime.setText(Html.fromHtml(textAt));
        textGoingTo.setText(Html.fromHtml(text));
        RelativeLayout all = (RelativeLayout) view.findViewById(R.id.layout_news_item);
        all.bringToFront();
        textGoingTo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textGoingTo.getMaxLines()==1){
                    textGoingTo.setMaxLines(999999);
                    //textGoingTo.setEms(15);
                    //textGoingTo.setEllipsize(TextUtils.TruncateAt.MIDDLE);

                }else
                    textGoingTo.setMaxLines(1);
                    //textGoingTo.setEms(15);
                    //textGoingTo.setEllipsize(TextUtils.TruncateAt.END);
            }
        });
        final FrameLayout frameImage = (FrameLayout) view.findViewById(R.id.news_picture_layout);
        final ProgressBar imageDialog = (ProgressBar) view.findViewById(R.id.image_dialog);
        frameImage.setVisibility(View.VISIBLE);
        imageDialog.setVisibility(View.VISIBLE);
        final ImageView newsImage = (ImageView) view.findViewById(R.id.news_picture);
        frameImage.setVisibility(View.GONE);
        RelativeLayout people_info = (RelativeLayout) view.findViewById(R.id.people_news);
        final int w = people_info.getWidth();
        //newsImage.setMaxHeight(w);
        //newsImage.setMinimumHeight(w);
        Log.v("mencobaImage2", picture);
        if(picture.isEmpty()){
            frameImage.setVisibility(View.GONE);
        }else{
            frameImage.setVisibility(View.VISIBLE);
            Picasso.with(context).load(picture).into(newsImage, new Callback() {
                @Override
                public void onSuccess() {
                    imageDialog.setVisibility(View.GONE);

                }

                @Override
                public void onError() {

                }
            });
            newsImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) newsImage.getLayoutParams();
                    int widthPixels = context.getResources().getDisplayMetrics().widthPixels;
                    Converter converter = new Converter();
                    int minusHeight = converter.dpToPx(20,context);
                    //int height = size.y;
                    if(params.height!=widthPixels-minusHeight){

                        params.height = widthPixels - minusHeight;

                    }else{
                        params.height = widthPixels*2/3;
                    }
                    newsImage.setLayoutParams(params);
                    newsImage.requestLayout();
                }
            });
        }

        final TextView likeNumbers = (TextView) view.findViewById(R.id.news_like_info_text);
        likeNumbers.setText(String.valueOf(likeNumber)+" pals like this.");
        final RelativeLayout likeInfo = (RelativeLayout) view.findViewById(R.id.news_like_info);
        LinearLayout chatBtn = (LinearLayout) view.findViewById(R.id.news_chat_button);
        chatBtn.setVisibility(View.INVISIBLE);
        if((withWho.contains(mUsername) || user.getUsername().equals(mUsername))&&!mUsername.isEmpty()){
            chatBtn.setVisibility(View.VISIBLE);
        }
        chatBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()) {
                    Intent o = new Intent(context, ChatActivity2.class);
                    o.putExtra("wroom", post.getDateTime());
                    o.putExtra("idroom", post.getPostId());
                    o.putExtra("nroom", goingTo);
                    context.startActivity(o);
                }
                else Toast.makeText(context, "No Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });
        Log.v("mencobalike", likers.toString());
        Log.v("mencobaUser", mUsername);
        final LinearLayout goingBtn = (LinearLayout) view.findViewById(R.id.news_going_button);
        final ImageView goingBtnImage = (ImageView) view.findViewById(R.id.news_going_button_image);
        goingBtn.setVisibility(View.INVISIBLE);
        if(!withWho.contains(mUsername) && !user.getUsername().equals(mUsername) && !waiting.contains(mUsername)){
            goingBtn.setVisibility(View.VISIBLE);
        }
        goingBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) {
                    if (privateStatus.equals("True")) {
                        ArrayList<String> waitingRecipients = waiting;
                        waitingRecipients.add(mUsername);
                        fbUpdateRef.child("waitingRecipients").setValue(waitingRecipients);
                        goingBtn.setVisibility(View.INVISIBLE);
                        Firebase pushFirebaseRef = firebaseRef.child("activity").child(user.getId()).push();
                        final String idActivity = pushFirebaseRef.getKey();
                        Long timeStamp = System.currentTimeMillis() / 1000 * (-1);
                        Activities newActivity = new Activities(user.getId(), "Request to Join you at", FirebaseAuth.getInstance().getCurrentUser().getUid(), "posts", post.getPostId(), idActivity, timeStamp);
                        pushFirebaseRef.setValue(newActivity);
                        Toast.makeText(context, "Your Going Request have been Sent...", Toast.LENGTH_LONG).show();

                    } else {


                        goingBtn.setVisibility(View.INVISIBLE);
                        ArrayList<String> newWithWho = withWho;
                        newWithWho.add(mUsername);
                        if (newWithWho.contains("")) {
                            newWithWho.remove("");
                        }
                        fbUpdateRef.child("withWho").setValue(newWithWho);
                        ArrayList<String> usernameInChat = newWithWho;
                        usernameInChat.add(user.getUsername());
                        fbUpdateRefChat.child(post.getPostId()).child("username").setValue(usernameInChat);
                        Firebase pushFirebaseRef = firebaseRef.child("activity").child(user.getId()).push();
                        final String idActivity = pushFirebaseRef.getKey();
                        Long timeStamp = System.currentTimeMillis() / 1000 * (-1);
                        Activities newActivity = new Activities(user.getId(), "Has Joined you at", FirebaseAuth.getInstance().getCurrentUser().getUid(), "posts", post.getPostId(), idActivity, timeStamp);
                        pushFirebaseRef.setValue(newActivity);
                        Toast.makeText(context, "Your Going Request have been Sent...", Toast.LENGTH_LONG).show();
                    }//goingBtnImage.setImageResource(R.drawable.ayamkuning);
                }
                else Toast.makeText(context, "No Network Connection!", Toast.LENGTH_SHORT).show();
            }

        });

        final ImageView likeBtnImage = (ImageView) view.findViewById(R.id.news_like_button_image);
        if(likers.contains(mUsername)&&mUsername!=null&&!mUsername.isEmpty()){
            likeBtnImage.setImageResource(R.drawable.liked_button);
        }else {
            likeBtnImage.setImageResource(R.drawable.like_button);
        }
        LinearLayout like_btn = (LinearLayout) view.findViewById(R.id.news_like_button);
        Log.v("mencobaLikers", String.valueOf(likers.size()));
        /*if(likers.size()==1){
            likeInfo.setVisibility(View.GONE);
        }*/
        like_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isNetworkAvailable()) {
                    if (likers.contains(mUsername) && mUsername != null && !mUsername.isEmpty()) {
                        ArrayList<String> newLikers = likers;
                        newLikers.remove(mUsername);
                    /*if(newLikers.size()<2){
                        likeInfo.setVisibility(View.GONE);
                    }*/

                        fbUpdateRef.child("likers").setValue(newLikers);
                        int newLikeNumber = newLikers.size();
                        likeNumbers.setText(String.valueOf(newLikeNumber) + " pals like this");
                        likeBtnImage.setImageResource(R.drawable.like_button);


                    } else {
                        if (mUsername != null && !mUsername.isEmpty()) {
                            ArrayList<String> newLikers = likers;
                            newLikers.add(mUsername);
                            if (newLikers.size() > 1) {
                                likeInfo.setVisibility(View.VISIBLE);
                            }
                            Firebase fbUpdateRef = new Firebase(appManager.FIREBASE).child("posts").child(post.getPostId());
                            fbUpdateRef.child("likers").setValue(newLikers);
                            int newLikeNumber = newLikers.size();
                            Firebase pushFirebaseRef = firebaseRef.child("activity").child(user.getId()).push();
                            final String idActivity = pushFirebaseRef.getKey();
                            Log.v("mencobaActivity", idActivity);
                            Log.v("mencobaActivity2", pushFirebaseRef.getPath().toString());
                            Long timeStamp = System.currentTimeMillis() / 1000 * (-1);
                            Activities newActivity = new Activities(user.getId(), "Liked your post", FirebaseAuth.getInstance().getCurrentUser().getUid(), "posts", post.getPostId(), idActivity, timeStamp);
                            pushFirebaseRef.setValue(newActivity);
                            likeNumbers.setText(String.valueOf(newLikeNumber) + " pals like this");
                            likeBtnImage.setImageResource(R.drawable.liked_button);
                        } else {
                            firebaseRef.child("users").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("username").addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    mUsername = dataSnapshot.getValue(String.class);
                                    ArrayList<String> newLikers = likers;
                                    newLikers.add(mUsername);
                                    if (newLikers.size() > 1) {
                                        likeInfo.setVisibility(View.VISIBLE);
                                    }
                                    Firebase fbUpdateRef = new Firebase(appManager.FIREBASE).child("posts").child(post.getPostId());
                                    fbUpdateRef.child("likers").setValue(newLikers);
                                    int newLikeNumber = newLikers.size();
                                    Firebase pushFirebaseRef = firebaseRef.child("activity").child(user.getId()).push();
                                    final String idActivity = pushFirebaseRef.getKey();
                                    Log.v("mencobaActivity", idActivity);
                                    Log.v("mencobaActivity2", pushFirebaseRef.getPath().toString());
                                    Long timeStamp = System.currentTimeMillis() / 1000 * (-1);
                                    Activities newActivity = new Activities(user.getId(), "Liked your post", FirebaseAuth.getInstance().getCurrentUser().getUid(), "posts", post.getPostId(), idActivity, timeStamp);
                                    pushFirebaseRef.setValue(newActivity);
                                    likeNumbers.setText(String.valueOf(newLikeNumber) + " pals like this");
                                    likeBtnImage.setImageResource(R.drawable.liked_button);
                                }

                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }
                            });
                        }

                    }
                }
                else
                    Toast.makeText(context, "No Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });


    }




}