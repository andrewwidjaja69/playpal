package twiscode.playpal;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;

import java.util.ArrayList;
import java.util.List;

import twiscode.playpal.Utilities.ConfigManager;

/**
 * Created by Crusader on 6/27/2016.
 */
public abstract class FirebaseListAdapterLobby extends BaseAdapter {
    ConfigManager appManager;
    private Query mRef;
    private Firebase mRefMessage = new Firebase(appManager.FIREBASE).child("message");
    private Firebase mRefPost = new Firebase(appManager.FIREBASE).child("posts");
    private Class<String> mModelClass;
    private int mLayout;
    private LayoutInflater mInflater;
    final private String username;
    private List<posts> mModels;
    private List<String> mKeys;
    private ChildEventListener mListener;
    private posts posts;
    private ArrayList<String> stringtampung= new ArrayList<String>();
    private String modelString = null;
    public FirebaseListAdapterLobby(final Query mRef, final Class<String> mModelClass, int mLayout, Activity activity, final String username) {
        this.mRef = mRef;
        this.mModelClass = mModelClass;
        this.mLayout = mLayout;
        this.username = username;
        this.posts = posts;
        mInflater = activity.getLayoutInflater();
        mModels = new ArrayList<posts>();
        mKeys = new ArrayList<String>();
        this.mRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, final String previousChildName) {
                final posts pos = dataSnapshot.getValue(posts.class);
                final String key = dataSnapshot.getKey();
                Log.v("iniKey", pos.getPostId());
                mRefMessage.child(pos.getPostId()).child("username").addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        modelString = dataSnapshot.getValue(FirebaseListAdapterLobby.this.mModelClass);
                        if(modelString.equals(username)) {
                            mRefPost.orderByChild("postId").startAt(pos.getPostId()).endAt(pos.getPostId()+"\uf8ff").addChildEventListener(new ChildEventListener() {
                                @Override
                                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                    Log.v("mrefpost", dataSnapshot.getRef().toString());
                                    posts poss = dataSnapshot.getValue(posts.class);
                                    if(!poss.isStatusDeleted()){
                                        if (previousChildName == null) {
                                            //Log.v("Ini Null",model.toString());
                                            mModels.add(0, poss);
                                            //Log.v("Nyoba models nul",mModels.toString());
                                            mKeys.add(0, key);
                                        } else {
                                            //Log.v("Nggak Null",model.toString());
                                            int previousIndex = mKeys.indexOf(previousChildName);
                                            int nextIndex = previousIndex + 1;
                                            if (nextIndex == mModels.size()) {
                                                mModels.add(poss);
                                                //Log.v("Nyoba models 1",mModels.toString());
                                                mKeys.add(key);
                                            } else {
                                                mModels.add(nextIndex, poss);
                                                //Log.v("Nyoba models 2",mModels.toString());
                                                mKeys.add(nextIndex, key);
                                            }
                                        }
                                        notifyDataSetChanged();
                                        }
                                }

                                @Override
                                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                }

                                @Override
                                public void onChildRemoved(DataSnapshot dataSnapshot) {

                                }

                                @Override
                                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                }

                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }
                            });
                        }
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {

                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });
                /*if(lala.getUsername().contains("andrew12"))
                {

                }*/
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });
    }

    @Override
    public int getCount() {
        return mModels.size();
    }

    @Override
    public Object getItem(int position) {
        return mModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(mLayout, parent, false);
        }

        posts model = mModels.get(position);
        Log.v("index", String.valueOf(position));
        Log.v("model", model.getImageURL());
        populateView(convertView, model);
        return convertView;
    }

    protected abstract void populateView(View view, posts model);
}
