package twiscode.playpal;

import java.util.ArrayList;

/**
 * Created by Crusader on 6/24/2016.
 */
public class Participant {
    public Participant(){

    }

    public Participant(ArrayList<String> username) {
        this.username = username;
    }

    public ArrayList<String> getUsername() {
        return username;
    }

    public void setFollowing(ArrayList<String> username) {
        this.username = username;
    }

    ArrayList<String> username;
}
