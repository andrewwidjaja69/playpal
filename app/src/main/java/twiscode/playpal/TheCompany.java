package twiscode.playpal;

import android.app.Activity;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.widget.ActionMenuView;

/**
 * Created by aldo on 15/04/2016.
 */
public class TheCompany extends Activity implements ActionMenuView.OnMenuItemClickListener {

    public static float screen_width = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.card_layout3);
        DisplayMetrics metrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(metrics);
        screen_width = metrics.widthPixels;

    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        return false;
    }
}
