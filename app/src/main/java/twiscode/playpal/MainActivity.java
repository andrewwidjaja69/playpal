package twiscode.playpal;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.firebase.client.Firebase;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.crash.FirebaseCrash;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import me.arulnadhan.searchview.MaterialSearchView;
import twiscode.playpal.Utilities.ApplicationData;
import twiscode.playpal.Utilities.ConfigManager;

public class MainActivity extends AppCompatActivity {
    private ImageView btn;
    private TextView usernameTextNav;
    private TextView namaTextNav;
    private ImageView imageNav;
    private static final String TAG = "EmailPassword";
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private FloatingActionButton fab;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    private Toolbar mToolbar;
    private ActionBar actionBar;
    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawerLayout;
    private NavigationView navView;
    private MaterialSearchView searchView;
    private ApplicationData appData;
    private FirebaseAuth.AuthStateListener mAuthListener;
    private GoogleApiClient mGoogleApiClient;
    private String providerId;
    private User user1;
    Stack<Integer> stack;
    String userIdOnline = FirebaseAuth.getInstance().getCurrentUser().getUid();
    FragmentTransaction ft;
    private List<Integer> mTrue;
    //Firebase firebaseRef;
    ConfigManager appManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        stack = new Stack<Integer>();
        setContentView(R.layout.activity_main);
        Firebase.setAndroidContext(this);
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Lato-Regular.ttf");
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser userF = FirebaseAuth.getInstance().getCurrentUser();
        drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        FacebookSdk.sdkInitialize(getApplicationContext());

        drawerToggle = setupDrawerToggle();
//test1
        // Tie DrawerLayout events to the ActionBarToggle
        drawerLayout.setDrawerListener(drawerToggle);

//        searchView = (MaterialSearchView) findViewById(R.id.search_view);
//        searchView.setVoiceSearch(false);
//        searchView.setHint("Search your friends ...");
//        searchView.setSuggestions(getResources().getStringArray(R.array.query_suggestions));
//        //Toast.makeText(MainActivity.this, mAuth.getCurrentUser().getProviderId(),Toast.LENGTH_SHORT).show();
//        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextSubmit(String query) {
////                Snackbar.make(findViewById(R.id.container), "Query: " + query, Snackbar.LENGTH_LONG)
////                        .show();
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextChange(String newText) {
//                //Do some magic
//                return false;
//            }
//        });

//        searchView.setOnSearchViewListener(new MaterialSearchView.SearchViewListener() {
//            @Override
//            public void onSearchViewShown() {
//                //Do some magic
//            }
//
//            @Override
//            public void onSearchViewClosed() {
//                //Do some magic
//            }
//        });

        navView = (NavigationView) findViewById(R.id.navigation);
        View header = navView.getHeaderView(0);
        usernameTextNav = (TextView) header.findViewById(R.id.username_nav_view);
        namaTextNav = (TextView) header.findViewById(R.id.nama_nav_view);
        imageNav = (ImageView) header.findViewById(R.id.image_nav_view);
        ImageView searchBtn = (ImageView) findViewById(R.id.search_btn);
        if (searchBtn != null) {
            searchBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent newIntent = new Intent(MainActivity.this, SearchPeopleActivity.class);
                    startActivity(newIntent);
                    //finish();
                }
            });
        }
        navView.setItemIconTintList(null);
        navView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(MenuItem menuItem) {
                //menuItem.setChecked(false);
                if (menuItem.isChecked())
                    menuItem.setChecked(false);
                else
                    menuItem.setChecked(true);

                drawerLayout.closeDrawers();

                switch (menuItem.getItemId()) {

                    case R.id.nav_home:

                        navView.setItemIconTintList(null);
                         // hi
                        return true;
                    case R.id.nav_my_profile:
                        navView.setItemIconTintList(null);
                        Intent j = new Intent(MainActivity.this, edit_profile.class);

                        startActivity(j);
                        //finish();
                        return true;

                    case R.id.invite_friends:
                        navView.setItemIconTintList(null);
                        Intent i = new Intent(MainActivity.this, InviteActivity.class);

                        startActivity(i);
                        //finish();
                        return true;
                    case R.id.nav_chat:
                        navView.setItemIconTintList(null);
                        Intent p = new Intent(MainActivity.this, LobbyChatActivity.class);
                        if(isNetworkAvailable())
                        {
                            p.putExtra("username",user1.getUsername());
                        }
                        startActivity(p);
                        //finish();
                        return true;
                    /*case R.id.nav_logout:
            /* logout of Firebase
                        mAuth.removeAuthStateListener(mAuthListener);
                        FirebaseAuth.getInstance().signOut();
                        //Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                        LoginManager.getInstance().logOut();
                        Intent ii = new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(ii);
                        finish();
                        return true;*/
                    case R.id.nav_setting:
                        navView.setItemIconTintList(null);
                        Intent tt = new Intent(MainActivity.this, SettingsActivity.class);
                        startActivity(tt);
                        //finish();
                        return true;
                }
                return true;
            }
        });
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("");
        //setSupportActionBar(toolbar);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.rsz_11menu_toggle);

        setSupportActionBar(mToolbar);

//        actionBar = getSupportActionBar();
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        LayoutInflater mInflater = LayoutInflater.from(this);
//
//        View mCustomView = mInflater.inflate(R.layout.custom_actionbar, null);
//        actionBar.setCustomView(mCustomView);
//        actionBar.setDisplayShowCustomEnabled(true);

//        btn = (ImageView) findViewById(R.id.btnCart);
//        btn.setOnClickListener(new View.OnClickListener() {
//
//            public void onClick(View v) {
//                Intent i = new Intent(MainActivity.this, SearchPeoplePost.class);
//
//                startActivity(i);
//                finish();
//            }
//        });
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);


        //namaTextNav.setText(mAuth.getCurrentUser().getUid());
        if (userF != null) {
            mDatabase.child("users").child(userF.getUid()).addValueEventListener(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        User user = dataSnapshot.getValue(User.class);
                        user1 = dataSnapshot.getValue(User.class);
                        //Log.v("adauser",user.toString());
                        //String name;
                        //String name = user.getNama();
                        //Toast.makeText(MainActivity.this, providerId,Toast.LENGTH_SHORT).show();
                        //String getusername = user.getUsername();
                        //String imageurl = user.getProfpic();
                        //Log.w("inierror",user.getNama());
                        //Toast.makeText(MainActivity.this, name, Toast.LENGTH_SHORT).show();
                        if(user != null) {
                            if (user.getNama() == null) {
                                return;
                            } else {
                                //Toast.makeText(MainActivity.this, name,Toast.LENGTH_SHORT).show();
                                usernameTextNav.setText(user.getNama());
                            }
                            if (user.getUsername() == null) {
                                return;
                            } else {
                                namaTextNav.setText(user.getUsername());
                                //Toast.makeText(MainActivity.this, getusername,Toast.LENGTH_SHORT).show();
                                //   usernameTextNav.setText(getusername);
                            }
                            imageNav.setImageResource(R.drawable.ayam);
                            //Picasso.with(context).load(profpicUrl).transform(new CircleTransform()).into(profpic);
                            if (user.getProfpic().equals("")) {
                                imageNav.setImageResource(R.drawable.ayam);
                                return;
                            } else {
                                Picasso.with(getApplicationContext()).load(user.getProfpic()).transform(new CircleTransform()).into(imageNav);
                            }
                            //iniImage.setImageDrawable(LoadImageFromWebOperations(imageurl));
                            //Picasso.with(getApplicationContext()).setLoggingEnabled(true);
                            //Picasso.with(getApplicationContext()).setIndicatorsEnabled(false);
                            //Toast.makeText(edit_profile.this, providerId,Toast.LENGTH_SHORT).show();

                            //Picasso.with(getApplicationContext()).load("https://".concat(imageurl)).into(iniImage);
                            //Toast.makeText(edit_profile.this, name,
                            //        Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            return;
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("!Error", "getUser:onCancelled", databaseError.toException());
                    }
                }
            );
        }
        else
        {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return;
        }
        FirebaseCrash.report(new Exception("My first Android non-fatal error"));

        //setIsiNav();
        //Log.w("initag", providerId);
    }


/*private void setIsiNav()


    /*private void setIsiNav()

    {

    }*/
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(!isNetworkAvailable())
        {
            Toast.makeText(MainActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new OneFragment(), "News");
        adapter.addFragment(new TwoFragment(), "Activities");
        adapter.addFragment(new ThreeFragment(), "Profile");
        viewPager.setAdapter(adapter);
    }

    class ViewPagerAdapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }

        @Override
        public void setPrimaryItem(ViewGroup container, int position, Object object) {
            if(stack.isEmpty()){
                stack.push(Integer.valueOf(position));
            }
            else if(stack.peek()!=position){
                Stack<Integer> tmpStack = new Stack<Integer>();
                while(!stack.isEmpty()){
                    if(stack.peek()!=position){
                        tmpStack.push(stack.pop());
                    }else{
                        stack.pop();
                    }
                }
                while(!tmpStack.isEmpty()){
                    stack.push(tmpStack.pop());
                }
                stack.push(Integer.valueOf(position));
            }
            super.setPrimaryItem(container, position, object);
        }
    }
    //@Override
    /*public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem item = menu.findItem(R.id.action_search);
        searchView.setMenuItem(item);

        return true;
    }*/
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //if (drawerToggle.onOptionsItemSelected(item)) {
        //   return true;
        //}

        switch (item.getItemId()) {
            case android.R.id.home:
                drawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, drawerLayout, mToolbar, R.string.drawer_open,  R.string.drawer_close);
    }


    @Override
    public void onBackPressed() {
        int i = viewPager.getCurrentItem();
        stack.pop();
        if(!stack.isEmpty()){
            viewPager.setCurrentItem(stack.pop());
        }
        else{
            super.onBackPressed();
        }
    }


}
