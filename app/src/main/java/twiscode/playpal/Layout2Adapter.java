package twiscode.playpal;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by aldo on 05/04/2016.
 */
public class Layout2Adapter extends RecyclerView.Adapter<Layout2Adapter.MyViewHolder> {



    private ArrayList<DataModel> dataSet;
    private static Context context;
    private static Button button_follow;
    private static Button btn_after4;

    public Layout2Adapter(Context context, ArrayList<DataModel> dataSet) {
        this.context = context;
        this.dataSet = dataSet;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtHour;
        TextView txtComment;

        public MyViewHolder(final View itemView) {
            super(itemView);
            this.txtName = (TextView) itemView.findViewById(R.id.nama);
            this.txtHour = (TextView) itemView.findViewById(R.id.following);
            this.txtComment = (TextView) itemView.findViewById(R.id.join);

            button_follow = (Button) itemView.findViewById(R.id.button_follow);
            button_follow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setVisibility(View.GONE);
                    Button btn_after4 = (Button) itemView.findViewById(R.id.btn_after4);
                    btn_after4.setVisibility(View.VISIBLE);

                }
            });
            btn_after4 = (Button) itemView.findViewById(R.id.btn_after4);
            btn_after4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    view.setVisibility(View.GONE);
                    Button btn_after4 = (Button) itemView.findViewById(R.id.button_follow);
                    btn_after4.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    public Layout2Adapter(ArrayList<DataModel> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_layout2, parent, false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        TextView txtName = holder.txtName;
        TextView txtHour = holder.txtHour;
        TextView txtComment = holder.txtComment;

        txtName.setText(dataSet.get(listPosition).getName());
        txtHour.setText(dataSet.get(listPosition).getHour());
        txtComment.setText(dataSet.get(listPosition).getComment());

//        Picasso.with(context).load(dataSet.get(listPosition).getImage()).into(viewHolder.img_android);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }}
