package twiscode.playpal;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.Query;
import com.firebase.client.ValueEventListener;
import com.firebase.client.utilities.Pair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import twiscode.playpal.Utilities.ConfigManager;

/**
 * Created by Crusader on 6/24/2016.
 */
public abstract class FirebaseListAdapterParticipant extends BaseAdapter {
    ConfigManager appManager;
    private Query mRef;
    private Firebase mRefUser = new Firebase(appManager.FIREBASE).child("users");
    private Class<String> mModelClass;
    private int mLayout;
    private LayoutInflater mInflater;
    private String mStatus;
    private List<User> mModels;
    private List<String> mKeys;
    private ChildEventListener mListener;
    private Participant participant;
    private ArrayList<String> stringtampung= new ArrayList<String>();

    public FirebaseListAdapterParticipant(final Query mRef, final Class<String> mModelClass, int mLayout, Activity activity){
        this.mRef = mRef;
        this.mModelClass = mModelClass;
        this.mLayout = mLayout;
        this.participant = participant;
        mInflater = activity.getLayoutInflater();
        mModels = new ArrayList<User>();
        mKeys = new ArrayList<String>();
        mListener = this.mRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, final String previousChildName) {

                if (!dataSnapshot.getValue(String.class).equals("")) {
                    final String modelString = dataSnapshot.getValue(FirebaseListAdapterParticipant.this.mModelClass);
                    //Log.v("previouschild", previousChildName );
                    final String key = dataSnapshot.getKey();
                    //Log.v("key", key );
                    String ref= dataSnapshot.getRef().toString();
                    Log.v("mencoba", ref );
                    mRefUser.orderByChild("username").equalTo(modelString).addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                            Log.v("mrefparticipant", dataSnapshot.getRef().toString());
                            //Log.v("mencobapath",dataSnapshot.getRef().getPath().toString());
                            User newUser = dataSnapshot.getValue(User.class);
                            //Log.v("prevchild",previousChildName);

                            if(!stringtampung.contains(newUser.getId()))
                            {
                                stringtampung.add(newUser.getId());
                                if (previousChildName == null) {
                                    //Log.v("Ini Null",model.toString());
                                    mModels.add(0, newUser);
                                    //Log.v("Nyoba models nul",mModels.toString());
                                    mKeys.add(0, key);
                                } else {
                                    //Log.v("Nggak Null",model.toString());
                                    int previousIndex = mKeys.indexOf(previousChildName);
                                    int nextIndex = previousIndex + 1;
                                    if (nextIndex == mModels.size()) {
                                        mModels.add(newUser);
                                        //Log.v("Nyoba models 1",mModels.toString());
                                        mKeys.add(key);
                                    } else {
                                        mModels.add(nextIndex, newUser);
                                        //Log.v("Nyoba models 2",mModels.toString());
                                        mKeys.add(nextIndex, key);
                                    }
                                }
                                //Log.v("coba mModels", mModels.get(0).getFirst().getGoingTo());
                                notifyDataSetChanged();
                                //Pair<String, User> pair = new Pair<String, User>(modelString, newUser);
                            }
                        }

                        @Override
                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                            notifyDataSetChanged();
                        }

                        @Override
                        public void onChildRemoved(DataSnapshot dataSnapshot) {


                            notifyDataSetChanged();
                        }

                        @Override
                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {
                            notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {

                        }
                    } );
                    /*{
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            Log.v("mencobapath",dataSnapshot.getRef().getPath().toString());
                            User newUser = dataSnapshot.getValue(User.class);
                            //Pair<String, User> pair = new Pair<String, User>(modelString, newUser);
                            if (previousChildName == null) {
                                //Log.v("Ini Null",model.toString());
                                mModels.add(0, newUser);
                                //Log.v("Nyoba models nul",mModels.toString());
                                mKeys.add(0, key);
                            } else {
                                //Log.v("Nggak Null",model.toString());
                                int previousIndex = mKeys.indexOf(previousChildName);
                                int nextIndex = previousIndex + 1;
                                if (nextIndex == mModels.size()) {
                                    mModels.add(newUser);
                                    //Log.v("Nyoba models 1",mModels.toString());
                                    mKeys.add(key);
                                } else {
                                    mModels.add(nextIndex, newUser);
                                    //Log.v("Nyoba models 2",mModels.toString());
                                    mKeys.add(nextIndex, key);
                                }
                            }
                            //Log.v("coba mModels", mModels.get(0).getFirst().getGoingTo());
                            notifyDataSetChanged();
                            //HashMap<String,User> userHashMap = dataSnapshot.getValue(FirebaseListAdapterParticipant.this.modelClass);
                            /*mRefUser.child(id).addListenerForSingleValueEvent(new ValueEventListener(){
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    User newUser = dataSnapshot.getValue(User.class);
                                    //Pair<String, User> pair = new Pair<String, User>(modelString, newUser);
                                    if (previousChildName == null) {
                                        //Log.v("Ini Null",model.toString());
                                        mModels.add(0, newUser);
                                        //Log.v("Nyoba models nul",mModels.toString());
                                        mKeys.add(0, key);
                                    } else {
                                        //Log.v("Nggak Null",model.toString());
                                        int previousIndex = mKeys.indexOf(previousChildName);
                                        int nextIndex = previousIndex + 1;
                                        if (nextIndex == mModels.size()) {
                                            mModels.add(newUser);
                                            //Log.v("Nyoba models 1",mModels.toString());
                                            mKeys.add(key);
                                        } else {
                                            mModels.add(nextIndex, newUser);
                                            //Log.v("Nyoba models 2",mModels.toString());
                                            mKeys.add(nextIndex, key);
                                        }
                                    }
                                    //Log.v("coba mModels", mModels.get(0).getFirst().getGoingTo());
                                    notifyDataSetChanged();
                                }
                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }
                            });*/

                        //}
//                        @Override
//                        public void onCancelled(FirebaseError firebaseError) {
//
//                        }
            //        });
                    notifyDataSetChanged();
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                //One of the mModels changed. Replace it in our list and name mapping
                /*
                final String newModel = dataSnapshot.getValue(String.class);
                mRefUser.orderByValue().equalTo(newModel).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String key = dataSnapshot.getKey();
                        User newUser = dataSnapshot.getValue(User.class);
                        Pair<String, User> newPair = new Pair<String, User>(newModel, newUser);
                        int index = mKeys.indexOf(key);

                        mModels.set(index, newPair);

                        notifyDataSetChanged();
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });*/
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {


            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, final String previousChildName) {
                // A model changed position in the list. Update our list accordingly
                /*if (!dataSnapshot.getValue(String.class).equals("")) {
                    final String newModel = dataSnapshot.getValue(String.class);
                    mRefUser.orderByValue().equalTo(newModel).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String key = dataSnapshot.getKey();
                            User newUser = dataSnapshot.getValue(User.class);
                            Log.v("mencoba", newUser.getNama());
                            //Pair<String, User> newPair = new Pair<String, User>(newModel, newUser);
                            int index = mKeys.indexOf(key);
                            mModels.remove(index);
                            mKeys.remove(index);
                            if (previousChildName == null) {
                                mModels.add(0, newUser);
                                mKeys.add(0, key);
                            } else {
                                int previousIndex = mKeys.indexOf(previousChildName);
                                int nextIndex = previousIndex + 1;
                                if (nextIndex == mModels.size()) {
                                    mModels.add(newUser);
                                    mKeys.add(key);
                                } else {
                                    mModels.add(nextIndex, newUser);
                                    mKeys.add(nextIndex, key);
                                }
                            }
                            notifyDataSetChanged();

                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {
                            Log.e("FirebaseListAdapter", "Listen was cancelled, no more updates will occur");
                        }
                    });
                }*/
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {
                Log.e("FirebaseListAdapter", "Listen was cancelled, no more updates will occur");
            }

        });
    }
    @Override
    public int getCount() {
        return mModels.size();
    }

    @Override
    public Object getItem(int position) {
        return mModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            convertView = mInflater.inflate(mLayout, parent, false);
        }

        User model = mModels.get(position);
        Log.v("index", String.valueOf(position));
        Log.v("model", model.getProfpic());
        populateView(convertView, model);
        return convertView;
    }
    protected abstract void populateView(View v, User model);
}
