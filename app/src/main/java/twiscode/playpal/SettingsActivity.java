package twiscode.playpal;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.preference.Preference;
import android.preference.PreferenceFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Toast;

import com.facebook.login.LoginManager;
import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class SettingsActivity extends AppCompatActivity {

    Toolbar mToolbar;
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    static String linkdownloadandroid = "";
    private final static String APP_TITLE = "PlayPal";// App Name
    //private final static String APP_PNAME = "com.example.name";// Package Name

    private final static int DAYS_UNTIL_PROMPT = 3;//Min number of days
    private final static int LAUNCHES_UNTIL_PROMPT = 3;//Min number of launches

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Lato-Regular.ttf");
        Firebase.setAndroidContext(this);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        if(!isNetworkAvailable())
        {
            Toast.makeText(SettingsActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
        // Display the fragment as the main content.
        mDatabase.child("download").addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        download download = dataSnapshot.getValue(download.class);
                        linkdownloadandroid = download.getAndroid();
                        //Toast.makeText(SettingsActivity.this, linkdownloadandroid,
                        //        Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("Error:mDatabase", "getUser:onCancelled", databaseError.toException());
                    }
                });

        getFragmentManager().beginTransaction().replace(R.id.container,
                new PrefsFragment()).commit();
        mToolbar = (Toolbar) findViewById(R.id.toolbar1);
        mToolbar.setNavigationIcon(R.drawable.ic_action_name_white);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case android.R.id.home:
                //Intent j = new Intent(SettingsActivity.this, MainActivity.class);
                //startActivity(j);
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    @Override
    public void onBackPressed() {
        //Intent i = new Intent(SettingsActivity.this, MainActivity.class);
        //startActivity(i);
        //overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
        finish();
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public class PrefsFragment extends PreferenceFragment {
        PrefsFragment(){}
        private FirebaseAuth mAuth;
        private FirebaseAuth.AuthStateListener mAuthListener;
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.preferences);
            Firebase.setAndroidContext(getApplicationContext());
            mAuth = FirebaseAuth.getInstance();
            /*final Preference myPref = (Preference) findPreference("rateandfeedback");
            myPref.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    Intent i = new Intent(android.content.Intent.ACTION_VIEW);
                    i.setData(Uri.parse(linkdownloadandroid));
                    startActivity(i);
                    return true;
                }
            });*/
            final Preference myPrefOut = (Preference) findPreference("logout");
            myPrefOut.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
                public boolean onPreferenceClick(Preference preference) {
                    if(isNetworkAvailable()) {
                        mAuth.removeAuthStateListener(mAuthListener);
                        FirebaseAuth.getInstance().signOut();
                        //Auth.GoogleSignInApi.signOut(mGoogleApiClient);
                        LoginManager.getInstance().logOut();

                        Intent ii = new Intent(getApplicationContext(), LoginActivity.class);
                        ii.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(ii);

                        finish();
                    }
                    else Toast.makeText(SettingsActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
                    return true;
                }
            });
        }
        private boolean isNetworkAvailable() {
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        }
    }
}
