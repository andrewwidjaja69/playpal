package twiscode.playpal;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import twiscode.playpal.Utilities.ConfigManager;

/**
 * Created by aldo on 29/03/2016.
 */
public class ThreeFragment extends Fragment {

    ConfigManager appManager;
    Dialog dialog;
    Firebase mRef = new Firebase(appManager.FIREBASE).child("posts");
    private String mUsername;
    private String userId = FirebaseAuth.getInstance().getCurrentUser().getUid();
    private ListAdapterPost mPostListAdapter;
    Context context = getContext();
    // private static RecyclerView.Adapter adapter;
    //private RecyclerView.LayoutManager layoutManager;
    //private static RecyclerView recyclerView;
    //private static ArrayList<DataModel> data;{
    // Required empty public constructor
    //}
    Firebase usernameRef = new Firebase(appManager.FIREBASE).child("users").child(userId).child("username");
    Firebase userRef = new Firebase(appManager.FIREBASE).child("users").child(userId);
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final Activity activity = this.getActivity();
        final View root = inflater.inflate(R.layout.fragment_three, container, false);

        final LinearLayout followingBtn = (LinearLayout) root.findViewById(R.id.profile_following_button);
        final LinearLayout followersBtn = (LinearLayout) root.findViewById(R.id.profile_followers_button);
        final ScrollView scrollView = (ScrollView) root.findViewById(R.id.layout_scroll);
        Firebase ref = new Firebase(appManager.FIREBASE).child("follow").child(userId);
        final TextView textNama = (TextView) root.findViewById(R.id.text_nama_fragment_three);
        final TextView textUserneme = (TextView) root.findViewById(R.id.text_username_fragment_three);
        final CircleImageView profpic = (CircleImageView) root.findViewById(R.id.prof_pic_fragment_three);
        final TextView textFollowing = (TextView) root.findViewById(R.id.text_following);
        final TextView textFollowers = (TextView) root.findViewById(R.id.text_follower);
        final Button btnProfile = (Button) root.findViewById(R.id.button_follow);
        final TextView postSum = (TextView) root.findViewById(R.id.post_sum);
        if(!isNetworkAvailable())
        {
            Toast.makeText(getContext(), "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
        else {
            btnProfile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent j = new Intent(activity, edit_profile.class);
                    startActivity(j);
                }
            });
            userRef.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    Log.v("lala", user.getNama());

                    textNama.setText(user.getNama());
                    textUserneme.setText(user.getUsername());
                    final String profpicUrl = user.getProfpic();
                    if (!profpicUrl.equals("")) {
                        String profpicUri = "https://graph.facebook.com/" + FirebaseAuth.getInstance().getCurrentUser().getProviderData().get(1).getUid() + "/picture?height=720&width=720";
                        Picasso.with(context).load(profpicUrl).into(profpic);
                        if (profpic != null) {
                            profpic.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    String profpicUri = "https://graph.facebook.com/" + FirebaseAuth.getInstance().getCurrentUser().getProviderData().get(1).getUid() + "/picture?height=720&width=720";
                                    showDialog(profpicUrl);
                                }
                            });
                        }
                    }
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
            ref.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    Follow follows = dataSnapshot.getValue(Follow.class);
                    ArrayList<String> followers = follows.getFollowers();
                    ArrayList<String> following = follows.getFollowing();
                    //if(!followers.contains("")){
                    textFollowers.setText(String.valueOf(followers.size() - 1));
                    //}
                    //if(!following.contains("")){
                    textFollowing.setText(String.valueOf(following.size() - 1));
                    //}
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });

            followingBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), FollowActivity.class);
                    i.putExtra("status", "Following");
                    i.putExtra("userId", userId);
                    startActivity(i);
                }
            });
            followersBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getActivity(), FollowActivity.class);
                    i.putExtra("status", "Followers");
                    i.putExtra("userId", userId);
                    startActivity(i);
                }
            });


            usernameRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    mUsername = dataSnapshot.getValue().toString();
                    Log.v("username", mUsername);
                    final ListView listView = (ListView) root.findViewById(android.R.id.list);
                    Log.v("usernameLuar", mUsername);

                    mPostListAdapter = new ListAdapterPost(getContext(), mRef.orderByChild("timeStamp"), activity, R.layout.news_item, mUsername, "profile", userId, new ArrayList<String>());
                    listView.setAdapter(mPostListAdapter);

                    mPostListAdapter.registerDataSetObserver(new DataSetObserver() {
                        @Override
                        public void onChanged() {
                            super.onChanged();

                            setListViewHeightBasedOnChildren(listView, postSum);
                            //
                        }

                        @Override
                        public void onInvalidated() {
                            super.onInvalidated();
                        }
                    });
                }

                @Override
                public void onCancelled(FirebaseError firebaseError) {

                }
            });
        }
        return root;
    }

    public static void setListViewHeightBasedOnChildren(ListView listView, TextView postSum)
    {
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null)
            return;

        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.UNSPECIFIED);
        int totalHeight=0;
        View view = null;

        for (int i = 0; i < listAdapter.getCount(); i++)
        {
            view = listAdapter.getView(i, view, listView);

            if (i == 0)
                view.setLayoutParams(new ViewGroup.LayoutParams(desiredWidth,
                        ViewGroup.LayoutParams.MATCH_PARENT));

            view.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += view.getMeasuredHeight();

        }
        int count = listAdapter.getCount();
        Log.v("mencobacoba", String.valueOf(count));
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        params.height = totalHeight + ((listView.getDividerHeight()) * (listAdapter.getCount()));
        Log.v("cobalaka", String.valueOf(params.height));
        listView.setLayoutParams(params);
        listView.requestLayout();
        if(listAdapter!=null&&!listAdapter.isEmpty()&&count!=0){
            String texting = String.valueOf(count) +" post";
            postSum.setText(texting);
        }
    }

    private void showDialog(String profpic) {
        // custom dialog
        dialog = new Dialog(getContext());
        dialog.setContentView(R.layout.custom_dialog);
        RelativeLayout layout = (RelativeLayout) dialog.findViewById(R.id.dialog_relative_layout);
        ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) layout.getLayoutParams();
        int widthPixels = getResources().getDisplayMetrics().widthPixels;
        Converter converter = new Converter();
        int minusHeight = converter.dpToPx(40,getContext());
        //int height = size.y;
        params.height = widthPixels - minusHeight;
        layout.setLayoutParams(params);
        layout.requestLayout();
        final ProgressBar imageDialog = (ProgressBar) dialog.findViewById(R.id.image_loading);
        imageDialog.setVisibility(View.VISIBLE);

        // set the custom dialog components - text, image and button

        ImageButton close = (ImageButton) dialog.findViewById(R.id.btnClose);
        final ImageView image =  (ImageView) dialog.findViewById(R.id.imageFull);
        Picasso.with(getContext()).load(profpic).into(image, new Callback() {
            @Override
            public void onSuccess() {
                imageDialog.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });

        // Close Button
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                //TODO Close button action
            }
        });

        // Buy Button


        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        dialog.show();
    }


}
