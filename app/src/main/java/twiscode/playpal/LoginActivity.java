package twiscode.playpal;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.GoogleAuthProvider;
import com.google.firebase.auth.UserInfo;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import twiscode.playpal.Utilities.ApplicationData;
import twiscode.playpal.Utilities.ConfigManager;

//import com.google.firebase.auth.FacebookAuthProvider;

public class LoginActivity extends AppCompatActivity {
    LoginButton loginButton;
    //private GoogleApiClient mGoogleApiClient;
    //SignInButton signInButton;
    private String providerId;
    private static final String TAG = "TAG";
    private DatabaseReference mDatabase;
    private FirebaseAuth mAuth;
    private ProgressDialog mAuthProgressDialog;
    CallbackManager callbackManager;
    /* Used to track user logging in/out off Facebook */
    private AccessTokenTracker mFacebookAccessTokenTracker;
    private EditText textEmail;
    private EditText textPass;
    //private String nama,bio,gender,number,website,username,usid,usemail;
    //private Uri fUri;
    Firebase dataRef;
    //private EditText regName;
    //private EditText regEmail;
    //private EditText regPass;
    private Button loginEButton;
    private Button registerEButton;
    private AuthData mAuthData;
    /* Listener for Firebase session changes */
    private FirebaseAuth.AuthStateListener mAuthListener;
    private static final int RC_SIGN_IN = 9001;
    ConfigManager appManager;
    //Firebase firebaseRef;
    User userJ;
    ApplicationData appData;
    private CallbackManager mCallbackManager;
    private ArrayList<String> follower = new ArrayList<String>();
    private ArrayList<String> following = new ArrayList<String>();
    private ArrayList<String> reqFollow = new ArrayList<String>();
    private ArrayList<String> reqingFollow = new ArrayList<String>();

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getApplicationContext());
        setContentView(R.layout.activity_login);
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Lato-Regular.ttf");
        //facebookSDKInitialize();
        Firebase.setAndroidContext(this);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        if(!isNetworkAvailable()){
            Toast.makeText(LoginActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
        //firebaseRef = new Firebase(appManager.FIREBASE);
        // Configure Google Sign In
        /*GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.CREDENTIALS_API)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();*/
        //FacebookSdk.sdkInitialize(getApplicationContext());
        //AppEventsLogger.activateApp(this);
        // [START initialize_fblogin]
        // Initialize Facebook Login button
        mCallbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.login_button);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.d(TAG, "facebook:onSuccess:" + loginResult);
                if(!isNetworkAvailable()){
                    Toast.makeText(LoginActivity.this, "Connection Failed", Toast.LENGTH_SHORT).show();
                }
                else handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {
                Log.d(TAG, "facebook:onCancel");
                Toast.makeText(LoginActivity.this, "Facebook Connection Canceled", Toast.LENGTH_SHORT).show();
                // [START_EXCLUDE]
                //updateUI(null);
                // [END_EXCLUDE]
            }

            @Override
            public void onError(FacebookException error) {
                Log.d(TAG, "facebook:onError", error);
                Toast.makeText(LoginActivity.this, "Facebook Connection Failed", Toast.LENGTH_SHORT).show();
                // [START_EXCLUDE]
                //updateUI(null);
                // [END_EXCLUDE]
            }
        });
        // [END initialize_fblogin]
        //loginButton = (LoginButton) findViewById(R.id.login_button);
        //firebaseRef.setAndroidContext(this);
        textEmail = (EditText) findViewById(R.id.editText2);
        textPass = (EditText) findViewById(R.id.editText3);
        textPass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        textEmail.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        loginEButton = (Button) findViewById(R.id.button);
        registerEButton = (Button) findViewById(R.id.button2);
        //Firebase userRef = rootRef.child(mAuthData.getUid() + "/");
       // signInButton = (SignInButton) findViewById(R.id.sign_in_button);

        //signInButton.setSize(SignInButton.SIZE_STANDARD);
        //signInButton.setScopes(gso.getScopeArray());
        //regName = (EditText) findViewById(R.id.editText4);
        //regEmail = (EditText) findViewById(R.id.editText5);
        //regPass = (EditText) findViewById(R.id.editText6);
        //firebaseRef.unauth();
        //LoginManager.getInstance().logOut();
        /*mFacebookAccessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(AccessToken oldAccessToken, AccessToken currentAccessToken) {
                Log.i("Token Tracker", "Facebook.AccessTokenTracker.OnCurrentAccessTokenChanged");
                LoginActivity.this.onFacebookAccessTokenChange(currentAccessToken);
            }
        };
        loginButton.setReadPermissions(Arrays.asList(
                "public_profile", "email", "user_birthday", "user_friends"));
        getLoginDetails(loginButton);*/
        /*registerEButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                registerWithPassword(regName.getText().toString(),regEmail.getText().toString(),regPass.getText().toString());
            }

        });*/
        /*signInButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                startActivityForResult(signInIntent, RC_SIGN_IN);
            }
        });*/
        loginEButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(!isNetworkAvailable()){
                    Toast.makeText(LoginActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
                }
                else signIn(textEmail.getText().toString(),textPass.getText().toString());
            }
        });
        textPass.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    loginEButton.performClick();
                    return true;
                }
                return false;
            }
        });
        registerEButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Intent i = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(i);
            }
        });
        mAuthProgressDialog = new ProgressDialog(this);
        mAuthProgressDialog.setTitle("Loading");
        mAuthProgressDialog.setMessage("Signing in...");
        mAuthProgressDialog.setCancelable(false);
        //mAuthProgressDialog.show();
        FirebaseUser userF = FirebaseAuth.getInstance().getCurrentUser();
        // [START auth_state_listener]
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                //User userTemp;
                final FirebaseUser user = firebaseAuth.getCurrentUser();
                /**/

                if (user != null) {
                    // User is signed in
                    for (UserInfo profile : user.getProviderData()) {
                        // Id of the provider (ex: google.com)
                        providerId = profile.getProviderId();
                    };
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getDisplayName());
                    Log.v("provid",providerId);
                    Intent i = new Intent(LoginActivity.this, MainActivity.class);
                    startActivity(i);
                    finish();
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // [START_EXCLUDE]

                // [END_EXCLUDE]
            }
        };
        // [END auth_state_listener]
    }
    public boolean isEmailValid(String email)
    {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(matcher.matches()) {
            textEmail.setError(null);
            return true;
        }
        else {
            textEmail.setError("Not an email.");
            return false;
        }
    }
    // [START auth_with_facebook]
    private void handleFacebookAccessToken(AccessToken token) {
        Log.d(TAG, "handleFacebookAccessToken:" + token);
        // [START_EXCLUDE silent]
        mAuthProgressDialog.show();
        // [END_EXCLUDE]
        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        Log.w("token",token.getToken().toString());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull final Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        //String foto = "https:"+task.getResult().getUser().getPhotoUrl().getSchemeSpecificPart();
                        String fotobaru = "https://graph.facebook.com/"+task.getResult().getUser().getProviderData().get(1).getUid()+"/picture?height=720&width=720";
                        Log.d("masalahfoto", fotobaru);
                        //task.getResult().getUser().getEmail();
                        final User iniuser = new User(task.getResult().getUser().getUid(), task.getResult().getUser().getDisplayName().toString(), "", fotobaru, "", "", "", "",0);
                        //isiUser(,,,task.getResult().getUser().getEmail());
                        mAuthProgressDialog.show();
                        mDatabase.child("users").child(task.getResult().getUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                if (dataSnapshot.exists()) {
                                    // TODO: handle the case where the data already exists
                                    mAuthProgressDialog.hide();
                                    return;
                                }
                                else {
                                    mAuthProgressDialog.hide();
                                    Follow mFollow = new Follow();
                                    mDatabase.child("users").child(task.getResult().getUser().getUid()).setValue(iniuser);
                                    FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                                    DatabaseReference myRef = firebaseDatabase.getReference().child("activity").child(task.getResult().getUser().getUid()).push();
                                    //DatabaseReference myRef = mDatabase.child("activity").child(task.getResult().getUser().getUid().toString()).push();
                                    //pushFirebaseRef = firebaseRef.child("activity").push();
                                    //final String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
                                    //mDatabase.child("activity").child(task.getResult().getUser().getUid().toString()).push();
                                    final Long ts = System.currentTimeMillis()/1000*(-1);
                                    //mDatabase.child("activity").child(task.getResult().getUser().getUid().toString()).push().setValue(new Activities("PlayPal","Welcome","PlayPal","Welcome","Welcome",mDatabase.getKey(),ts));
                                    //Log.v("iniactkey",myRef.getKey());
                                    Activities activities = new Activities("PlayPal","Welcome","PlayPal","Welcome","Welcome",myRef.getKey(),ts);
                                    myRef.setValue(activities);
                                    following.add("");
                                    follower.add("");
                                    reqFollow.add("");
                                    reqingFollow.add("");
                                    mFollow.setFollowing(following);
                                    mFollow.setFollowers(follower);
                                    mFollow.setRequestedFollow(reqFollow);
                                    mFollow.setRequestingFollow(reqingFollow);
                                    mDatabase.child("follow").child(task.getResult().getUser().getUid()).setValue(mFollow);
                                    Intent i = new Intent(LoginActivity.this, edit_profile.class);
                                    startActivity(i);
                                    finish();
                                    return;
                                }
                            }
                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Toast.makeText(LoginActivity.this, "Login Failed.",
                                        Toast.LENGTH_SHORT).show();
                            }
                        });
                        //nama = task.getResult().getUser().getDisplayName().toString();
                        //Toast.makeText(LoginActivity.this, task.getResult().getUser().getEmail(),
                        //        Toast.LENGTH_SHORT).show();
                        //fUri = task.getResult().getUser().getPhotoUrl();
                        //usid = task.getResult().getUser().getUid();
                        //usemail = task.getResult().getUser().getEmail();
                        //Log.w("hasil task", task.getResult().getUser().toString());
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // [START_EXCLUDE]
                        mAuthProgressDialog.hide();
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END auth_with_facebook]

    public static String Random() {
        Random generator = new Random();
        StringBuilder randomStringBuilder = new StringBuilder();
        int randomLength = generator.nextInt(32);
        char tempChar;
        for (int i = 0; i < randomLength; i++){
            tempChar = (char) (generator.nextInt(96) + 32);
            randomStringBuilder.append(tempChar);
        }
        return randomStringBuilder.toString();
    }

    private void isiUser(String iniid, String Nama, String fotoUrl, String usemail)
    {
        String random = Random();
        random = random.replace(" ","");
        random = random.trim();
        String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
        userJ = new User(iniid, Nama, "", fotoUrl, "", "", "", "",0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);

        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }
    // [START auth_with_google]
    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {
        Toast.makeText(LoginActivity.this, "firebaseAuthWithGoogle",
                Toast.LENGTH_SHORT).show();
        Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId());
        // [START_EXCLUDE silent]
        mAuthProgressDialog.show();
        // [END_EXCLUDE]

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithCredential:onComplete:" + task.isSuccessful());
                        Intent i = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithCredential", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                        // [START_EXCLUDE]
                        mAuthProgressDialog.hide();
                        // [END_EXCLUDE]
                    }
                });
    }
    // [END auth_with_google]

    // [START handleSignInResult]

    // [END handleSignInResult]

    // [START on_start_add_listener]
    @Override
    public void onResume() {
        super.onResume();
        if(!isNetworkAvailable()){
            Toast.makeText(LoginActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
        else {
            mAuth.addAuthStateListener(mAuthListener);
        }
    }
    @Override
    public void onStart() {
        super.onStart();
        if(!isNetworkAvailable()){
            Toast.makeText(LoginActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
        else {
            mAuth.addAuthStateListener(mAuthListener);
        }
    }
    // [END on_start_add_listener]

    // [START on_stop_remove_listener]
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
    // [END on_stop_remove_listener]

    @Override
    public void onPause() {
        super.onPause();
        mAuthProgressDialog.dismiss();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mAuthProgressDialog.dismiss();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private boolean validateForm() {
        boolean valid = true;

        String email = textEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            textEmail.setError("Required.");
            valid = false;
        } else {
            textEmail.setError(null);
        }

        String password = textPass.getText().toString();
        if (TextUtils.isEmpty(password)) {
            textPass.setError("Required.");
            valid = false;
        } else {
            textPass.setError(null);
        }

        return valid;
    }

    private void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if (!validateForm()) {
            return;
        }
        if(!isEmailValid(email)){
            return;
        }

        mAuthProgressDialog.show();

        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail", task.getException());
                            Toast.makeText(LoginActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // [START_EXCLUDE]
                        mAuthProgressDialog.hide();
                        // [END_EXCLUDE]
                    }
                });
        // [END sign_in_with_email]
    }
    @Override
    public void onBackPressed() {
        android.os.Process.killProcess(android.os.Process.myPid());
    }
}
