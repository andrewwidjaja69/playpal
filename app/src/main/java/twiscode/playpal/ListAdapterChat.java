package twiscode.playpal;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.client.Query;

/**
 * Created by Crusader on 6/30/2016.
 */
public class ListAdapterChat extends FirebaseListAdapterChat<FriendlyMessage>{
    private String mUsername;
    private LinearLayout message_row;
    private LinearLayout item_message;

    public ListAdapterChat(Query ref, Activity activity, int layout, String mUsername) {
        super(ref, FriendlyMessage.class, layout, activity, mUsername);
        this.mUsername = mUsername;
    }
    @Override
    protected void populateView(View view, FriendlyMessage chat) {
        // Map a Chat object to an entry in our listview

        float d = view.getResources().getDisplayMetrics().density;
        int margin = (int)(100 * d);
        message_row = (LinearLayout) view.findViewById(R.id.singleMessageContainer);
        item_message = (LinearLayout) view.findViewById(R.id.singleMessageContainer2);
        String author = chat.getName();
        String username = chat.getUsername();
        TextView authorText = (TextView) view.findViewById(R.id.author);

        TextView message = (TextView) view.findViewById(R.id.message);
        message.setText(chat.getText());
        // If the message was sent by this user, color it differently
        if (author != null && username.equals(mUsername)) {
            //authorText.setTextColor(Color.parseColor("#FFFFFF"));
            item_message.setBackgroundResource(R.drawable.layout_bg1);
            message.setTextColor(Color.parseColor("#000000"));
            authorText.setVisibility(View.GONE);
            //authorText.setText(author);
            //authorText.setBackgroundResource(R.drawable.bubble_a);
            //message_row.setHorizontalGravity(Gravity.RIGHT);
            message_row.setGravity(Gravity.RIGHT);
            //ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) message_row.getLayoutParams();
            //params.rightMargin = 100;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(margin,0,0,0);
            item_message.setLayoutParams(layoutParams);
            //message_row.setBackgroundResource(R.drawable.bubble_a);
        }
        else {
            authorText.setVisibility(View.VISIBLE);
            authorText.setText(author);
            authorText.setTextColor(Color.parseColor("#000000"));
            item_message.setBackgroundResource(R.drawable.layout_bg);
            //item_message.setBackgroundColor(Color.parseColor("#A2A2A3"));
            message.setTextColor(Color.parseColor("#000000"));
            //authorText.setBackgroundResource(R.drawable.bubble_b);
            //message_row.setHorizontalGravity(Gravity.LEFT);
            message_row.setGravity(Gravity.LEFT);
            //ViewGroup.MarginLayoutParams params = (ViewGroup.MarginLayoutParams) message_row.getLayoutParams();
            //params.leftMargin = 100;
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams
                    (LinearLayout.LayoutParams.WRAP_CONTENT,LinearLayout.LayoutParams.WRAP_CONTENT);
            layoutParams.setMargins(0,0,margin,0);
            item_message.setLayoutParams(layoutParams);
            //message_row.setBackgroundResource(R.drawable.bubble_b);
        }
    }
}
