package twiscode.playpal;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.app.ProgressDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.EditText;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphRequestAsyncTask;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import android.app.ProgressDialog;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import android.text.TextUtils;
import twiscode.playpal.Utilities.ApplicationData;
import twiscode.playpal.Utilities.ConfigManager;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import android.support.annotation.NonNull;

public class RegisterActivity extends AppCompatActivity {
    private static final String TAG = "EmailPassword";
    private DatabaseReference mDatabase;
    Firebase pushFirebaseRef;
    private FirebaseAuth mAuth;
    private ProgressDialog mAuthProgressDialog;
    private EditText textEmail;
    private EditText textPass;
    private EditText textUsername;
    private EditText textName;
    private EditText textPhone;
    private EditText textBirth;
    private EditText textGender;
    private Button buttonReg;
    private SimpleDateFormat dateFormatter;
    private Spinner spGender;
    private String genderType[] = {"Male","Female"};
    private ArrayAdapter<String> adapterGenderType;
    /* Listener for Firebase session changes */
    private FirebaseAuth.AuthStateListener mAuthListener;
    private String itemG;
    private DatePickerDialog fromDatePickerDialog;
    private Follow mFollow = new Follow();
    private ArrayList<String> follower = new ArrayList<String>();
    private ArrayList<String> following = new ArrayList<String>();
    private ArrayList<String> reqFollow = new ArrayList<String>();
    private ArrayList<String> reqingFollow = new ArrayList<String>();
    ConfigManager appManager;
    Firebase firebaseRef;
    User user;
    ApplicationData appData;
    @Override
    public void onResume() {
        super.onResume();
        if(!isNetworkAvailable()){
            Toast.makeText(RegisterActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
        else {
            mAuth.addAuthStateListener(mAuthListener);
        }
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        Firebase.setAndroidContext(this);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        textEmail = (EditText)findViewById(R.id.email);
        textPass = (EditText)findViewById(R.id.pass);
        textPass.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        textEmail.setInputType(InputType.TYPE_TEXT_VARIATION_EMAIL_ADDRESS);
        textUsername = (EditText)findViewById(R.id.username);
        textName = (EditText)findViewById(R.id.nick);
        buttonReg = (Button) findViewById(R.id.register);
        textPhone = (EditText)findViewById(R.id.phone);
        //textBirth = (EditText)findViewById(R.id.birth);
        textBirth = (EditText) findViewById(R.id.birth);
        textBirth.setInputType(InputType.TYPE_NULL);
        //textBirth.requestFocus();
        setDateTimeField();
        dateFormatter = new SimpleDateFormat("dd-MM-yyyy", Locale.US);
        //textGender = (EditText)findViewById(R.id.gender);
        spGender = (Spinner) findViewById(R.id.spGender);
        if(!isNetworkAvailable()){
            Toast.makeText(RegisterActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
        adapterGenderType = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, genderType);
        adapterGenderType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spGender.setAdapter(adapterGenderType);
        spGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> adapter, View v,
                                       int position, long id) {
                // On selecting a spinner item
                itemG = adapter.getItemAtPosition(position).toString();

                // Showing selected spinner item
                //Toast.makeText(getApplicationContext(),
                //       "Gender : " + itemG, Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
        mAuthProgressDialog = new ProgressDialog(this);
        mAuthProgressDialog.setTitle("Loading");
        mAuthProgressDialog.setMessage("Signing Up...");
        mAuthProgressDialog.setCancelable(false);
        buttonReg.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                //
                if(!isNetworkAvailable()){
                    Toast.makeText(RegisterActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
                }
                else {registerWithPasswordCheck(textName.getText().toString(), textEmail.getText().toString(), textUsername.getText().toString(), textPass.getText().toString(),textPhone.getText().toString()
                        ,textBirth.getText().toString(),itemG);}
                //Toast.makeText(RegisterActivity.this, "All Success", Toast.LENGTH_SHORT).show();
            }
        });
        // [START auth_state_listener]
        mAuthListener = new FirebaseAuth.AuthStateListener() {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user != null) {
                    // User is signed in
                    Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
                } else {
                    // User is signed out
                    Log.d(TAG, "onAuthStateChanged:signed_out");
                }
                // [START_EXCLUDE]
                //updateUI(user);
                // [END_EXCLUDE]
            }
        };
        // [END auth_state_listener]
    }

    private void setDateTimeField() {
        textBirth.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    fromDatePickerDialog.show();
                }
            }
        });
        /*textBirth.setOnFocusChangeListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {

            }
        });*/

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                textBirth.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    // [START on_start_add_listener]
    @Override
    public void onStart() {
        super.onStart();
        if(!isNetworkAvailable()){
            Toast.makeText(RegisterActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
        else {
            mAuth.addAuthStateListener(mAuthListener);
        }
    }
    // [END on_start_add_listener]

    // [START on_stop_remove_listener]
    @Override
    public void onStop() {
        super.onStop();
        if (mAuthListener != null) {
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }
    // [END on_stop_remove_listener]

    private void updateUI(FirebaseUser user) {
        mAuthProgressDialog.hide();
        if (user != null) {
            Intent i = new Intent(RegisterActivity.this, MainActivity.class);
            startActivity(i);
            finish();
        } else {
            Toast.makeText(RegisterActivity.this, "You need to sign in or register",
                    Toast.LENGTH_SHORT).show();
        }
    }

    public boolean isEmailValid(String email)
    {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(matcher.matches()) {
            textEmail.setError(null);
            return true;
        }
        else {
            textEmail.setError("Not an email.");
            return false;
        }
    }

    private boolean validateForm() {
        boolean valid = true;

        String email = textEmail.getText().toString();
        if (TextUtils.isEmpty(email)) {
            textEmail.setError("Required.");
            valid = false;
        } else {
            textEmail.setError(null);
        }

        String password = textPass.getText().toString();
        if (TextUtils.isEmpty(password)) {
            textPass.setError("Required.");
            valid = false;
        } else {
            textPass.setError(null);
        }

        String username = textUsername.getText().toString();
        if (TextUtils.isEmpty(username)) {
            textUsername.setError("Required.");
            valid = false;
        } else {
            textUsername.setError(null);
        }

        String name = textName.getText().toString();
        if (TextUtils.isEmpty(name)) {
            textName.setError("Required.");
            valid = false;
        } else {
            textName.setError(null);
        }

        return valid;
    }

    private void signIn(String email, String password) {
        Log.d(TAG, "signIn:" + email);
        if (!validateForm()) {
            return;
        }
        if(!isEmailValid(email)){
            return;
        }

        //mAuthProgressDialog.show();

        // [START sign_in_with_email]
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "signInWithEmail:onComplete:" + task.isSuccessful());
                        mDatabase.child("users").child(task.getResult().getUser().getUid().toString()).setValue(user);
                        FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance();
                        DatabaseReference myRef = firebaseDatabase.getReference().child("activity").child(task.getResult().getUser().getUid().toString()).push();
                        //DatabaseReference myRef = mDatabase.child("activity").child(task.getResult().getUser().getUid().toString()).push();
                        //pushFirebaseRef = firebaseRef.child("activity").push();
                        //final String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
                        //mDatabase.child("activity").child(task.getResult().getUser().getUid().toString()).push();
                        final Long ts = System.currentTimeMillis()/1000*(-1);
                        //mDatabase.child("activity").child(task.getResult().getUser().getUid().toString()).push().setValue(new Activities("PlayPal","Welcome","PlayPal","Welcome","Welcome",mDatabase.getKey(),ts));
                        Log.v("iniactkey",myRef.getKey());
                        Activities activities = new Activities("PlayPal","Welcome","PlayPal","Welcome","Welcome",myRef.getKey(),ts);
                        myRef.setValue(activities);
                        following.add("");
                        follower.add("");
                        reqFollow.add("");
                        reqingFollow.add("");
                        mFollow.setFollowers(follower);
                        mFollow.setFollowing(following);
                        mFollow.setRequestedFollow(reqFollow);
                        mFollow.setRequestingFollow(reqingFollow);
                        mDatabase.child("follow").child(task.getResult().getUser().getUid().toString()).setValue(mFollow);
                        Intent i = new Intent(RegisterActivity.this, MainActivity.class);
                        startActivity(i);
                        finish();
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Log.w(TAG, "signInWithEmail", task.getException());
                            Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // [START_EXCLUDE]
                        //mAuthProgressDialog.hide();
                        // [END_EXCLUDE]
                    }
                });
        // [END sign_in_with_email]
    }
    private void registerWithPasswordCheck(final String rawName, final String rawEmail, final String username, final String password, final String phone, final String birth, final String gender)
    {
        //user = new User(result.get("uid").toString(), rawName, "", rawEmail, birthday);
        Log.d(TAG, "createAccount:" + rawEmail);
        if (!validateForm()) {
            return;
        }

        mAuthProgressDialog.show();

        Query queryRef = mDatabase.child("users").orderByChild("username").equalTo(username);
        queryRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                if (dataSnapshot.exists()) {
                    // TODO: handle the case where the data already exists
                    Toast.makeText(RegisterActivity.this, "username sudah ada",Toast.LENGTH_SHORT).show();
                    mAuthProgressDialog.hide();
                }
                else {
                    registerWithPassword(rawName,rawEmail,username,password,phone,birth,gender);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    private void registerWithPassword(final String rawName, final String rawEmail, final String username, final String password, final String phone, final String birth, final String gender)
    {
        // [START create_user_with_email]
        mAuth.createUserWithEmailAndPassword(rawEmail, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());
                        user = new User(task.getResult().getUser().getUid().toString(), rawName, username, "", rawEmail, birth, phone, gender,0);
                        //mDatabase.child("users").child(task.getResult().getUser().getUid().toString()).setValue(user);
                        signIn(rawEmail,password);
                        //signIn(rawEmail,password);
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
                            Toast.makeText(RegisterActivity.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }

                        // [START_EXCLUDE]
                        mAuthProgressDialog.hide();
                        // [END_EXCLUDE]
                    }
                });
        // [END create_user_with_email]
    }
}
