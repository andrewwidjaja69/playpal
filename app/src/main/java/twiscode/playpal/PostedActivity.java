package twiscode.playpal;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.firebase.auth.FirebaseAuth;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import twiscode.playpal.Utilities.ConfigManager;

/**
 * Created by Romario on 30/06/2016.
 */
public class PostedActivity extends AppCompatActivity{
    String mUsername;
    ConfigManager appManager;
    Toolbar mToolbar;
    Firebase firebaseRefUser = new Firebase(appManager.FIREBASE).child("users");
    Firebase firebaseRefPost = new Firebase(appManager.FIREBASE).child("posts");
    private RelativeLayout RL;
    private TextView titleini;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Map a Chat object to an entry in our listview
        setContentView(R.layout.news_item);
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_18dp);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle("");
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
        TextView titleini = (TextView) findViewById(R.id.title_text_news);
        RL = (RelativeLayout) findViewById(R.id.news_item_root);
        if(!isNetworkAvailable()){
            RL.setVisibility(View.GONE);
            Toast.makeText(PostedActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
        firebaseRefUser.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).child("username").addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String username = dataSnapshot.getValue(String.class);
                mUsername = username;

                String idPost = getIntent().getStringExtra("postId");
                String idUser = getIntent().getStringExtra("userId");
                firebaseRefPost.child(idPost).addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        final posts post = dataSnapshot.getValue(posts.class);
                        if (!post.isStatusDeleted()){
                            TextView title = (TextView) findViewById(R.id.title_text_news);
                            title.setText(post.getGoingTo());
                            firebaseRefUser.child(post.getUserId()).addValueEventListener(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final User user = dataSnapshot.getValue(User.class);
                                    final ImageView lock = (ImageView) findViewById(R.id.lock_icon);
                                    lock.setVisibility(View.GONE);
                                    if (post.getPrivateStatus().equals("True")) {
                                        lock.setVisibility(View.VISIBLE);
                                    }
                                    final Firebase firebaseRef = new Firebase(appManager.FIREBASE);
                                    final ArrayList<String> waiting = post.getWaitingRecipients();
                                    final Firebase fbUpdateRef = new Firebase(appManager.FIREBASE).child("posts").child(post.getPostId());
                                    final Firebase fbUpdateRefChat = new Firebase(appManager.FIREBASE).child("message");
                                    String nama = user.getNama();
                                    final String privateStatus = post.getPrivateStatus();
                                    final int more = 0;
                                    String profpic = user.getProfpic();
                                    final String goingTo = post.getGoingTo();
                                    String atTime = post.getDateTime();
                                    String picture = post.getImageURL();
                                    Log.v("MencobaLagi", post.getImageURL());
                                    final ArrayList<String> withWho = post.getWithWho();
                                    final ArrayList<String> likers = post.getLikers();
                                    final int likeNumber = likers.size() - 1;
                                    TextView textNama = (TextView) findViewById(R.id.people_name_news);
                                    textNama.setText(nama);
                                    textNama.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            new Firebase(appManager.FIREBASE).child("follow").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addListenerForSingleValueEvent(new com.firebase.client.ValueEventListener() {
                                                @Override
                                                public void onDataChange(com.firebase.client.DataSnapshot dataSnapshot) {
                                                    Follow follow = dataSnapshot.getValue(Follow.class);
                                                    ArrayList<String> following = follow.getFollowing();
                                                    Intent t = new Intent(PostedActivity.this, ProfileUser.class);
                                                    t.putStringArrayListExtra("following", following);
                                                    t.putExtra("userId", user.getId());
                                                    t.putExtra("username", user.getUsername());
                                                    startActivity(t);
                                                }

                                                @Override
                                                public void onCancelled(FirebaseError firebaseError) {

                                                }
                                            });
                                        }
                                    });
                                    ImageView profpicView = (ImageView) findViewById(R.id.prof_pic_news);
                                    if (!profpic.isEmpty()) {
                                        Picasso.with(PostedActivity.this).load(profpic).transform(new CircleTransform()).into(profpicView);
                                    }
                                    if (profpicView != null) {
                                        profpicView.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                new Firebase(appManager.FIREBASE).child("follow").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                                    @Override
                                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                                        Follow follow = dataSnapshot.getValue(Follow.class);
                                                        ArrayList<String> following = follow.getFollowing();
                                                        Intent t = new Intent(PostedActivity.this, ProfileUser.class);
                                                        t.putStringArrayListExtra("following", following);
                                                        t.putExtra("userId", user.getId());
                                                        t.putExtra("username", user.getUsername());
                                                        startActivity(t);
                                                    }

                                                    @Override
                                                    public void onCancelled(FirebaseError firebaseError) {

                                                    }
                                                });
                                            }
                                        });
                                    }
                                    final TextView textGoingTo = (TextView) findViewById(R.id.going_to_news);
                                    String text = "Going to "+"<b>"+goingTo+"</b>";
                                    String textAt ="At "+"<b><i>"+atTime+"</i></b>";
                                    final TextView textAtTime = (TextView) findViewById(R.id.at_news);
                                    textAtTime.setText(Html.fromHtml(textAt));
                                    textGoingTo.setText(Html.fromHtml(text));

                                    RelativeLayout all = (RelativeLayout) findViewById(R.id.layout_news_item);
                                    all.bringToFront();
                                    textGoingTo.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (textGoingTo.getMaxLines() == 1) {
                                                textGoingTo.setMaxLines(999999);
                                                //textGoingTo.setEms(15);
                                                //textGoingTo.setEllipsize(TextUtils.TruncateAt.MIDDLE);

                                            } else
                                                textGoingTo.setMaxLines(1);
                                            //textGoingTo.setEms(15);
                                            //textGoingTo.setEllipsize(TextUtils.TruncateAt.END);
                                        }
                                    });
                                    final FrameLayout frameImage = (FrameLayout) findViewById(R.id.news_picture_layout);
                                    final ProgressBar imageDialog = (ProgressBar) findViewById(R.id.image_dialog);
                                    frameImage.setVisibility(View.VISIBLE);
                                    imageDialog.setVisibility(View.VISIBLE);
                                    final ImageView newsImage = (ImageView) findViewById(R.id.news_picture);
                                    RelativeLayout people_info = (RelativeLayout) findViewById(R.id.people_news);
                                    final int w = people_info.getWidth();
                                    //newsImage.setMaxHeight(w);
                                    //newsImage.setMinimumHeight(w);
                                    Log.v("mencobaImage2", picture);
                                    if (picture.isEmpty()) {
                                        frameImage.setVisibility(View.GONE);
                                    } else {
                                        frameImage.setVisibility(View.VISIBLE);
                                        Picasso.with(PostedActivity.this).load(picture).into(newsImage, new Callback() {
                                            @Override
                                            public void onSuccess() {
                                                imageDialog.setVisibility(View.GONE);

                                            }

                                            @Override
                                            public void onError() {

                                            }
                                        });
                                        newsImage.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) newsImage.getLayoutParams();
                                                if (params.height != w) {
                                                    params.height = w;
                                                } else {
                                                    params.height = w * 2 / 3;
                                                }
                                                newsImage.setLayoutParams(params);
                                            }
                                        });
                                    }

                                    final TextView likeNumbers = (TextView) findViewById(R.id.news_like_info_text);
                                    likeNumbers.setText(String.valueOf(likeNumber) + " pals like this.");
                                    final RelativeLayout likeInfo = (RelativeLayout) findViewById(R.id.news_like_info);
                                    LinearLayout chatBtn = (LinearLayout) findViewById(R.id.news_chat_button);
                                    chatBtn.setVisibility(View.INVISIBLE);
                                    if (withWho.contains(mUsername) || user.getUsername().equals(mUsername)) {
                                        chatBtn.setVisibility(View.VISIBLE);
                                    }
                                    chatBtn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent o = new Intent(PostedActivity.this, ChatActivity2.class);
                                            //o.putExtra("user",mUsername);
                                            o.putExtra("wroom", post.getDateTime());
                                            o.putExtra("idroom", post.getPostId());
                                            o.putExtra("nroom", goingTo);
                                            startActivity(o);
                                        }
                                    });
                                    Log.v("mencobalike", likers.toString());
                                    Log.v("mencobaUser", mUsername);
                                    final LinearLayout goingBtn = (LinearLayout) findViewById(R.id.news_going_button);
                                    final ImageView goingBtnImage = (ImageView) findViewById(R.id.news_going_button_image);
                                    goingBtn.setVisibility(View.INVISIBLE);
                                    if (!withWho.contains(mUsername) && !user.getUsername().equals(mUsername) && !waiting.contains(mUsername)) {
                                        goingBtn.setVisibility(View.VISIBLE);
                                    }
                                    goingBtn.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (privateStatus.equals("True")) {
                                                ArrayList<String> waitingRecipients = waiting;
                                                waitingRecipients.add(mUsername);
                                                fbUpdateRef.child("waitingRecipients").setValue(waitingRecipients);
                                                goingBtn.setVisibility(View.INVISIBLE);
                                                Firebase pushFirebaseRef = firebaseRef.child("activity").child(user.getId()).push();
                                                final String idActivity = pushFirebaseRef.getKey();
                                                Long timeStamp = System.currentTimeMillis() / 1000 * (-1);
                                                Activities newActivity = new Activities(user.getId(), "Request to Join you at", FirebaseAuth.getInstance().getCurrentUser().getUid(), "posts", post.getPostId(), idActivity, timeStamp);
                                                pushFirebaseRef.setValue(newActivity);
                                                Toast.makeText(PostedActivity.this, "Your Going Request have been Sent...", Toast.LENGTH_LONG).show();

                                            } else {


                                                goingBtn.setVisibility(View.INVISIBLE);
                                                ArrayList<String> newWithWho = withWho;
                                                newWithWho.add(mUsername);
                                                if (newWithWho.contains("")) {
                                                    newWithWho.remove("");
                                                }
                                                fbUpdateRef.child("withWho").setValue(newWithWho);
                                                ArrayList<String> usernameInChat = newWithWho;
                                                usernameInChat.add(user.getUsername());
                                                fbUpdateRefChat.child(post.getPostId()).child("username").setValue(usernameInChat);
                                                Firebase pushFirebaseRef = firebaseRef.child("activity").child(user.getId()).push();
                                                final String idActivity = pushFirebaseRef.getKey();
                                                Long timeStamp = System.currentTimeMillis() / 1000 * (-1);
                                                Activities newActivity = new Activities(user.getId(), "Has Joined you at", FirebaseAuth.getInstance().getCurrentUser().getUid(), "posts", post.getPostId(), idActivity, timeStamp);
                                                pushFirebaseRef.setValue(newActivity);
                                                Toast.makeText(PostedActivity.this, "Your Going Request have been Sent...", Toast.LENGTH_LONG).show();
                                            }//goingBtnImage.setImageResource(R.drawable.ayamkuning);
                                        }

                                    });

                                    final ImageView likeBtnImage = (ImageView) findViewById(R.id.news_like_button_image);
                                    if (likers.contains(mUsername)) {
                                        if (likeBtnImage != null) {
                                            likeBtnImage.setImageResource(R.drawable.liked_button);
                                        }
                                    } else {
                                        if (likeBtnImage != null) {
                                            likeBtnImage.setImageResource(R.drawable.like_button);
                                        }
                                    }
                                    LinearLayout like_btn = (LinearLayout) findViewById(R.id.news_like_button);
                                    Log.v("mencobaLikers", String.valueOf(likers.size()));
            /*if(likers.size()==1){
                likeInfo.setVisibility(View.GONE);
            }*/
                                    if (like_btn != null) {
                                        like_btn.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (likers.contains(mUsername)) {
                                                    ArrayList<String> newLikers = likers;
                                                    newLikers.remove(mUsername);
                            /*if(newLikers.size()<2){
                                likeInfo.setVisibility(View.GONE);
                            }*/

                                                    fbUpdateRef.child("likers").setValue(newLikers);
                                                    int newLikeNumber = newLikers.size();
                                                    likeNumbers.setText(String.valueOf(newLikeNumber) + " pals like this");
                                                    likeBtnImage.setImageResource(R.drawable.like_button);


                                                } else {
                                                    ArrayList<String> newLikers = likers;
                                                    newLikers.add(mUsername);
                                                    if (newLikers.size() > 1) {
                                                        likeInfo.setVisibility(View.VISIBLE);
                                                    }
                                                    Firebase fbUpdateRef = new Firebase(appManager.FIREBASE).child("posts").child(post.getPostId());
                                                    fbUpdateRef.child("likers").setValue(newLikers);
                                                    int newLikeNumber = newLikers.size();
                                                    Firebase pushFirebaseRef = firebaseRef.child("activity").child(user.getId()).push();
                                                    final String idActivity = pushFirebaseRef.getKey();
                                                    Log.v("mencobaActivity", idActivity);
                                                    Log.v("mencobaActivity2", pushFirebaseRef.getPath().toString());
                                                    Long timeStamp = System.currentTimeMillis() / 1000 * (-1);
                                                    Activities newActivity = new Activities(user.getId(), "Liked your post", FirebaseAuth.getInstance().getCurrentUser().getUid(), "posts", post.getPostId(), idActivity, timeStamp);
                                                    pushFirebaseRef.setValue(newActivity);
                                                    likeNumbers.setText(String.valueOf(newLikeNumber) + " pals like this");
                                                    likeBtnImage.setImageResource(R.drawable.liked_button);


                                                }
                                            }
                                        });
                                    }
                                }

                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }
                            });
                        }else{
                            TextView title = (TextView) findViewById(R.id.title_text_news);
                            title.setText("Post Deleted");
                            RelativeLayout newsItemRoot = (RelativeLayout) findViewById(R.id.news_item_root);
                            newsItemRoot.setVisibility(View.GONE);
                            RelativeLayout textDeleted = (RelativeLayout) findViewById(R.id.text_deleted);
                            textDeleted.setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onCancelled(FirebaseError firebaseError) {

                    }
                });


            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });


    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //if (drawerToggle.onOptionsItemSelected(item)) {
        //   return true;
        //}

        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
        finish();



    }
}