package twiscode.playpal;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.HashMap;
import java.util.Map;

public class RateAppActivity extends AppCompatActivity {

    RatingBar ratingBar_default;
    TextView text;
    Toolbar mToolbar;
    Button sndBtn;
    private Integer poinrating= 0;
    DatabaseReference mDatabase;
    private FirebaseAuth mAuth;

    private String iniid,ininama,iniusername,inipic,iniemail,inibirthday,ininumber,inigender;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rate_app);
        Firebase.setAndroidContext(this);
        mDatabase = FirebaseDatabase.getInstance().getReference();
        mAuth = FirebaseAuth.getInstance();
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Lato-Regular.ttf");
        mToolbar = (Toolbar) findViewById(R.id.toolbar1);
        mToolbar.setNavigationIcon(R.drawable.ic_action_name_white);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
        ratingBar_default = (RatingBar) findViewById(R.id.ratingbar_default);
        text = (TextView) findViewById(R.id.textView);
        sndBtn = (Button) findViewById(R.id.submitBtn);
        ratingBar_default.setRating(0);
        ratingBar_default.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener(){
            @Override
            public void onRatingChanged(RatingBar ratingBar, float rating,
                                        boolean fromUser) {
                // TODO Auto-generated method stub
                text.setText("Rating: "+String.valueOf(rating));
                poinrating=(int)rating;
            }}
        );
        mDatabase.child("users").child(mAuth.getCurrentUser().getUid()).addListenerForSingleValueEvent(
                new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        // Get user value
                        User user = dataSnapshot.getValue(User.class);
                        //User(mAuth.getCurrentUser().getUid(),unama,uusername,sdownload,uemail,ubirthday,unumber,ugender,rating);
                        iniid = user.getId();
                        ininama = user.getNama();
                        iniusername = user.getUsername();
                        inipic = user.getProfpic();
                        iniemail = user.getEmail();
                        inibirthday = user.getBirthdate();
                        ininumber = user.getPhone();
                        inigender = user.getGender();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        Log.w("!Error", "getUser:onCancelled", databaseError.toException());
                    }
                });
        sndBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if(isNetworkAvailable()) {
                    User userf = new User(iniid, ininama, iniusername, inipic, iniemail, inibirthday, ininumber, inigender, poinrating);
                    //userf.setRating(poinrating);
                    Map<String, Object> postValues = userf.toMap();
                    Map<String, Object> childUpdates = new HashMap<>();
                    childUpdates.put("/users/" + mAuth.getCurrentUser().getUid(), postValues);
                    mDatabase.updateChildren(childUpdates);
                    Toast.makeText(RateAppActivity.this, "Thank You for Rate this App.",
                            Toast.LENGTH_SHORT).show();
                    if (poinrating <= 3) {
                        Intent i = new Intent(RateAppActivity.this, FeedbackActivity.class);
                        startActivity(i);
                        finish();
                    } else {
                        Intent i = new Intent(RateAppActivity.this, SettingsActivity.class);
                        startActivity(i);
                        finish();
                    }
                }
                else
                    Toast.makeText(RateAppActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });
        if(!isNetworkAvailable())
        {
            Toast.makeText(RateAppActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
