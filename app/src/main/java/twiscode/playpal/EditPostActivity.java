package twiscode.playpal;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.firebase.client.ChildEventListener;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;

import twiscode.playpal.Utilities.ConfigManager;

/**
 * Created by Romario on 12/07/2016.
 */
public class EditPostActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener{
    Firebase firebaseRef;
    Firebase pushFirebaseRef;
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl(ConfigManager.FIREBASE_STORAGE);
    private LinearLayout addpeople;
    private LinearLayout adddate;
    private FirebaseAuth mAuth;
    private int PICK_IMAGE_REQUEST = 1;
    private int PEOPLE_SELECTED = 2;
    private int CROP_IMAGE = 3;
    private LinearLayout addphoto;
    private ImageView addphotocontainer;
    private View line_addphoto;
    public static TextView SelectedDateView;
    private static int Year;
    private static int Month;
    private static int Day;
    private static int hour;
    private static int Minute;
    ArrayList<String> withWho = new ArrayList<String>();
    TextView addpeople_text;
    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1;
    private static final int REQUEST_WRITE_STORAGE = 2;

    String privateStatus = null;
    String textGointToString;
    private String datetime;
    public static String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    private Uri selectedImage;
    String uriDownload = null;
    ConfigManager appManager;
    ProgressDialog mAuthProgressDialog;
    User someone = new User();
    ArrayList<String> meWithWho = new ArrayList<String>();
    ArrayList<String> liker = new ArrayList<String>();
    ArrayList<String> waiting = new ArrayList<>();
    byte[] dataFinal;
    AutoCompleteTextView atvPlaces;
    PlacesTask placesTask;
    ParserTask parserTask;
    private Uri mCropImagedUri;
    private Uri mFinalImageUri;
    ArrayList<posts> mPost = new ArrayList<posts>();
    String idPostEditted;
    EditText textGoingTo;
    CheckBox checkBox;
    ArrayList<Double> lat = new ArrayList<Double>();
    ArrayList<Double> lon = new ArrayList<Double>();
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    ProgressDialog dialog;
    String urlImageTemp = "";
    int w;
    /**Crop the image
     * @return returns <tt>true</tt> if crop supports by the device,otherwise false*/
    private boolean performCropImage(){
        try {
            if(mFinalImageUri!=null){
                //call the standard crop action intent (the user device may not support it)
                Intent cropIntent = new Intent("com.android.camera.action.CROP");
                //indicate image type and Uri
                cropIntent.setDataAndType(mFinalImageUri, "image/*");
                //set crop properties
                cropIntent.putExtra("crop", "true");
                //indicate aspect of desired crop
                cropIntent.putExtra("aspectX", 1);
                cropIntent.putExtra("aspectY", 1);
                cropIntent.putExtra("scale", true);
                //indicate output X and Y
                cropIntent.putExtra("outputX", 800);
                cropIntent.putExtra("outputY", 800);
                //retrieve data on return
                cropIntent.putExtra("return-data", true);

                File f = createNewFile("CROP_");
                try {
                    f.createNewFile();
                } catch (IOException ex) {
                    Log.e("io", ex.getMessage());
                }

                mCropImagedUri = Uri.fromFile(f);
                Log.v("coba3",mCropImagedUri.toString());
                cropIntent.putExtra(MediaStore.EXTRA_OUTPUT, mCropImagedUri);
                //start the activity - we handle returning in onActivityResult
                startActivityForResult(cropIntent, CROP_IMAGE);
                return true;
            }
        }
        catch(ActivityNotFoundException anfe){
            //display an error message
            String errorMessage = "Whoops - your device doesn't support the crop action!";
            Toast toast = Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT);
            toast.show();
            return false;
        }
        return false;
    }

    private File createNewFile(String prefix){
        if(prefix==null || "".equalsIgnoreCase(prefix)){
            prefix="IMG_";
        }
        File newDirectory = new File(Environment.getExternalStorageDirectory()+"/mypics/");
        if(!newDirectory.exists()){
            if(newDirectory.mkdir()){
                Log.d(getApplicationContext().getClass().getName(), newDirectory.getAbsolutePath()+" directory created");
            }
        }
        File file = new File(newDirectory,(prefix+System.currentTimeMillis()+".jpg"));
        if(file.exists()){
            //this wont be executed
            file.delete();
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return file;
    }



    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        /*EditText textGoingTo = (EditText) findViewById(R.id.textView);
        textGointToString = textGoingTo.getText().toString();
        CheckBox checkBox = (CheckBox) findViewById(R.id.checkBox);
        if (checkBox.isChecked()) {
            privateStatus = "True";
        } else {
            privateStatus = "False";
        }
        Log.v("TEXT_GOING_1", textGointToString);
        outState.putString("TEXT_GOING",textGointToString);
        Log.v("TEXT_GOING", outState.getString("TEXT_GOING"));
        Toast.makeText(PostActivity.this, outState.getString("TEXT_GOING"), Toast.LENGTH_SHORT).show();*/
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        /*EditText textGoingTo = (EditText) findViewById(R.id.textView);
        textGoingTo.setText(savedInstanceState.getString("TEXT_GOING"));
        Toast.makeText(PostActivity.this, savedInstanceState.getString("TEXT_GOING"), Toast.LENGTH_SHORT).show();*/
    }
    @Override
    protected void onStart() {
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        mGoogleApiClient.disconnect();
        super.onStop();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dialog = new ProgressDialog(this);
        dialog.setMessage("Loading data... please wait");
        dialog.show();
        Firebase.setAndroidContext(this);
        idPostEditted = getIntent().getStringExtra("idPost");
        if (android.os.Build.VERSION.SDK_INT > 9) {
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
            StrictMode.setThreadPolicy(policy);
        }
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(LocationServices.API)
                    .build();
        }


        firebaseRef = new Firebase(appManager.FIREBASE);
        Firebase postFirebaseRef = new Firebase(appManager.FIREBASE).child("posts").child(idPostEditted);
        setContentView(R.layout.post);
        addpeople_text = (TextView) findViewById(R.id.addpeople_text);
        textGoingTo = (EditText) findViewById(R.id.atv_places);
        checkBox = (CheckBox) findViewById(R.id.checkBox);
        atvPlaces = (AutoCompleteTextView) findViewById(R.id.atv_places);
        SelectedDateView = (TextView) findViewById(R.id.selected_date);
        addphoto = (LinearLayout) findViewById(R.id.addphoto);
        addpeople = (LinearLayout) findViewById(R.id.addpeople);
        addphotocontainer = (ImageView) findViewById(R.id.postImg);
        line_addphoto = (View) findViewById(R.id.line_addphoto);
        postFirebaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                posts model = dataSnapshot.getValue(posts.class);
                mPost.add(0,model);
                posts editedPost = mPost.get(0);


                textGoingTo.setText(editedPost.getGoingTo());

                if(editedPost.getPrivateStatus().equals("True")){
                    checkBox.setChecked(true);
                }else{
                    checkBox.setChecked(false);
                }


                if (atvPlaces != null) {
                    atvPlaces.setThreshold(1);
                }
                atvPlaces.setText(editedPost.getGoingTo());
                withWho = editedPost.getWithWho();
                if(!withWho.contains("")){
                    StringBuilder sb = new StringBuilder();
                    if(withWho.size()>2){
                        for(int i=0;i<2;i++){
                            sb.append(withWho.get(i));
                            sb.append(", ");
                        }
                        sb.append("and ");
                        sb.append(withWho.size()-2);
                        sb.append(" more");
                    }else{
                        for(int i=0;i<withWho.size();i++){
                            sb.append(withWho.get(i));
                            if(i!=withWho.size()-1){
                                sb.append(", ");
                            }
                        }
                    }
                    addpeople_text.setText(sb.toString());
                }
                atvPlaces.addTextChangedListener(new TextWatcher() {

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        placesTask = new PlacesTask();
                        placesTask.execute(s.toString());
                    }

                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count,
                                                  int after) {
                        // TODO Auto-generated method stub
                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        // TODO Auto-generated method stub
                    }
                });


                if(!editedPost.getDateTime().isEmpty()){
                    SelectedDateView.setText("Selected Date: " + editedPost.getDateTime());
                }


                addpeople.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {
                        firebaseRef.child("follow").child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Follow follow = dataSnapshot.getValue(Follow.class);
                                ArrayList<String> following = follow.getFollowing();
                                ArrayList<String> followers = follow.getFollowers();
                                Intent i = new Intent(getApplicationContext(), SearchPeoplePost1.class);
                                i.putStringArrayListExtra("array",withWho);
                                i.putStringArrayListExtra("following",following);
                                i.putStringArrayListExtra("followers", followers);
                                startActivityForResult(i,PEOPLE_SELECTED);
                            }

                            @Override
                            public void onCancelled(FirebaseError firebaseError) {

                            }
                        });



                    }
                });



                addphoto.setOnClickListener(new View.OnClickListener() {

                    public void onClick(View v) {
                        if(isNetworkAvailable()) {
                            reqPermission();
                            Intent intent = new Intent();
                            // Show only images, no videos or anything else
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            // Always show the chooser (if there are multiple options available)
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
                        }
                        else
                            Toast.makeText(EditPostActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
                    }
                });

                //ViewGroup.LayoutParams params = (ViewGroup.LayoutParams) addphotocontainer.getLayoutParams();
                if(!editedPost.getImageURL().isEmpty()){
                    new DownloadTask().execute(editedPost.getImageURL());
                }else{
                    dialog.dismiss();
                }
                //params.height = w;
                //addphotocontainer.setLayoutParams(params);
                datetime = editedPost.getDateTime();
                addphotocontainer.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if(isNetworkAvailable()) {
                            reqPermission();
                            Intent intent = new Intent();
                            // Show only images, no videos or anything else
                            intent.setType("image/*");
                            intent.setAction(Intent.ACTION_GET_CONTENT);
                            // Always show the chooser (if there are multiple options available)
                            startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);
                        }
                        else
                            Toast.makeText(EditPostActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
                    }
                });



            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {

            }
        });

    }

    private class DownloadTask extends AsyncTask<String, Void, byte[]> {
        protected byte[] doInBackground(String... args) {
            byte[] data = null;
            Log.v("mencobaUrlIm", args[0]);
            EditPostActivity.this.urlImageTemp = args[0];
            URL url = null;
            try {
                url = new URL(args[0]);
                HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
                Log.v("mencobaUrlCon", url.toString());
                InputStream input = connection.getInputStream();
                Bitmap myBitmap = BitmapFactory.decodeStream(input);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                data = baos.toByteArray();
                Log.v("mencobaData", data.toString());

            } catch (IOException e) {
                e.printStackTrace();
            }// This is where you would do all the work of downloading your data
            Log.v("mencobafinal", data.toString());
            return data;
        }

        protected void onPostExecute(byte[] result) {
            // Pass the result data back to the main activity
            EditPostActivity.this.dataFinal = result;
            addphoto.setVisibility(View.GONE);
            line_addphoto.setVisibility(View.GONE);
            addphotocontainer.setVisibility(View.VISIBLE);
            Log.v("cobaImage", dataFinal.toString());
            Display display = getWindowManager().getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            int width = size.x;
            //int height = size.y;
            addphotocontainer.getLayoutParams().height = width;
            addphotocontainer.requestLayout();
            Picasso.with(EditPostActivity.this).load(urlImageTemp).into(addphotocontainer);
            if (EditPostActivity.this.dialog != null) {
                EditPostActivity.this.dialog.dismiss();
            }
        }
    }

    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            // Creating an http connection to communicate with url
            urlConnection = (HttpURLConnection) url.openConnection();

            // Connecting to url
            urlConnection.connect();

            // Reading data from url
            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            //Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        if (mLastLocation != null) {
            lat.add(0,mLastLocation.getLatitude());
            lon.add(0,mLastLocation.getLongitude());
            //Toast.makeText(PostActivity.this, String.valueOf(lat.get(0))+" - "+String.valueOf(lon.get(0)), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(EditPostActivity.this, "Cannot get current device location, using default location from IP Address", Toast.LENGTH_LONG).show();

    }

    // Fetches all places from GooglePlaces AutoComplete Web Service
    private class PlacesTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... place) {
            // For storing data from web service
            String data = "";

            // Obtain browser key from https://code.google.com/apis/console
            String key = "key="+appManager.API_KEY;

            String input="";

            input = "input=" + place[0].toString();


            // place type to be searched
            String radius = "radius=10000";

            // Sensor enabled
            String sensor = "sensor=true";
            String library = "libraries=places";
            String parameters = input+"&"+radius+"&"+sensor+"&"+key;

            if(lat.size()!=0 || lon.size()!=0) {

                String location = "location=" + String.valueOf(lat.get(0)) + "," + String.valueOf(lon.get(0));
                Log.v("cobaLocation", location);
                parameters = input+"&"+radius+"&"+sensor+"&"+location+"&"+key;
            }
            // Output format
            String output = "json";


            // Building the url to the web service
            String url = "https://maps.googleapis.com/maps/api/place/autocomplete/"+output+"?"+parameters;
            url = url.replaceAll(" ","%20");
            try{
                // Fetching the data from we service
                data = downloadUrl(url);
            }catch(Exception e){
                Log.d("Background Task",e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            // Creating ParserTask
            parserTask = new ParserTask();

            // Starting Parsing the JSON string returned by Web Service
            parserTask.execute(result);
        }
    }
    /** A class to parse the Google Places in JSON format */
    private class ParserTask extends AsyncTask<String, Integer, List<HashMap<String,String>>>{

        JSONObject jObject;

        @Override
        protected List<HashMap<String, String>> doInBackground(String... jsonData) {

            List<HashMap<String, String>> places = null;

            PlaceJSONParser placeJsonParser = new PlaceJSONParser();

            try{
                jObject = new JSONObject(jsonData[0]);

                // Getting the parsed data as a List construct
                places = placeJsonParser.parse(jObject);

            }catch(Exception e){
                Log.d("Exception",e.toString());
            }
            return places;
        }

        @Override
        protected void onPostExecute(List<HashMap<String, String>> result) {

            String[] from = new String[] { "description"};
            int[] to = new int[] { android.R.id.text1 };
            ArrayList<String> dataArray = new ArrayList<String>();
            if(result!=null) {
                for (int i = 0; i < result.size(); i++) {
                    String res = result.get(i).toString();
                    String resFinal = res.substring(res.lastIndexOf("description=") + 12);
                    resFinal = resFinal.replace("}", "");
                    dataArray.add(resFinal);
                }
            }
            FilterWithSpaceAdapter<String> adapter1;
//...
            adapter1 = new FilterWithSpaceAdapter<String>(EditPostActivity.this,
                    android.R.layout.simple_list_item_1, dataArray);
            // Setting the adapter
            atvPlaces.setAdapter(adapter1);
        }
    }


    public void reqPermission()
    {
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
        if (ContextCompat.checkSelfPermission(this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                // Show an expanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

            } else {

                // No explanation needed, we can request the permission.

                ActivityCompat.requestPermissions(this,
                        new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        REQUEST_WRITE_STORAGE);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Toast.makeText(EditPostActivity.this, "The app was not allowed to read to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case REQUEST_WRITE_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                {
                    //reload my activity with permission granted or use the features what required the permission
                } else
                {
                    Toast.makeText(EditPostActivity.this, "The app was not allowed to write to your storage. Hence, it cannot function properly. Please consider granting it this permission", Toast.LENGTH_LONG).show();
                }
            }

            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current date as the default date in the picker
            final Calendar c = Calendar.getInstance();
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            // Create a new instance of DatePickerDialog and return it
            return new DatePickerDialog(getActivity(), this, year, month, day);
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
//                    SelectedDateView.setText("Selected Date: " + (month + 1) + "-" + day + "-" + year);
//            Toast.makeText(PostActivity.this,"Selected Date: " + (month + 1) + "-" + day + "-" + year, Toast.LENGTH_SHORT ).show();
            Day = day;
            Month = month;
            Year = year;
            showTimePickerDialog(view);
        }
    }

    public static TimePicker timePicker;

    public class TimePickerFragment extends DialogFragment
            implements TimePickerDialog.OnTimeSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Use the current time as the default values for the picker
            final Calendar c = Calendar.getInstance();
            int hour = c.get(Calendar.HOUR_OF_DAY);
            int minute = c.get(Calendar.MINUTE);

            // Create a new instance of TimePickerDialog and return it
            return new TimePickerDialog(getActivity(), this, hour, minute,
                    DateFormat.is24HourFormat(getActivity()));
        }

        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            hour = hourOfDay;
            Minute = minute;
            setDateTime();
        }
    }

    public void showTimePickerDialog(View v) {
        DialogFragment newFragment = new TimePickerFragment();
        newFragment.show(getFragmentManager(), "timePicker");
    }

    public void setDateTime() {
        String menittemp="00";
        String jamtemp="00";
        if(hour<10) {
            jamtemp = "0"+String.valueOf(hour);
        }
        else
        {
            jamtemp=String.valueOf(hour);
        }
        if(Minute<10) {
            menittemp = "0"+String.valueOf(Minute);
        }
        else
        {
            menittemp=String.valueOf(Minute);
        }
        SelectedDateView.setText("Selected Date: " + (MONTHS[Month]) + "-" + Day + "-" + Year + "-" + jamtemp + "." + menittemp);
        datetime = (MONTHS[Month]) + "-" + Day + "-" + Year + "-" + jamtemp + "." + menittemp;

//        Toast.makeText(PostActivity.this,"Selected Date: " + (Month + 1) + "-" + Day + "-" + Year + "-" + hour + "." + Minute, Toast.LENGTH_SHORT ).show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CROP_IMAGE && data!=null) {
            Bundle extras = data.getExtras();
            if(extras != null ) {

                Bitmap selectedBitmap = null;
                try {
                    selectedBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),mCropImagedUri);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    selectedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                    dataFinal = baos.toByteArray();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                addphoto.setVisibility(View.GONE);
                line_addphoto.setVisibility(View.GONE);
                addphotocontainer.setVisibility(View.VISIBLE);
                Log.v("cobaImage", dataFinal.toString());
                Display display = getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);
                int width = size.x;
                //int height = size.y;
                addphotocontainer.setImageBitmap(selectedBitmap);
                addphotocontainer.getLayoutParams().height = width;
                addphotocontainer.requestLayout();
//                    addphotocontainer.setImageBitmap(BitmapFactory.decodeFile(picturePath));



            }
        }

        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.getData() != null){
            mFinalImageUri = data.getData();
            performCropImage();
            Log.v("mencobalagi",mCropImagedUri.toString());
            Log.v("mencobalagi2", mFinalImageUri.toString());
            //addphoto.setVisibility(View.GONE);
            //line_addphoto.setVisibility(View.GONE);
            //addphotocontainer.setVisibility(View.VISIBLE);
            //Picasso.with(this).load(mFinalImageUri).into(addphotocontainer);


        }

        if (requestCode == PEOPLE_SELECTED) {
            if(resultCode == RESULT_OK){
                if (data != null) {
                    ArrayList<String> peopleSelected = data.getStringArrayListExtra("result");
                    StringBuilder sb = new StringBuilder();
                    if(peopleSelected.size()>2){
                        for(int i=0;i<2;i++){
                            sb.append(peopleSelected.get(i));
                            sb.append(", ");
                        }
                        sb.append("and ");
                        sb.append(peopleSelected.size()-2);
                        sb.append(" more");
                    }else{
                        for(int i=0;i<peopleSelected.size();i++){
                            sb.append(peopleSelected.get(i));
                            if(i!=peopleSelected.size()-1){
                                sb.append(", ");
                            }
                        }
                    }
                    addpeople_text.setText(sb.toString());
                    withWho = peopleSelected;
                    if(withWho.size()>1){
                        withWho.remove("");
                    }
                }
            }

        }
    }

    @Override
    public void onBackPressed() {
        //Intent i = new Intent(PostActivity.this, MainActivity.class);

        //startActivity(i);
        //overridePendingTransition(R.anim.slide_out, R.anim.slide_in);

        finish();
    }

    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getFragmentManager(), "datePicker");
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    public void onButtonPostPressed(View v) {

        if(isNetworkAvailable()) {
            textGointToString = textGoingTo.getText().toString();
            final String timeStamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new java.util.Date());
            final Long ts = System.currentTimeMillis() / 1000 * (-1);
            final int size = withWho.size();
            //final String ts = tsLong.toString();

            if (checkBox.isChecked()) {
                privateStatus = "True";
            } else {
                privateStatus = "False";
            }

            Toast.makeText(EditPostActivity.this, privateStatus, Toast.LENGTH_SHORT).show();

            if (dataFinal != null) {
                StorageReference riversRef = storageRef.child("images/" + "IMG_CROP" + timeStamp);
                UploadTask uploadTask = riversRef.putBytes(dataFinal);

                // Register observers to listen for when the download is done or if it fails
                uploadTask.addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception exception) {
                        // Handle unsuccessful uploads
                    }
                }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                    @Override
                    public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                        // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                        Uri downloadUrl = taskSnapshot.getDownloadUrl();
                        uriDownload = downloadUrl.toString();
                        Toast.makeText(EditPostActivity.this, uriDownload, Toast.LENGTH_SHORT).show();


                        if (user != null) {
                            Toast.makeText(EditPostActivity.this, "Post Success!", Toast.LENGTH_SHORT).show();
                            //Toast.makeText(PostActivity.this, authData.getToken(), Toast.LENGTH_SHORT ).show();
                            Log.v("mencoba", firebaseRef.child("users").orderByChild(user.getUid()).startAt(user.getUid()).endAt(user.getUid() + "\uf8ff").getPath().toString());
                            pushFirebaseRef = firebaseRef.child("posts").child(idPostEditted);
                            pushFirebaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    final posts temp = dataSnapshot.getValue(posts.class);
                                    final posts post = new posts(textGointToString, uriDownload, withWho, datetime, privateStatus, user.getUid(), idPostEditted, mPost.get(0).getTimeStamp());
                                    for (int x = 0; x < temp.getLikers().size(); x++) {
                                        liker.add(temp.getLikers().get(x));
                                    }
                                    for (int y = 0; y < temp.getWaitingRecipients().size(); y++) {
                                        waiting.add(temp.getWaitingRecipients().get(y));
                                    }
                                    for (int z = 0; z < temp.getWithWho().size(); z++) {
                                        if (!withWho.contains(temp.getWithWho().get(z)) && !mPost.get(0).getWithWho().contains(temp.getWithWho().get(z))) {
                                            withWho.add(temp.getWithWho().get(z));
                                        }
                                    }
                                    post.setWaitingRecipients(waiting);
                                    post.setLikers(liker);
                                    pushFirebaseRef.setValue(post);
                                    for (int i = 0; i < size; i++) {
                                        String username = withWho.get(i);
                                        if (username.isEmpty()) {

                                        } else {
                                            firebaseRef.child("users").orderByChild("username").startAt(username).endAt(username + "\uf8ff").addChildEventListener(new ChildEventListener() {
                                                @Override
                                                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                                    User someone = dataSnapshot.getValue(User.class);
                                                    Firebase pushFirebaseRef = firebaseRef.child("activity").child(someone.getId()).push();
                                                    final String idActivity = pushFirebaseRef.getKey();
                                                    Long timeStamp = System.currentTimeMillis() / 1000 * (-1);
                                                    Activities newActivity = new Activities(someone.getId(), "Invite you to join", FirebaseAuth.getInstance().getCurrentUser().getUid(), "posts", idPostEditted, idActivity, timeStamp);
                                                    pushFirebaseRef.setValue(newActivity);
                                                }

                                                @Override
                                                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                                }

                                                @Override
                                                public void onChildRemoved(DataSnapshot dataSnapshot) {

                                                }

                                                @Override
                                                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                                }

                                                @Override
                                                public void onCancelled(FirebaseError firebaseError) {

                                                }
                                            });
                                        }
                                    }

                                    firebaseRef.child("users").child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            someone = dataSnapshot.getValue(User.class);
                                            FriendlyMessage chat = new FriendlyMessage("Hey, let's join me going to " + post.getGoingTo(), someone.getUsername(), someone.getProfpic(), timeStamp, someone.getNama());
                                            meWithWho = withWho;
                                            meWithWho.add(someone.getUsername());
                                            Log.v("mencoba lgi", someone.getUsername());
                                            if (!temp.getGoingTo().equals(post.getGoingTo())) {
                                                firebaseRef.child("message").child(idPostEditted).child("chat").push().setValue(chat);
                                            }
                                            firebaseRef.child("message").child(idPostEditted).child("username").setValue(meWithWho);

                                        }

                                        @Override
                                        public void onCancelled(FirebaseError firebaseError) {

                                        }
                                    });
                                }

                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }
                            });


                        } else {
                            Toast.makeText(EditPostActivity.this, "Failed to Post, please Login first!", Toast.LENGTH_SHORT).show();
                        }


                        //Intent i = new Intent(PostActivity.this, MainActivity.class);
                        //startActivity(i);
                        finish();

                    }
                });

                mAuthProgressDialog = new ProgressDialog(this);
                mAuthProgressDialog.setTitle("Loading");
                mAuthProgressDialog.setMessage("Posting your Post...");
                mAuthProgressDialog.setCancelable(false);
                mAuthProgressDialog.show();


            } else {
                final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();


                if (user != null) {
                    Toast.makeText(EditPostActivity.this, "Post Success!", Toast.LENGTH_SHORT).show();
                    //Toast.makeText(PostActivity.this, authData.getToken(), Toast.LENGTH_SHORT ).show();
                    pushFirebaseRef = firebaseRef.child("posts").child(idPostEditted);
                    pushFirebaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            final posts temp = dataSnapshot.getValue(posts.class);
                            for (int x = 0; x < temp.getLikers().size(); x++) {
                                liker.add(temp.getLikers().get(x));
                            }
                            for (int y = 0; y < temp.getWaitingRecipients().size(); y++) {
                                waiting.add(temp.getWaitingRecipients().get(y));
                            }
                            for (int z = 0; z < temp.getWithWho().size(); z++) {
                                if (!withWho.contains(temp.getWithWho().get(z)) && !mPost.get(0).getWithWho().contains(temp.getWithWho().get(z))) {
                                    withWho.add(temp.getWithWho().get(z));
                                }
                            }
                            final posts post = new posts(textGointToString, uriDownload, withWho, datetime, privateStatus, user.getUid(), idPostEditted, mPost.get(0).getTimeStamp());
                            post.setWaitingRecipients(waiting);
                            post.setLikers(liker);
                            pushFirebaseRef.setValue(post);
                            for (int i = 0; i < size; i++) {
                                String username = withWho.get(i);
                                if (username.isEmpty()) {

                                } else {
                                    firebaseRef.child("users").orderByChild("username").startAt(username).endAt(username + "\uf8ff").addChildEventListener(new ChildEventListener() {
                                        @Override
                                        public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                                            User someone = dataSnapshot.getValue(User.class);
                                            Firebase pushFirebaseRef = firebaseRef.child("activity").child(someone.getId()).push();
                                            final String idActivity = pushFirebaseRef.getKey();
                                            Long timeStamp = System.currentTimeMillis() / 1000 * (-1);
                                            Activities newActivity = new Activities(someone.getId(), "Invite you to join", FirebaseAuth.getInstance().getCurrentUser().getUid(), "posts", idPostEditted, idActivity, timeStamp);
                                            pushFirebaseRef.setValue(newActivity);
                                        }

                                        @Override
                                        public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onChildRemoved(DataSnapshot dataSnapshot) {

                                        }

                                        @Override
                                        public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                                        }

                                        @Override
                                        public void onCancelled(FirebaseError firebaseError) {

                                        }
                                    });
                                }
                            }

                            firebaseRef.child("users").child(user.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    someone = dataSnapshot.getValue(User.class);
                                    FriendlyMessage chat = new FriendlyMessage("Hey, let's join me going to " + post.getGoingTo(), someone.getUsername(), someone.getProfpic(), timeStamp, someone.getNama());
                                    meWithWho = withWho;
                                    meWithWho.add(someone.getUsername());
                                    meWithWho.remove("");
                                    Log.v("mencoba lgi", someone.getUsername());
                                    if (!temp.getGoingTo().equals(post.getGoingTo())) {
                                        firebaseRef.child("message").child(idPostEditted).child("chat").push().setValue(chat);
                                    }
                                    firebaseRef.child("message").child(idPostEditted).child("username").setValue(meWithWho);
                                }

                                @Override
                                public void onCancelled(FirebaseError firebaseError) {

                                }
                            });
                        }

                        @Override
                        public void onCancelled(FirebaseError firebaseError) {

                        }
                    });


                } else {
                    Toast.makeText(EditPostActivity.this, "Failed to Post, please Login first!", Toast.LENGTH_SHORT).show();
                }
                //Intent i = new Intent(PostActivity.this, MainActivity.class);
                //startActivity(i);
                finish();
            }
        }
        else
            Toast.makeText(EditPostActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
    }
}
