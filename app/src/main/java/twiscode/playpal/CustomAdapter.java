package twiscode.playpal;

/**
 * Created by User on 4/4/2016.
 */

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.transition.Explode;
import android.transition.Transition;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import twiscode.playpal.Utilities.ApplicationData;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private ArrayList<DataModel> dataSet;
    private static Context context;
    private static LinearLayout btn_like;
    private static LinearLayout btn_comment;
    private static LinearLayout btn_going;
    private static LinearLayout unlike;
    public static float screen_width;

    public CustomAdapter(Context context, ArrayList<DataModel> dataSet) {
        this.context = context;
        this.dataSet = dataSet;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        TextView txtName;
        TextView txtHour;
        TextView txtComment;
        ImageView imgView;
        ApplicationData appData;


        public MyViewHolder(final View itemView) {
            super(itemView);
            this.txtName = (TextView) itemView.findViewById(R.id.awijaya);
            this.txtHour = (TextView) itemView.findViewById(R.id.at15_00);
            this.txtComment = (TextView) itemView.findViewById(R.id.comment);
            this.imgView = (ImageView) itemView.findViewById(R.id.img_activity);

            btn_like = (LinearLayout) itemView.findViewById(R.id.btn_like);
            btn_like.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    view.setVisibility(View.GONE);
                    LinearLayout unlike = (LinearLayout) itemView.findViewById(R.id.unlike);
                    unlike.setVisibility(View.VISIBLE);
                }
            });

            unlike = (LinearLayout) itemView.findViewById(R.id.unlike);
            unlike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.setVisibility(View.GONE);
                    LinearLayout btn_like = (LinearLayout) itemView.findViewById(R.id.btn_like);
                    btn_like.setVisibility(View.VISIBLE);
                }
            });
            btn_comment = (LinearLayout) itemView.findViewById(R.id.btn_comment);
            btn_comment.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    Intent i=new Intent(view.getContext(),CommentActivity.class);
                    view.getContext().startActivity(i);
                    appData.itemPosition = getAdapterPosition();


                }

            });

            btn_going = (LinearLayout) itemView.findViewById(R.id.btn_going);
            btn_going.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {
                    AlertDialog.Builder builder =
                            new AlertDialog.Builder(context);
                    builder.setTitle(R.string.app_name);
                    builder.setMessage("Are you sure ?");
                    builder.setPositiveButton("Yes, of course", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            ImageView iconGoing = (ImageView) itemView.findViewById(R.id.iconGoing);
                            TextView txtGoing = (TextView) itemView.findViewById(R.id.txtGoing);

                            if (txtGoing.getText().toString().equals("Going")) {
                                iconGoing.setImageResource(R.drawable.goingpinkicon);
                                txtGoing.setText("Ungoing");
                            } else {
                                iconGoing.setImageResource(R.drawable.newgoingicon);
                                txtGoing.setText("Going");
                            }
                        }
                    });
                    builder.setNegativeButton("Cancel", null);
                    builder.show();

                }
            });

            imgView = (ImageView) itemView.findViewById(R.id.img_activity);
            imgView.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {

                    if (imgView.getHeight()==400) {
                        int width = imgView.getWidth();
                        imgView.setLayoutParams(new LinearLayout.LayoutParams(width, width));
                        imgView.setScaleType(ImageView.ScaleType.FIT_XY);

                    }else{
                        int width = imgView.getWidth();
                        int height = imgView.getHeight();
                        imgView.setLayoutParams(new LinearLayout.LayoutParams(width, 400));
                        imgView.setScaleType(ImageView.ScaleType.FIT_XY);


                    }
                }


            });


        }
    }

    public CustomAdapter(ArrayList<DataModel> data) {
        this.dataSet = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_layout, parent, false);

        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {
        TextView txtName = holder.txtName;
        TextView txtHour = holder.txtHour;
        TextView txtComment = holder.txtComment;

        txtName.setText(dataSet.get(listPosition).getName());
        txtHour.setText(dataSet.get(listPosition).getHour());
        txtComment.setText(dataSet.get(listPosition).getComment());

        Picasso.with(context).load(dataSet.get(listPosition).getImage()).into(holder.imgView);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}

