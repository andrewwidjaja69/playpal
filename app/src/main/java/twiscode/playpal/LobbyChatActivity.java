package twiscode.playpal;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.DataSetObserver;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import twiscode.playpal.Utilities.ConfigManager;

public class LobbyChatActivity extends AppCompatActivity {

    Toolbar mToolbar;
    private DatabaseReference mDatabase;
    ListAdapterLobby mListAdapterLobby;
    Activity activity = this;
    private String username;
    Firebase mFirebaseRef = new Firebase(ConfigManager.FIREBASE).child("posts");
    ListView listView = null;
    SwipeRefreshLayout refresher;
    private User user1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lobby_chat);
        Firebase.setAndroidContext(this);
        FirebaseUser userF = FirebaseAuth.getInstance().getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        TypefaceUtil.overrideFont(getApplicationContext(), "SERIF", "fonts/Lato-Regular.ttf");
        Intent intent = getIntent();
        //username = intent.getStringExtra("username");
        mToolbar = (Toolbar) findViewById(R.id.toolbar1);
        mToolbar.setNavigationIcon(R.drawable.ic_action_name_white);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(true);
        listView = (ListView) findViewById(R.id.List);
        refresher = (SwipeRefreshLayout) findViewById(R.id.refresher);
        if (userF != null) {
            mDatabase.child("users").child(userF.getUid()).addValueEventListener(
                    new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            // Get user value
                            User user = dataSnapshot.getValue(User.class);
                            user1 = dataSnapshot.getValue(User.class);
                            username = user1.getUsername();
                            mListAdapterLobby = new ListAdapterLobby(getApplicationContext(),mFirebaseRef,activity,R.layout.lobby_item,username);
                            listView.setAdapter(mListAdapterLobby);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {
                            Log.w("!Error", "getUser:onCancelled", databaseError.toException());
                        }
                    }
            );
        }
        else
        {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return;
        }


        //Log.v("mencoba", mDatabase.child("message").orderByChild("username").startAt(userid).endAt(userid+"\uf8ff").getRef().getKey().toString());

        if(listView != null)
        {
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener(){
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(isNetworkAvailable()) {
                        posts select = (posts) listView.getItemAtPosition(position);
                        Intent j = new Intent(LobbyChatActivity.this, ChatActivity2.class);
                        j.putExtra("idroom", select.getPostId());
                        j.putExtra("nroom", select.getGoingTo());
                        j.putExtra("wroom", select.getDateTime());
                        j.putExtra("user", username);
                        startActivityForResult(j, 1);
                        //finish();
                    }
                    else
                        Toast.makeText(LobbyChatActivity.this, "No Network Connection! Can't Enter This Chat Room", Toast.LENGTH_SHORT).show();
                }
            });
        }
        refresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if(!isNetworkAvailable())
                {
                    Toast.makeText(LobbyChatActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
                    refresher.setRefreshing(false);
                }
                else {
                    mListAdapterLobby = new ListAdapterLobby(getApplicationContext(), mFirebaseRef, activity, R.layout.lobby_item, username);
                    listView.setAdapter(mListAdapterLobby);
                    mListAdapterLobby.registerDataSetObserver(new DataSetObserver() {
                        @Override
                        public void onChanged() {
                            super.onChanged();
                            refresher.setRefreshing(false);
                        }
                    });
                }
            }
        });
    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
    @Override
    public void onStart() {
        super.onStart();
        if(!isNetworkAvailable())
        {
            Toast.makeText(LobbyChatActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        if(isNetworkAvailable()) {
            mListAdapterLobby = new ListAdapterLobby(getApplicationContext(), mFirebaseRef, activity, R.layout.lobby_item, username);
            listView.setAdapter(mListAdapterLobby);
        }
        else
            Toast.makeText(LobbyChatActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //if (drawerToggle.onOptionsItemSelected(item)) {
        //   return true;
        //}

        switch (item.getItemId()) {
            case android.R.id.home:
                //Intent j = new Intent(LobbyChatActivity.this, MainActivity.class);
                //startActivity(j);
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
    @Override
    public void onBackPressed() {
        //Intent i = new Intent(LobbyChatActivity.this, MainActivity.class);
        //startActivity(i);
        //overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
        finish();
    }
}
