package twiscode.playpal;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.firebase.client.Firebase;
import com.firebase.client.Query;
import com.firebase.client.utilities.Pair;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import twiscode.playpal.Utilities.ConfigManager;

/**
 * Created by Crusader on 6/24/2016.
 */
public class ListAdapterParticipant extends FirebaseListAdapterParticipant {
    private String mUsername;
    Firebase mFirebaseRefCustom = new Firebase(ConfigManager.FIREBASE).child("users");
    Context context;
    Firebase firebaseRef = new Firebase(ConfigManager.FIREBASE).child("message");
    FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
    String userId = user.getUid();
    ChatDetailsActivity mActivity;
    public ListAdapterParticipant(Context context, Query ref, Activity activity, int layout) {
        super(ref, String.class, layout, activity);
        this.context = context;
        this.mActivity = (ChatDetailsActivity) activity;
    }
    @Override
    protected void populateView(final View view, final User follow) {
        //Firebase newMFirebaseRefCustom = mFirebaseRefCustom.child(follow);
//        Log.v("FollowText", follow);
//        Log.v("StatusText", status);
//        String lala = mFollow.getFollowers().get(0);
//        String lala2 = mFollow.getFollowers().get(1);
//        Log.v("get", lala);
//        Log.v("get2", lala2);
        //final User someone = follow.;
        String profpicUrl = follow.getProfpic();
        ImageView profpic = (ImageView) view.findViewById(R.id.follow_item_image);
        TextView nama = (TextView) view.findViewById(R.id.follow_item_nama);
        TextView username = (TextView) view.findViewById(R.id.follow_item_username);
        RelativeLayout followBtn = (RelativeLayout) view.findViewById(R.id.follow_item_button);
        TextView followBtnText = (TextView) view.findViewById(R.id.follow_item_button_text);
        if(profpicUrl!="")
        {
            Picasso.with(context).load(profpicUrl).transform(new CircleTransform()).into(profpic);
        }
        else
        {
            profpic.setImageResource(R.drawable.ayam);
        }
        //Picasso.with(context).load(profpicUrl).into(profpic);
        Log.v("cobaUser", follow.getNama());
        String namaText = follow.getNama();
        String usernameText = follow.getUsername();
        nama.setText(namaText);
        username.setText(usernameText);
        Log.v("inilagi", nama.getText().toString());
        //Log.v("mParticipant", mParticipant.getUsername().toString());
        Log.v("mFollowID", follow.getId());
    }
}
