package twiscode.playpal;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.firebase.client.core.view.Change;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserInfo;

public class ChangePassActivity extends AppCompatActivity {

    private FirebaseAuth mAuth;
    private String email;
    private String oldpassword;
    private EditText newPass;
    private EditText rnewPass;
    private Button changepass;
    private String providerId;
    FirebaseUser user;
    Toolbar mToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pass);
        mToolbar = (Toolbar) findViewById(R.id.toolbar1);
        mToolbar.setNavigationIcon(R.drawable.ic_action_name_white);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(null);
        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();
        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);
        newPass = (EditText) findViewById(R.id.input_new_pass);
        rnewPass = (EditText) findViewById(R.id.input_retype_new_pass);
        changepass = (Button) findViewById(R.id.buttonchange);
        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            email=user.getEmail();
            for (UserInfo profile : user.getProviderData()) {
                // Id of the provider (ex: google.com)
                providerId = profile.getProviderId();
            };
        }
        else
        {
            // Not signed in, launch the Sign In activity
            Toast.makeText(ChangePassActivity.this, "Please Login Again", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(this, LoginActivity.class));
            finish();
            return;
        }
        changepass.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if(isNetworkAvailable()) {
                    if (providerId == "facebook.com") {
                        Toast.makeText(ChangePassActivity.this, "You're login with facebook.",
                                Toast.LENGTH_SHORT).show();
                    } else {
                        changeIt();
                    }
                }
                else
                    Toast.makeText(ChangePassActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
            }
        });
        if(!isNetworkAvailable()) Toast.makeText(ChangePassActivity.this, "No Network Connection!", Toast.LENGTH_SHORT).show();
    }
    private boolean validateForm() {
        boolean valid = true;

        String newpasswordtemp = newPass.getText().toString();
        if (TextUtils.isEmpty(newpasswordtemp)) {
            newPass.setError("Required.");
            valid = false;
        } else {
            newPass.setError(null);
        }

        String rnewpasswordtemp = rnewPass.getText().toString();
        if (TextUtils.isEmpty(rnewpasswordtemp)) {
            rnewPass.setError("Required.");
            valid = false;
        } else {
            rnewPass.setError(null);
        }
        if(newPass.getText().toString().equals(rnewPass.getText().toString())){
            rnewPass.setError(null);
        }else{
            rnewPass.setError("Not same.");
            valid = false;
        }
        return valid;
    }
    private void changeIt()
    {
        if (!validateForm()) {
            return;
        }
        user.updatePassword(rnewPass.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(ChangePassActivity.this, "Change password success.",
                            Toast.LENGTH_SHORT).show();
                    Intent i = new Intent(ChangePassActivity.this, SettingsActivity.class);
                    startActivity(i);
                    //overridePendingTransition(R.anim.slide_out, R.anim.slide_in);
                    finish();
                    Log.d("changepass", "User password updated.");
                }
                else{
                    Toast.makeText(ChangePassActivity.this, "Change password failed.",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

    }
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }
}
